#include "open62541/server.h"
#include <open62541/client_config_default.h>
#include <open62541/client_highlevel.h>
#include <open62541/client_subscriptions.h>
#include "optimum_pubsub_client.h"
#include "open62541/plugin/log_stdout.h"
//#include "optimum_mqtt_publish.h"
#include "pubsub_mqtt_publish.h"
#include "optimum_device1.h"
#include "ua_pubsub.h"
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

char DockerBridgeIP[15];
int appMode;
int32_t PosX;
int32_t PosY;
uint32_t PosXCount;
uint32_t PosYCount;
char delimiter[] = ";";
char *ptr;
char serverURL[100];

UA_AppConfig *g_appConfig1;
UA_Client *client;

void writeValueToNode(){

    /** Write the value to a VariableNode Position X **/
    UA_NodeId nodeX = UA_NODEID_NUMERIC(2, 1026);
    UA_Variant valueX;
    UA_Variant_init(&valueX);
    UA_Variant_setScalar(&valueX, &PosX, &UA_TYPES[UA_TYPES_INT32]);
    UA_Server_writeValue(g_appConfig1->server, nodeX, valueX);

    /** Write the value to a VariableNode Position Y **/
    UA_NodeId nodeY = UA_NODEID_NUMERIC(2, 1027);
    UA_Variant valueY;
    UA_Variant_init(&valueY);
    UA_Variant_setScalar(&valueY, &PosY, &UA_TYPES[UA_TYPES_INT32]);
    UA_Server_writeValue(g_appConfig1->server, nodeY, valueY);

}

void clientInitialization(void *arg){
    PosX = 0;
    PosXCount = 0;
    PosY = 0;
    PosYCount = 0;

    g_appConfig1 = (UA_AppConfig *) malloc(sizeof(UA_AppConfig));
    g_appConfig1 = (UA_AppConfig*)arg;

    ptr = strtok((char *)g_appConfig1->arguments, delimiter);
    
    
    strcpy(serverURL, ptr);
    ptr = strtok(NULL, delimiter);    
    appMode = atoi(ptr);

    if(appMode == COMETOME || appMode == FOLLOWME){
	UA_StatusCode retval;
	client = UA_Client_new();
	UA_ClientConfig_setDefault(UA_Client_getConfig(client));
	//client = UA_Client_new(UA_ClientConfig_default); // opc.tcp://nirazan-Inspiron-3420:4840/
	retval = UA_Client_connect(client, serverURL);

        if (retval != UA_STATUSCODE_GOOD)
       {
	  UA_Client_delete(client);
      //  return EXIT_FAILURE;
       }		
    }
	
}

static void
callback_NewLocDataX(UA_Client *client1, UA_UInt32 subId, void *subContext,
                     UA_UInt32 monId, void *monContext, UA_DataValue *value)
{
    if (value->value.type == &UA_TYPES[UA_TYPES_INT32])
    {
        PosX = *(UA_Int32 *)value->value.data;
		writeValueToNode(); // write PosX and PosY variable values from Human Operator to the IIoT platform address space
		pub_mqtt_setup(g_appConfig1, FOLLOWME);
    }
    else
    {
        printf("could not decode received value for coordinate X!\n");
    }
}

static void
callback_NewLocDataY(UA_Client *client2, UA_UInt32 subId, void *subContext,
                     UA_UInt32 monId, void *monContext, UA_DataValue *value)
{
    if (value->value.type == &UA_TYPES[UA_TYPES_INT32])
    {
        PosY = *(UA_Int32 *)value->value.data;
		writeValueToNode(); // write PosX and PosY variable values from Human Operator to the IIoT platform address space
		pub_mqtt_setup(g_appConfig1, FOLLOWME);
    }
    else
    {
        printf("could not decode received value for coordinate Y!\n");
    }
}

void *goToPosition(void *arg)
{
    clientInitialization(arg);
	ptr = strtok(NULL, delimiter);
 	PosX = atoi(ptr);
	ptr = strtok(NULL, delimiter);
	PosY = atoi(ptr);      	
		   	
	writeValueToNode();
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;
	retVal = pub_mqtt_setup(g_appConfig1, GOTO);

	if (retVal == UA_STATUSCODE_GOOD) {
	    	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "NetworkMessage set up successful\n");
	    }
	else
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "NetworkMessage set up failed\n");
	pthread_exit(NULL); 
	return 0;
}

void *comeToMe(void *arg){
	clientInitialization(arg);

	UA_ReadRequest request;
	UA_ReadRequest_init(&request);
	UA_ReadValueId ids[2];
	UA_ReadValueId_init(&ids[0]);
	ids[0].attributeId = UA_ATTRIBUTEID_VALUE;
	ids[0].nodeId = UA_NODEID_NUMERIC(1, 1026); // variableNode PositionX in Human Operator

	UA_ReadValueId_init(&ids[1]);
	ids[1].attributeId = UA_ATTRIBUTEID_VALUE;
	ids[1].nodeId = UA_NODEID_NUMERIC(1, 1027); // variableNode PositionY in Human Operator

	// set here the nodes you want to read
	request.nodesToRead = ids;
	request.nodesToReadSize = 2;

	UA_ReadResponse response = UA_Client_Service_read(client, request);
   
	if(response.responseHeader.serviceResult == UA_STATUSCODE_GOOD && response.resultsSize > 0 &&
	response.results[0].hasValue && UA_Variant_isScalar(&response.results[0].value) &&
	response.results[0].value.type == &UA_TYPES[UA_TYPES_INT32]) {
			PosX = *(UA_Int32 *)response.results[0].value.data;
	}

   if(response.responseHeader.serviceResult == UA_STATUSCODE_GOOD && response.resultsSize > 1 &&
	response.results[1].hasValue && UA_Variant_isScalar(&response.results[1].value) &&
	response.results[1].value.type == &UA_TYPES[UA_TYPES_INT32]) {
			PosY = *(UA_Int32 *)response.results[1].value.data;
	}
	
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Read PosX: %d, PosY: %d \n", PosX, PosY);
	
	writeValueToNode(); // write PosX and PosY variable values from Human Operator to the IIoT platform address space
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;
	retVal = pub_mqtt_setup(g_appConfig1, COMETOME);
	
	if (retVal == UA_STATUSCODE_GOOD) {
	    	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "NetworkMessage set up successful\n");
	    }
	else
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "NetworkMessage set up failed\n");
	UA_Client_delete(client);
    
	pthread_exit(NULL); 
	return 0;
}

void *followMe(void *arg){

    clientInitialization(arg);
    //pub_mqtt_setup(g_appConfig1, FOLLOWME);
	 

	/* Create a subscription */
    UA_CreateSubscriptionRequest request = UA_CreateSubscriptionRequest_default();

    request.requestedLifetimeCount = 1000;
    request.requestedMaxKeepAliveCount = 1000;

    // ask for a publishing interval of 100 ms
    //TODO: check if server accepts this value
    request.requestedPublishingInterval = 100.0;
    UA_CreateSubscriptionResponse response = UA_Client_Subscriptions_create(client, request, NULL, NULL, NULL);

    UA_UInt32 subId = response.subscriptionId;
    if (response.responseHeader.serviceResult == UA_STATUSCODE_GOOD)
        printf("Create subscription succeeded, id %u\n", subId);

    UA_MonitoredItemCreateRequest monRequestX = UA_MonitoredItemCreateRequest_default(UA_NODEID_NUMERIC(1, 1026)); //X Coordinate
    UA_MonitoredItemCreateResult monResponseX = UA_Client_MonitoredItems_createDataChange(client, response.subscriptionId,
                                                                                          UA_TIMESTAMPSTORETURN_BOTH, monRequestX, NULL, 		callback_NewLocDataX, NULL);
    if (monResponseX.statusCode == UA_STATUSCODE_GOOD)
        printf("Monitoring 'Operator Hardware Location X Coordinate', id %u\n", monResponseX.monitoredItemId);

    UA_MonitoredItemCreateRequest monRequestY = UA_MonitoredItemCreateRequest_default(UA_NODEID_NUMERIC(1, 1027)); //Y Coordinate
    UA_MonitoredItemCreateResult monResponseY = UA_Client_MonitoredItems_createDataChange(client, response.subscriptionId,
                                                                                          UA_TIMESTAMPSTORETURN_BOTH, monRequestY, NULL, 	  callback_NewLocDataY, NULL);
    if (monResponseY.statusCode == UA_STATUSCODE_GOOD)
        printf("Monitoring 'Operator Hardware Location Y Coordinate', id %u\n", monResponseY.monitoredItemId);

    int cond = TRUE;

    while (cond)
    {
        UA_Client_run_iterate(client, 0);

        if ((appMode != FOLLOWME) && (appMode != FOLLOWMACHINE))
        {
            cond = FALSE;
        }

        if (clientTerminateSignal == 1)
        {
            clientTerminateSignal = 0;
            cond = FALSE;
            printf("client termiantes now\n");
        }
    }

    UA_Client_disconnect(client);
    UA_Client_delete(client);
    pthread_exit(NULL);
    return EXIT_SUCCESS;	

}

