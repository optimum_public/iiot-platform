
#include "ua_pubsub.h"
#include "iiot_platform.h"
#include <pthread.h>
#include <json.h>

#define _TRUE            1
#define _FALSE           0

//UA_StatusCode clientConnectionInitialization(void *arg);
UA_StatusCode clientConnectionInitialization(request_data *requestConfig);

void *opc_ua_client(void *arg);

bool decodeResponseMessage(UA_String responseData);

UA_StatusCode writeValueToRequestIdSpecificNode(UA_Server *server, UA_String writeValue, char* nodeIdentifier);

//UA_StatusCode replyToReadRequest(UA_String value, common_data *commonPtr, request_data *requestConfig);
UA_StatusCode replyToReadRequest(char * identifier, UA_String value, common_data *commonPtr, request_data *requestConfig);

json_object * formatReadOperationMessage(UA_String readData, char *reqId);

json_object * formatMonitoredMessage(UA_String readData, char *reqId);


