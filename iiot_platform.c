
#define _GNU_SOURCE // for getline function
//#define OPC_UA_SECURITY
#include <signal.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "open62541/server.h"
#include "open62541/server_config_default.h"
#include "ua_pubsub.h"
#include "ua_network_pubsub_mqtt.h"
#include "open62541/plugin/log_stdout.h"
#include <open62541/client_config_default.h>
#include <open62541/client_highlevel.h>
#include <open62541/client_subscriptions.h>
#include <open62541/plugin/log_stdout.h>
#ifdef OPC_UA_SECURITY
	#include <open62541/plugin/securitypolicy.h>
#endif

//TO DO: may move opt also to deps folder
//#include "dep/opt.h"
#include "json.h"
#include "iiot_platform.h"
#include "optimum_mqtt_publish.h"
#include "pubsub_mqtt_subscribe.h"
#include "opc_ua_client.h"
#include "log.h"
#include "open62541/namespace_optimum_generated.h"

//TO DO add standard crane variables (Craneautomation)

#define LOG_START_MARKER            "LOG_START_FROM_THE_NEXT_LINE"
#define CONNECTION_NAME              "MQTT Subscriber Connection1"
#define TRANSPORT_PROFILE_URI        "http://opcfoundation.org/UA-Profile/Transport/pubsub-mqtt"
#define MQTT_CLIENT_ID               "IIoT_Component"
#define CONNECTIONOPTION_NAME        "mqttClientId"
#define PUBLISH_TOPIC              "topic-to-publish-to" // later set to the topic sent from the mqtt client
#define SUBSCRIBE_TOPIC				"iiot-common"
#define MODIFY_DATA_TOPIC			"iiot-modify-data"
//#define BROKER_ADDRESS_URL           "opc.mqtt://127.0.0.1:1883"
//#define BROKER_ADDRESS_URL           "opc.mqtt://127.0.0.1:"
#define BROKER_PORT           		"1883"
//set minimal limit for subscription intervall, client can request only values higher than this , 10.0 = 10 ms
#define SUBSCRIPTION_INTERVAL 5.0
#define PUBLISH_INTERVAL            500 //MQTT publish interval, value only used for pubsub configuration, every MQTT message publication is triggered via event not timing in OPTIMUM case
#define CLOCKS_PER_MILI_SEC CLOCKS_PER_SEC/1000
#define CLOCKS_PER_MICRO_SEC CLOCKS_PER_MILI_SEC/1000
#define APP_LOGGING
#define REQUEST_IIOT_READ 1
#define REQUEST_IIOT_METHOD 2
#define REQUEST_COMPONENT 3
#define REQUEST_IIOT_READ_SUBSCRIPTION 4
#define REQUEST_IIOT_READ_SUBSCRIPTION_CANCEL 5
#define REQUEST_ALL 10
#define RESPONSE 20
#define UNKNOWN 100
#define TYPE_DOUBLE 11
#define TYPE_FLOAT 10
#define TYPE_INTEGER 27
#define TYPE_DECIMAL 50
#define TYPE_STRING 12
#define REQUEST_ID_PREFIX "iiot"
#define METHOD_GOTO 1
#define Method_ComeTo 2
#define Method_Follow 3
long reqIdCount = 0;
bool optimumPlainMsg = true; // configure pubsub stack of open62541 to send MQTT payload plain withaout any additional JSON encoded OPC UA metadata (since receiver is not an OPC UA endpoint) 
// ! TODO: define g_appConfig locally, currently requesting coe and replying code share this variable! DANGEROUS!
//UA_AppConfig g_appConfig;


//UA_AppClient opcClient;
long g_message_count;

pid_t clientPid;
//pthread_t clientThread;
int rc, iQos;
bool result[2] = {true, false}; 

char clientArguments[200];
int clientTerminateSignal = 0;
//CList *list; 
//HashTable *table;
request_data *hashTable = NULL;
common_data common;


//struct json_object *parsed_json,*data_object, *cmd, *args;
struct json_object *data_object, *cmd, *args;
struct json_object *request, *answerTopic, *url, *component, *request_id, *subscription;
// TODO generate arbitrary request_id, but do not repeat for a given time...

bool decodeJson(UA_ByteString *encodedBuffer);

/*static void
//dummyFunction(void *handle, const UA_NodeId nodeid, const UA_Variant *data,
//               const UA_NumericRange *range) {
dummyFunction	(UA_Server *server, const UA_NodeId *sessionId,
	                    void *sessionContext, const UA_NodeId *nodeId,
	                    void *nodeContext, const UA_NumericRange *range,
	                    const UA_DataValue *data){

   // UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "The values of PositionDetector were updated");
}
*/


//-------------------------------------- Code for support functions ------------------------------------------------------
#ifdef OPC_UA_SECURITY
UA_ByteString* loadFile(char *path){
	//fileContents = UA_ByteString_new();
	UA_ByteString* fileContents= malloc( sizeof(UA_ByteString));
	fileContents->length = 0;
	fileContents->data = NULL;

	/* Open the file */
	//printf("loadFile path: %s\n", path);
	FILE *fp = fopen(path, "rb");
	if (!fp)
	{
		errno = 0; /* We read errno also from the tcp layer... */
		//printf("loadFile: couldn't open file with given path\n");
		return fileContents;
	}

	/* Get the file length, allocate the data and read */
	fseek(fp, 0, SEEK_END);
	fileContents->length = (size_t)ftell(fp);
	fileContents->data = (UA_Byte *)UA_malloc(fileContents->length * sizeof(UA_Byte));
	if (fileContents->data)
	{
		fseek(fp, 0, SEEK_SET);
		size_t read = fread(fileContents->data, sizeof(UA_Byte), fileContents->length, fp);
		if (read != fileContents->length)
		{
			UA_ByteString_clear(fileContents);
		}
	}
	else
	{
		//fileContents->length = 0;
		//printf("loadFile: file seems empty\n");
	}
	fclose(fp);

	return fileContents;
}
#endif

void free_UA_AppClient(request_data *requestConfig){
	
	
	
	
	int i;
	//free (opcClientStruct->mode);
	
	//free (opcClientStruct->reqId);
	
	
	// TODO: be careful currently some of the appConfig content is shared with local opc ua server. First separate these dependencies then free it here
	/*free (opcClientStruct->appConfig->publishTopic);
	free (opcClientStruct->appConfig->subscribeTopic);
	free (opcClientStruct->appConfig->server);
	free (opcClientStruct->appConfig->arguments);
	free (opcClientStruct->appConfig->operation);
	free (opcClientStruct->appConfig->reqId);
	free (opcClientStruct->appConfig);*/
	if (requestConfig->reqId != NULL)
		free(requestConfig->reqId);
	if (requestConfig->client != NULL)
		 UA_Client_delete(requestConfig->client);
	if (requestConfig->url != NULL)
		free (requestConfig->url);
	if (requestConfig->payload_data != NULL)
		free (requestConfig->payload_data);
	if (requestConfig->payload_dataArray != NULL){
		for (i=0; i< requestConfig->payload_dataArraySize; i++){
				free(requestConfig->payload_dataArray[i]);
			}
			free(requestConfig->payload_dataArray);
		}
	if (requestConfig->data != NULL)
		free (requestConfig->data);
	if (requestConfig->component != NULL)
		free (requestConfig->component);
	if (requestConfig->subscription != NULL)
		free (requestConfig->subscription);
	if (requestConfig->command != NULL)
		free (requestConfig->command);
	if (requestConfig->method_id != NULL)
		free (requestConfig->method_id);
	if (requestConfig->subscribedArraySize > 0){
		if (requestConfig->subscribedValuesArray != NULL){
			for (i=0;i<requestConfig->subscribedArraySize;i++){
				if (requestConfig->subscribedValuesArray[i] != NULL){
					UA_String_delete(requestConfig->subscribedValuesArray[i]);
				}
			}
			free(requestConfig->subscribedValuesArray);
		}
		if (requestConfig->subscribedVariablesArray != NULL){
			for (i=0;i<requestConfig->subscribedArraySize;i++){
				if (requestConfig->subscribedVariablesArray[i] != NULL){
					free(requestConfig->subscribedVariablesArray[i]);
				}
			}
			free(requestConfig->subscribedVariablesArray);
		}
	}
	if (requestConfig->val != NULL){	
		UA_Variant_delete(requestConfig->val);
	}

	free(requestConfig->publisher.publishTopic);
	UA_NodeId_clear(&requestConfig->publisher.publishedDataSetIdent);
	UA_NodeId_clear(&requestConfig->publisher.writerGroupIdentifier);
	UA_NodeId_clear(&requestConfig->publisher.DataSetWriterIdentifier);
	//free(requestConfig->publisher);

	free(requestConfig);
}

/*
 * @brief Adds a pubsub connection
 *
 * @param appConfig reference to global appConfig struct
 * @param networkAddressUrl newtork url to connect to
 */
static UA_StatusCode
//addPubSubConnection(UA_AppConfig *appConfig, UA_NetworkAddressUrlDataType *networkAddressUrl) {
addPubSubConnection(common_data *common) {
    /* Details about the connection configuration and handling are located
     * in the pubsub connection tutorial */
    UA_PubSubConnectionConfig connectionConfig;
    memset(&connectionConfig, 0, sizeof(connectionConfig));
    connectionConfig.name = UA_STRING(CONNECTION_NAME);
    connectionConfig.transportProfileUri = UA_STRING(TRANSPORT_PROFILE_URI);
    connectionConfig.enabled = UA_TRUE;

    /* configure address of the mqtt broker (local on default port) */
    UA_Variant_setScalar(&connectionConfig.address, &common->mqttBrokerURL,
                         &UA_TYPES[UA_TYPES_NETWORKADDRESSURLDATATYPE]);
    /* Changed to static publisherId from random generation to identify
     * the publisher on Subscriber side */
    connectionConfig.publisherId.numeric = 2233;

    /* configure options, set mqtt client id */
    UA_KeyValuePair connectionOptions[1];
    connectionOptions[0].key = UA_QUALIFIEDNAME(0, CONNECTIONOPTION_NAME);
	  srand(time(NULL));
    int random = rand();
	random = rand();
    char *clientName = malloc(snprintf(NULL, 0,"%s%d", MQTT_CLIENT_ID, random)+1);
    sprintf(clientName, "%s%d", MQTT_CLIENT_ID, random);
    //UA_String mqttClientId = UA_STRING(MQTT_CLIENT_ID);
	//UA_String mqttClientId = UA_STRING(clientName);
	UA_String mqttClientId = UA_String_fromChars(clientName);
	free(clientName);
    UA_Variant_setScalar(&connectionOptions[0].value, &mqttClientId, &UA_TYPES[UA_TYPES_STRING]);
	connectionConfig.connectionProperties = connectionOptions;
    connectionConfig.connectionPropertiesSize = 1;
    UA_StatusCode ret = UA_Server_addPubSubConnection(common->server, &connectionConfig, &common->connectionIdent);
	UA_String_clear(&mqttClientId);
	return ret;
}
/*
 * event based publiation trigger for pub sub connection ( acitvates a writergroup directly only once, also if it is in disabled mode (timing based triggering))
 */
void pubSubNotificationTrigger(common_data *common, publisher_data *publisher){
	UA_WriterGroup *wg = UA_WriterGroup_findWGbyId(common->server, publisher->writerGroupIdentifier);
	wg->config.publishingInterval = 0.0;
    if(!wg){
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "failed to trigger publishCallback to publish data via pub sub connection");
	}else{
		UA_Server_setWriterGroupOperational(common->server, publisher->writerGroupIdentifier);
		//UA_WriterGroup_publishCallback(common->server, wg);
		UA_Server_setWriterGroupDisabled(common->server, publisher->writerGroupIdentifier);
		//UA_WriterGroup_publishCallback(common->server, wg);

		//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "The value of writer group writers count: %d", wg->writersCount);
	}
}

// static UA_StatusCode
// addDeviceStatusVariable(UA_Server *server) {
// 	UA_StatusCode result;
//     /* Define the attribute of the myInteger variable node */
//     UA_VariableAttributes attr = UA_VariableAttributes_default;
// 	attr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
//     UA_String deviceStatus = UA_STRING("IDLE");
//     UA_Variant_setScalar(&attr.value, &deviceStatus, &UA_TYPES[UA_TYPES_STRING]);
//     attr.description = UA_LOCALIZEDTEXT("en-US","Device Status");
//     attr.displayName = UA_LOCALIZEDTEXT("en-US","Device Status");
//     attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
//     attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

//     /* Add the variable node to the information model */
//     UA_NodeId myStringNodeId = UA_NODEID_STRING(1, "device_status");
//     UA_QualifiedName myStringName = UA_QUALIFIEDNAME(1, "Device Status");
//     UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
//     UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
//     result = UA_Server_addVariableNode(server, myStringNodeId, parentNodeId,
//                               parentReferenceNodeId, myStringName,
//                               UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
// 	return result;
// }

/*
 * @brief stops the main thread
 *
 * @param sign type of signal received
 */
UA_Boolean running = true;
static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
	//pthread_join(clientThread, NULL);
	//pthread_cancel(clientThread);
	//pthread_exit(NULL);
    running = false;
	//UA_Server_delete(common.server);
	//#ifdef OPC_UA_SECURITY
	//	free (serverCertPath);
	//	free (serverPrivateKeyPath);
	//#endif
	exit(0);
}

void free_payload_elements(optimum_msg *payload, UA_Boolean error){
		if (payload->answer_topic != NULL)
			free(payload->answer_topic);
		if (payload->command != NULL)
			free(payload->command);
		if (payload->data != NULL)
			free(payload->data);
		if (payload->dataArray != NULL && error){
			int i;
			for (i=0; i< payload->dataArraySize; i++){
				free(payload->dataArray[i]);
			}
			free(payload->dataArray);
		}
		if (payload->device != NULL)
			free(payload->device);
		if (payload->final_msg != NULL)
			free(payload->final_msg);
		if (payload->method_id != NULL)
			free(payload->method_id);
		if (payload->req_id != NULL)
			free(payload->req_id);
		if (payload->status != NULL)
			free(payload->status);
		if (payload->subscription != NULL)
			free(payload->subscription);
		if (payload->component != NULL)
			free(payload->component);
	}

// hashtable wrapper function
request_data *find_entry(request_data *hashTable, char *reqId) {
    request_data *hashEntry;

    HASH_FIND_STR( hashTable, reqId, hashEntry);
    return hashEntry;
}

char* safeStringCopy (char *source)
{
	if (source != NULL)
		{
			return(strdup(source));
		}
		else
		{
			return(NULL);
		}
}


// derived from https://stackoverflow.com/questions/12700497/how-to-concatenate-two-integers-in-c
unsigned concatenate_uint(unsigned x, unsigned y) {
    unsigned pow = 10;
    while(y >= pow)
        pow *= 10;
    return x * pow + y;        
}

unsigned numeric_id(char *id){
	return concatenate_uint((unsigned) id[0],atoi(&id[1]));
}

/*	Node specific to each request made from the opc client from another optimum device	*/
static UA_StatusCode addRequestIdSpecificNode(UA_Server *server, char *id)
{
	UA_StatusCode retval;

    /* Define the attribute of the myString variable node */
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_String myDefaultString = UA_STRING("req_id node");
    UA_Variant_setScalar(&attr.value, &myDefaultString, &UA_TYPES[UA_TYPES_STRING]);
    attr.description = UA_LOCALIZEDTEXT("en-US","RequestSpecificIDNode");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","RequestSpecificIDNode");
    attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable node to the information model */
    UA_NodeId myRequestStringNodeId = UA_NODEID_STRING(1, id);
    UA_QualifiedName myStringName = UA_QUALIFIEDNAME(1, "RequestSpecificIDNode");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    retval = UA_Server_addVariableNode(server, myRequestStringNodeId, parentNodeId,
                              parentReferenceNodeId, myStringName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                              attr, NULL, NULL);

    return retval;
}

//TODO: overloaded function call, merging possible?
/*	Node specific to each request made from the opc client from optimum device 1	*/
static UA_StatusCode addRequestSpecificNode(UA_Server *server, char *id, char *request)
{
	
	UA_StatusCode result;
    /* Define the attribute of the myString variable node */
    UA_VariableAttributes attr = UA_VariableAttributes_default;
	//! why putting the request data into the answer node? because anser node and forward node (to target component) are the same!
    UA_String myDefaultString = UA_STRING(request);

    UA_Variant_setScalar(&attr.value, &myDefaultString, &UA_TYPES[UA_TYPES_STRING]);
   
	attr.description = UA_LOCALIZEDTEXT("en-US","RequestSpecificNode");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","RequestSpecificNode");
    attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable node to the information model */
    UA_NodeId myRequestStringNodeId = UA_NODEID_STRING(1, id);
    UA_QualifiedName myStringName = UA_QUALIFIEDNAME(1, id);
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    result = UA_Server_addVariableNode(server, myRequestStringNodeId, parentNodeId,
                              parentReferenceNodeId, myStringName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                              attr, NULL, NULL);
 	//UA_String_delete(&myDefaultString);
    return result;
}

/* UA_StatusCode
writeValueToRequestIdSpecificNode(UA_Server *server, UA_String writeValue, char* reqId) {
    UA_NodeId responseNodeId = UA_NODEID_STRING(1, reqId);

    UA_Variant variant;
    UA_Variant_init(&variant);
    UA_Variant_setScalar(&variant, &writeValue, &UA_TYPES[UA_TYPES_STRING]);
    return UA_Server_writeValue(server, responseNodeId, variant);	
} */

void decodeJsonMsg(UA_ByteString *encodedBuffer, optimum_msg *payload){
	struct json_object *parsed_json;
	//char  buffer[strlen((char*)encodedBuffer->data)];
	//strncpy(buffer, (char*)encodedBuffer->data, strlen((char*)encodedBuffer->data));
	//parsed_json = json_tokener_parse(buffer);	
	// TODO: is the temp buffer really required? trying it without buffer:
	// create a NULL-termintated string for strtok
	char *buffer = malloc(encodedBuffer->length+1);
	memcpy(buffer, encodedBuffer->data, encodedBuffer->length);
	buffer[encodedBuffer->length] = '\0';
	parsed_json = json_tokener_parse(buffer);	
	free(buffer);
/*	if(json_object_object_get_ex(parsed_json, "mode", &request) 
		&& json_object_object_get_ex(parsed_json, "data", &data_object) 
		&& json_object_object_get_ex(parsed_json, "answer_topic", &answerTopic)
		&& json_object_object_get_ex(parsed_json, "device", &url)
		&& json_object_object_get_ex(parsed_json, "req_id", &request_id)){ 
		return result[0];
	}
	else{
		return result[1];
	}	
*/
	struct json_object *temp;
	if (json_object_object_get_ex(parsed_json, "answer_topic", &temp)){	// this is the answer topic every request to the IIoT-Platform has to contain to be able to send the response
		payload->answer_topic = strdup(json_object_get_string(temp)); 
	}else{
		payload->answer_topic = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "req_id", &temp)){		//arbitrary id to match responses with initial request
		payload->req_id = strdup(json_object_get_string(temp)); 
	}else{
		payload->req_id = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "device", &temp)){		// URL/IP of target device that can answer the request
		payload->device = strdup(json_object_get_string(temp)); 
	}else{
		payload->device = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "component", &temp)){
		payload->component = strdup(json_object_get_string(temp)); 
	}else{
		payload->component = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "command", &temp)){		
		payload->command = strdup(json_object_get_string(temp)); 
	}else{
		payload->command = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "method_id", &temp)){		
		payload->method_id = strdup(json_object_get_string(temp)); 
	}else{
		payload->method_id = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "subscription", &temp)){	// enable or disable OPC UA subscription feature
		payload->subscription = strdup(json_object_get_string(temp)); 
	}else{
		payload->subscription = NULL; 
	}	

	if (json_object_object_get_ex(parsed_json, "final_msg", &temp)){	// enable or disable OPC UA subscription feature
		payload->final_msg = strdup(json_object_get_string(temp)); 
	}else{
		payload->final_msg = NULL; 
	}

	if (json_object_object_get_ex(parsed_json, "status", &temp)){	// enable or disable OPC UA subscription feature
		payload->status = strdup(json_object_get_string(temp)); 
	}else{
		payload->status = NULL; 
	}
	
	if (json_object_object_get_ex(parsed_json, "data", &temp)){
		if (json_object_is_type(temp,json_type_array)){
			payload->dataArraySize = json_object_array_length(temp);
			int i;
			//struct array_list *list = json_object_get_array(temp);
			char **charPointerArray;
			charPointerArray = malloc(payload->dataArraySize*sizeof(char *));
			//char *charPointerArray[] = malloc(payload->dataArraySize*sizeof(char*));
			for (i = 0; i < payload->dataArraySize; i++){
				char *entry = strdup(json_object_get_string(json_object_array_get_idx(temp, i)));
				charPointerArray[i] = entry;
			}
			payload->dataArray = charPointerArray;
		}else{
			payload->data = strdup(json_object_get_string(temp));
			payload->dataArraySize = 0; 
			payload->dataArray = NULL;
		}
	}else{
		payload->data = NULL; 
		payload->dataArraySize = 0; 
		payload->dataArray = NULL;
	}	

	if (json_object_object_get_ex(parsed_json, "sub_id", &temp)){	// enable or disable OPC UA subscription feature
		payload->sub_id = (UA_UInt32) json_object_get_int(temp); 
	}else{
		payload->sub_id = 0; 
	}	
	json_object_put(parsed_json);
}

char *mergeStrings(const char *string1,const  char *string2){
	// adapted from https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c
	const size_t len1 = strlen(string1);
	const size_t len2 = strlen(string2);
	char *merged = malloc(len1 + len2 + 1);
	memcpy(merged, string1, len1);
	memcpy(merged +  len1 , string2, len2 + 1);
	return merged;
}

//-------------------------------------- Code to offer service/method call for other OPTIMUM devices (Gerneral Purpose Method Call) ------------------------

// static void
// addValueCallbackToResponseNode(UA_Server *server) {
//     UA_ValueCallback callback ;
// 	callback.onRead = beforeReadPosition;
//     callback.onWrite = afterWriteToResponseNode;
// 	UA_NodeId responseNodeIdentifier = UA_NODEID_STRING(1, identifier); //responseNode
	
// 	UA_StatusCode callbackResult;
//     callbackResult = UA_Server_setVariableNode_valueCallback(server, responseNodeIdentifier, callback);
// 	if (callbackResult != UA_STATUSCODE_GOOD) {
// 		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Call Back on: %s Error Name = %s",
// 		identifier, UA_StatusCode_name(callbackResult));
// 	}
// 	else{
// 		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Adding Call Back Function: %s %s",
// 		identifier, UA_StatusCode_name(callbackResult));
// 	}
// }

//  static UA_StatusCode addresponseNode(UA_Server *server, UA_String outputStr, char *identifier)
// {
// 	UA_StatusCode result;

//     /* Define the attribute of the myString variable node */
//     UA_VariableAttributes attr = UA_VariableAttributes_default;
//     UA_String myString = outputStr;
//     UA_Variant_setScalar(&attr.value, &myString, &UA_TYPES[UA_TYPES_STRING]);
//     attr.description = UA_LOCALIZEDTEXT("en-US","ResponseNode");
//     attr.displayName = UA_LOCALIZEDTEXT("en-US","ResponseNode");
//     attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
//     attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

//     /* Add the variable node to the information model */
//     UA_NodeId myStringNodeId = UA_NODEID_STRING(1, identifier);
//     UA_QualifiedName myStringName = UA_QUALIFIEDNAME(1, "ResponseNode");
//     UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
//     UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
//     result = UA_Server_addVariableNode(server, myStringNodeId, parentNodeId,
//                               parentReferenceNodeId, myStringName,
//                               UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
//                               attr, NULL, NULL);
	
// 	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addresponseNode Result = %s\n",UA_StatusCode_name(result));
// 	addValueCallbackToResponseNode(server); // adding callback method to the responseNode "Y22691"

//     return result;
// }
 /*
  *@Brief: send MQTT msg to broker
  * 
  */
 void *opc_ua_pubsub_api(void *arg){
	request_data *requestConfig;
	int returnValue;
	//g_appConfig1 = (UA_AppConfig*) malloc(sizeof(UA_AppConfig));
    requestConfig = (request_data*)arg; 

	// TODO: the malloc seems to be useless here
	
	returnValue = setup_publish_message(&common, requestConfig);
	if (returnValue != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "setup_publish_message: Error Name = %s",
			UA_StatusCode_name(returnValue));
	}
	//sleep(1); // workaround to create a event-based mqtt publsiher based on timer-based publish function in open62541
	// DONE: find better or at least more reliable solution
	//UA_Server_setWriterGroupDisabled(pubsubConfig->server, pubsubConfig->writerGroupIdentifier);
	pubSubNotificationTrigger(&common, &requestConfig->publisher);


	// --- 3) change value of request node to status information abput pending request
	//returnValue = addRequestSpecificNode(requestConfig->server, requestConfig->reqId, mqttPayloadString);
	// TODO: check if escape symbols used in the right way, check msg syntax on remote device
	UA_String requestStatus = UA_STRING("{\"status\": \"pending\", \"final_msg\": \"false\", \"data\": \"forwarding request to target component\"}");
	returnValue = writeValueToRequestIdSpecificNode(common.server, requestStatus, requestConfig->reqId);
    free_UA_AppClient(requestConfig);
	return EXIT_SUCCESS;	
}
/*
 * @Brief: create response node for requesting device in OPC UA address space AND to allow pub sub extension to forward request to target component!
 * 1) First fill node value with request data,
 * 2) activate writer group to send msg to broker and then disable writer group
 * 3) insert msg for remote/requesting device 
 * 4) answer remote device with ack and name of node to allow remote device to subscribe to node
 * 5) set configure pub sub extension callbacks to forward respone of target component to node 
 */
static UA_StatusCode GPRequestCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {
	
	UA_String *inputStr = (UA_String*)input->data;	
	//struct json_object *component_name;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;
/*#ifdef OPC_UA_SECURITY
	requestConfig->serverCert =  UA_BYTESTRING_NULL;
	requestConfig->serverPrivateKey =  UA_BYTESTRING_NULL;
	requestConfig->clientCert =  UA_BYTESTRING_NULL;
	requestConfig->clientPrivateKey =  UA_BYTESTRING_NULL;
#endif */
	
	/*returnValue = addresponseNode(server, outputStr, identifier); // create a responseNode on the OPC-UA Server
	
	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "commondata: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return EXIT_FAILURE;
	}*/
	 
	if (strcmp((char *)inputStr->data, "terminateClient") == 0)
	{
		clientTerminateSignal = 1;
	}		
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Input Arguments in MethodCallBack Method%s\n", inputStr->data);
	

	// --- 1.1) prepare message to be published via MQTT and received by target component
	// TODO: check if received request fullfulls minimum requirements --> UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Invalid format of Input Arguments passed to the method call\n");
	//char *ptr = strtok((char *)inputStr->data, ";");
	//char * data_content = ptr;
	//ptr = strtok(NULL, ";");
	//reqId = ptr;
	//g_appConfig.reqId = ptr;
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Data %s, Id %s \n",data_content, g_appConfig.reqId);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received JSON payload: %s", (char *)inputStr->data);

	//struct json_object *parsed_json = json_tokener_parse(data_content);
	struct json_object *parsed_json = json_tokener_parse((char *)inputStr->data);
	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	//char *componentName;

	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the responde node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//g_appConfig.reqId = strdup(identifier);
	//TODO free reqId

	// ! TODO: seems to be important, seems to change some internal pointers to NOT NULL, investigate who has intizialized these pointers. Maybe still wrong 
	//requestConfig = g_appConfig;
	//requestConfig->connectionIdent = g_appConfig.connectionIdent;
	//if (json_object_object_get_ex(parsed_json, "command", &temp))
	//{
	//	json_object_object_add(mqttPayload, "command", temp);
	//}
	UA_Boolean dataMissing = UA_FALSE;
	if (json_object_object_get_ex(parsed_json, "data", &temp))
	{
		json_object_object_add(mqttPayload, "data", temp);
	} else {
		dataMissing = UA_TRUE;
	}

	if (json_object_object_get_ex(parsed_json, "component", &temp))
	{
		//componentName = strdup(json_object_get_string(temp));
		requestConfig->publisher.publishTopic = mergeStrings(json_object_get_string(temp), "-common");
		// TODO: free publishTopic
	} else {
		dataMissing = UA_TRUE;
	}

	if (dataMissing){
		UA_String errorString = UA_STRING("{\"status\":\"nack\",\"final_msg\":\"true\",\"info\":\"One or both key-value pairs 'component' and 'data' are missing\"}");
		// --- send method response with output argument to requesting device
		UA_Variant_setScalarCopy(output, &errorString, &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GPRequestCallback: Received a gerneral-purpose method call (Method_JSON) with wrong format. One or both key-value pairs 'component' and 'data' are missing.");
		return EXIT_FAILURE;
	}
	/*else
	{
		componentName = NULL;
	}*/

	//requestConfig->reqId = iiotToXReqId;
	//requestConfig->reqId_numeric = numeric_id(requestConfig->reqId);
	//requestConfig->server = server;

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
    //requestConfig->subscribeTopic = (char*)malloc(sizeof(char)*strlen(SUBSCRIBE_TOPIC)); // required?
    //strcpy(requestConfig->subscribeTopic, SUBSCRIBE_TOPIC); // requried?
    //requestConfig->arguments = (char*)malloc(sizeof(char)*strlen(clientArguments)); // not used!?
	//requestConfig->subscribeTopic = strdup(SUBSCRIBE_TOPIC);

	/* 	if(json_object_object_get_ex(parsed_json, "cmd", &cmd) 
			&& json_object_object_get_ex(parsed_json, "data", &args)
			&& json_object_object_get_ex(parsed_json, "component", &component_name)
			&& strlen(g_appConfig.reqId) > 0){ 
			json_object *jobj_data = json_object_new_object();
			json_object *jobj_final = json_object_new_object();
			json_object *jStr1 = json_object_new_string((char *)SUBSCRIBE_TOPIC);
			json_object *jstr2 = json_object_new_string((char *)identifier);
			json_object *jstr3 = json_object_new_string(json_object_get_string(cmd));
			json_object *jstr4 = json_object_new_string(json_object_get_string(args));
			json_object *jstr5 = json_object_new_string(json_object_get_string(component_name));
			json_object_object_add(jobj_final,"answer_topic", jStr1 );
			json_object_object_add(jobj_final,"req_id", jstr2 );
			json_object_object_add(jobj_data,"component", jstr5 );
			json_object_object_add(jobj_data,"cmd", jstr3 );
			json_object_object_add(jobj_data,"data", jstr4);
			
			json_object_object_add(jobj_final,"data", jobj_data ); */
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	//returnValue = addRequestSpecificNode(g_appConfig.server, g_appConfig.reqId, strdup(json_object_get_string(mqttPayload)));
	int returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	//int returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, "{ \"answer_topic\": \"iiot-common\", \"req_id\": \"Y22693\", \"command\": \"init_skynet\", \"data\": \"test\" }");
	//int returnValue = addRequestSpecificNode(requestConfig->server, requestConfig->reqId, "{\"status\": \"pending\", \"final_msg\": \"false\", \"data\": \"forwarding request to target component--\"}");

	
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	//pthread_t clientThread;
	//int rc = pthread_create(&clientThread, NULL, opc_ua_pubsub_api, (void *)&g_appConfig);
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	//int rc = pthread_create(&clientThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);

	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	//else
	//{

		// not required here
		
		// // add thread (realising one instance of OPC UA client) to list and match it to request id
		//// clientList *tempClient = malloc(sizeof(clientList));
		//// tempClient->req_id = strdup(requestConfig->reqId);
		//// tempClient->client = tempThread;
		//// bool *tempTerminateSignal = malloc(sizeof(bool));
		//// (tempTerminateSignal) = UA_FALSE; // or *tempTerminateSignal?
		//// tempClient->terminateSignal = tempTerminateSignal;
		//// list->add(list, &tempClient);  

	//}
	// --- disabled thread for test purpose and use function to publish msg directly
	// TODO: check implications of this change
	
	// returnValue = setup_publish_message(&requestConfig, requestConfig->reqId);
	// //g_appConfig.reqId = identifier;
	// //returnValue = setup_publish_message(&g_appConfig, requestConfig->reqId);
	// if (returnValue != UA_STATUSCODE_GOOD) {
	// 		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "setup_publish_message: Error Name = %s",
	// 		UA_StatusCode_name(returnValue));
	// }
	
	// sleep(1); // workaround to create a event-based mqtt publsiher based on timer-based publish function in open62541
	// // TODO: find better or at least more reliable solution
	// //UA_Server_setWriterGroupDisabled(requestConfig->server, requestConfig->writerGroupIdentifier);
	// //UA_Server_setWriterGroupDisabled(g_appConfig.server, g_appConfig.writerGroupIdentifier);

	//moved to thread
	// // --- 3) change value of request node to status information abput pending request
	// //returnValue = addRequestSpecificNode(requestConfig->server, requestConfig->reqId, mqttPayloadString);
	// // TODO: check if escape symbols used in the right way, check msg syntax on remote device
	// UA_String requestStatus = UA_STRING("{\"status\": \"pending\", \"final_msg\": \"false\", \"data\": \"forwarding request to target component\"}");
	// returnValue = writeValueToRequestIdSpecificNode(requestConfig->server, requestStatus, requestConfig->reqId);
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
#define COMPONENT_RESPONSE "{\"status\":\"ack\",\"final_msg\":\"false\",\"info\":\"Further information available via subscription to response_node\",\"response_node_ns\":\"1\",\"response_node\":\""
	char dummy1[sizeof(COMPONENT_RESPONSE)+sizeof("\"}")+1] = COMPONENT_RESPONSE;
	strcat(dummy1, iiotToXReqId);
	char dummy2[] = "\"}";
	strcat(dummy1, dummy2);
	UA_String outputStr = UA_STRING(dummy1);
	//int returnValue;

	// --- 4.2) send method response with output argument to remote device
	// TODO: does this simple copy function really trigger the response mechanism?
	returnValue = UA_Variant_setScalarCopy(output, &outputStr, &UA_TYPES[UA_TYPES_STRING]);
	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}


//-------------------------------------- Semi-Autonomous Methods -------------------------------------------------------------------

/*static UA_StatusCode
 GoToMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

    UA_String *inputStr = (UA_String*)input->data;
    char* coordinates = (char*)UA_malloc(sizeof(char)*inputStr->length+1);
    memcpy(coordinates, inputStr->data, inputStr->length);
    coordinates[inputStr->length] = '\0';

    //assuming coordinates are transmitted as a string separated by ;
    char *ptr = strtok(coordinates, ";");
    int32_t PosX = atoi(ptr);
    ptr = strtok(NULL, ";");
    int32_t PosY = atoi(ptr);
        
    //sprintf(clientArguments, "%s;%d;%d;%d",  "dummyurl", GOTO, PosX, PosY);
    //pthread_create(&clientThread, NULL, opcUaClient, clientArguments);
	// TODO: build json string that contains all required information and send it to dcp 
	//pthread_t *tempThread = malloc(sizeof(pthread_t));
	//int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);
	//if (rc)
//	{
//		printf("ERR; pthread_create() ret = %d\n", rc);
//		exit(-1);
//	}

	// TODO: currently only the input values are copied as output argument, in future: give a resonable answer or return a respone node id where the client can get further details on the current status of the triggered action.
    UA_Variant_setScalarCopy(output, inputStr, &UA_TYPES[UA_TYPES_STRING]);
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Method GoToLocation was called");
    return UA_STATUSCODE_GOOD;
} */

/*
 * @Brief: create response node for requesting device in OPC UA address space AND to allow pub sub extension to forward request to target component!
 * 1) First fill node value with request data,
 * 2) activate writer group to send msg to broker and then disable writer group
 * 3) insert msg for remote/requesting device 
 * 4) answer remote device with ack and name of node to allow remote device to subscribe to node
 * 5) set configure pub sub extension callbacks to forward respone of target component to node 
 */
static UA_StatusCode 
genericCallback(const UA_Variant *input, UA_Variant *output, UA_Int16 method){
	//UA_String *inputStr = (UA_String*)input->data;	
	//struct json_object *component_name;
	int returnValue = 0;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;
	

	// --- 1.1) prepare message to be published via MQTT and received by target component
	// TODO: check if received request fullfulls minimum requirements --> UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Invalid format of Input Arguments passed to the method call\n");
	//char *ptr = strtok((char *)inputStr->data, ";");
	//char * data_content = ptr;
	//ptr = strtok(NULL, ";");
	//reqId = ptr;
	//g_appConfig.reqId = ptr;
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Data %s, Id %s \n",data_content, g_appConfig.reqId);
	//UA_String *deviceAddress = (UA_String*)input[0].data;	
	UA_String *hmiID = (UA_String*)input[0].data;
	UA_String *target = (UA_String*)input[1].data;	
	UA_String *masterDevice = (UA_String*)input[2].data;	
	UA_String *slaveDevices = (UA_String*)input[3].data;	
	UA_String *masterTrolley = (UA_String*)input[4].data;	
	UA_String *slaveTrolleys = (UA_String*)input[5].data;	

	//in case of FollowMe, check if this is the subscription cancel request
	// no, DCP needs to be informed to stop followme -> DCP tells IIoT to cancel target location subscription
	// if (method == Method_Follow && (strcmp(target->data, "terminateClient") == 0)){
	// 	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received cancel request for Method_Follow");
	// 	clientTerminateSignal = 1;
	// 	UA_String stringtmp = UA_String_fromChars("ok");
	// 	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	// 	//Output Information 
	// 	stringtmp = UA_String_fromChars("Canceled");
	// 	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
		
	// 	if (returnValue != UA_STATUSCODE_GOOD) {
	// 		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: creation of method output arguments failed: Error Name = %s", UA_StatusCode_name(returnValue));
	// 	return returnValue;
	// }
	// }

	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received JSON payload: %s", (char *)inputStr->data);

	//struct json_object *parsed_json = json_tokener_parse(data_content);
	//struct json_object *parsed_json = json_tokener_parse((char *)inputStr->data);
	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	struct json_object *data = json_object_new_object();
	struct json_object *arguments = json_object_new_object();
	struct json_object *position = json_object_new_object();
	struct json_object *slaveDeviceArray = json_object_new_array();
	struct json_object *slaveTrolleyArray = json_object_new_array();
	//char *componentName;
	UA_Boolean dataMissing = UA_FALSE;

	// Add answer_topic to JSON object
	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the responde node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//TODO free reqId

	// Add device address to JSON object
	//temp = json_object_new_string((char *) deviceAddress);
	//json_object_object_add(mqttPayload, "device", temp);

	// Add targert component to JSON object
	//temp = json_object_new_string((char *) "dcp");
	//json_object_object_add(mqttPayload, "component", temp);

	//create sub structure data
	//--- Add HMI ID to JSON object
	if (hmiID->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument HMI_ID.");
	}else{
		//temp = json_object_new_string((char *) hmiID->data);
		temp = json_object_new_string_len((char *) hmiID->data, hmiID->length);
		json_object_object_add(data, "dev_id", temp);
	}

	//--- Add command to JSON object
	switch(method){
		case METHOD_GOTO:
			temp = json_object_new_string((char *) "gotoposition");
			break;
		case Method_ComeTo:
			temp = json_object_new_string((char *) "cometo");
			break;
		case Method_Follow:
			temp = json_object_new_string((char *) "follow");
			break;
		default:
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: unknown method identifier: %d", method);
			return UA_STATUSCODE_BADOUTOFRANGE;
	}
	json_object_object_add(data, "cmd", temp);

	//--- create sub sub structure arguments
	//------ Add master device
	if (masterDevice->length== 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument masterDevice.");
	}else{
		//temp = json_object_new_string((char *) masterDevice->data);
		temp = json_object_new_string_len((char *) masterDevice->data, masterDevice->length);
		json_object_object_add(arguments, "master", temp);
	}

	//------ Add slave devices
	if (slaveDevices->length == 0){
		//no slave 
		temp = json_object_new_string("");
		json_object_array_add(slaveDeviceArray, temp);
	}else{
		char *slave = strtok((char *) slaveDevices->data, ",");
		//one or multiple slaveDeviceArray
		while(slave != NULL){
			temp = json_object_new_string((char *) slave);
			json_object_array_add(slaveDeviceArray, temp);
			slave = strtok(NULL, ",");
		}
	}
	json_object_object_add(arguments, "slaves", slaveDeviceArray);
	

	//------ Add master trolley
	if (masterTrolley->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument masterTrolley.");
	}else{
		temp = json_object_new_string_len((char *) masterTrolley->data, masterTrolley->length);
		json_object_object_add(arguments, "trolley_master", temp);
	}

	//------ Add slave trolleys
	if (slaveTrolleys->length == 0){
		//no slave 
		temp = json_object_new_string("");
		json_object_array_add(slaveTrolleyArray, temp);
	}else{
		char *slaveTrolleyData = malloc(slaveTrolleys->length+1);
		memcpy(slaveTrolleyData, slaveTrolleys->data, slaveTrolleys->length);
		slaveTrolleyData[slaveTrolleys->length] = '\0';
		char *slaveTrolley = strtok(slaveTrolleyData, ",");
		//one or multiple slaveDeviceArray
		while(slaveTrolley != NULL){
			temp = json_object_new_string((char *) slaveTrolley);
			json_object_array_add(slaveTrolleyArray, temp);
			slaveTrolley = strtok(NULL, ",");
		}
		free(slaveTrolleyData);
	}
	json_object_object_add(arguments, "trolley_slaves", slaveTrolleyArray);
	//temp = json_object_new_string((char *) slaveTrolleys->data);
	//json_object_object_add(arguments, "trolley_slaveDeviceArray", temp);

	//------ create sub sub sub structure position or target_ip
	if (method == METHOD_GOTO){
		//--------- Add X
		if (target->length == 0){
			dataMissing = UA_TRUE;
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument coordinates.");
		
		}else{
			// create a NULL-termintated string for strtok
			char *allCoordiantes = malloc(target->length+1);
			memcpy(allCoordiantes, target->data, target->length);
			allCoordiantes[target->length] = '\0';
			char *coordinate = strtok(allCoordiantes, ",");
			if (coordinate == NULL){
				dataMissing = UA_TRUE;
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument x coordiante.");
			}else{
				temp = json_object_new_int(atoi((char *) coordinate));
				json_object_object_add(position, "x", temp);
			}

			//--------- Add Y
			coordinate = strtok(NULL, ",");
			if (coordinate == NULL){
				dataMissing = UA_TRUE;
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument y coordiante.");
			}else{
				temp = json_object_new_int(atoi((char *) coordinate));
				json_object_object_add(position, "y", temp);
			}

			//--------- Add Z
			coordinate = strtok(NULL, ",");
			if (coordinate == NULL){
				dataMissing = UA_TRUE;
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument z coordiante.");
			}else{
				temp = json_object_new_int(atoi((char *) coordinate));
				json_object_object_add(position, "z", temp);
				//------ add sub sub sub structure position to arguments
				json_object_object_add(arguments, "position", json_object_get(position));
			}
		free(allCoordiantes);
		}
	}else{
		//cometome or followme method
		if (target->length == 0){
			dataMissing = UA_TRUE;
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing argument target hardware address.");
		}else{
			//temp = json_object_new_string((char *) strndup(target->data, target->length));
			//json_object_to_json_string_ext(temp, JSON_C_TO_STRING_NOSLASHESCAPE);
			temp = json_object_new_string_len((char *) target->data, target->length);
			//------ add sub sub sub structure targert_ip to arguments
			json_object_object_add(arguments, "target_ip", temp);
		}
	}

	if (dataMissing){
		//UA_String errorString = UA_STRING("{\"status\":\"nack\",\"final_msg\":\"true\",\"info\":\"One or more mandatory arguments are missing\"}");
		// --- send method response with output argument to requesting device
		//UA_Variant_setScalarCopy(output, &errorString, &UA_TYPES[UA_TYPES_STRING]);
		//Output Request Status 
		UA_Variant_setScalarCopy(&output[0], "error", &UA_TYPES[UA_TYPES_STRING]);
		//Output Information 
		UA_Variant_setScalarCopy(&output[1], "One or more mandatory arguments are missing", &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "genericCallback: Received a method call with missing arguments.");
		json_object_put(data);
		json_object_put(mqttPayload);
		json_object_put(arguments);
		json_object_put(position);
		return EXIT_FAILURE;
	}

	

	//--- add sub sub structure arguments to data
	//temp = json_object_new_string(json_object_get_string(arguments));
	temp = json_object_new_string(json_object_to_json_string_ext(arguments, JSON_C_TO_STRING_NOSLASHESCAPE));
	json_object_object_add(data, "arguments", temp);

	// add sub structure data to mqttPayload
	temp = json_object_new_string(json_object_get_string(data));
	json_object_object_add(mqttPayload, "data", temp);

	// Add command component to JSON object
	//temp = json_object_new_string((char *) "methodcall");
	//json_object_object_add(mqttPayload, "command", temp);

	requestConfig->publisher.publishTopic = strdup("dcp-common");
	// TODO: free publishTopic

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
  
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));

	json_object_put(data);
	json_object_put(mqttPayload); //may enough, recursive freeing?
	json_object_put(arguments);
	json_object_put(position);

	//char *mqttPayloadString = strdup(json_object_to_json_string_ext(mqttPayload, JSON_C_TO_STRING_NOSLASHESCAPE));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);
	rc |= pthread_detach(*tempThread);
	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	free(tempThread);
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
	//#define COMPONENT_RESPONSE "{\"status\":\"ack\",\"final_msg\":\"false\",\"info\":\"Further information available via subscription to response_node\",\"response_node_ns\":\"1\",\"response_node\":\""
	//char dummy1[sizeof(COMPONENT_RESPONSE)+sizeof("\"}")+1] = COMPONENT_RESPONSE;
	//strcat(dummy1, iiotToXReqId);
	//char dummy2[] = "\"}";
	//strcat(dummy1, dummy2);
	//	UA_String outputStr = UA_STRING(dummy1);

	// --- 4.2) send method response with output argument to remote device
	//returnValue = UA_Variant_setScalarCopy(output, &outputStr, &UA_TYPES[UA_TYPES_STRING]);
	//Output Request Status 
	UA_String stringtmp = UA_STRING("ok");
	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	//UA_String_delete(&stringtmp);
	//Output Information 
	stringtmp = UA_STRING("Further status information available via subscription to Response Node");
	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	//UA_String_delete(&stringtmp);
	//Output Response Node 
	stringtmp = UA_STRING(iiotToXReqId);
	returnValue |= UA_Variant_setScalarCopy(&output[2], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	//UA_String_delete(&stringtmp);
	//Output Response Node Namespace 
	stringtmp = UA_STRING("1");
	returnValue |= UA_Variant_setScalarCopy(&output[3], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);

	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}

static UA_StatusCode 
GoToMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {
	return genericCallback(input, output, METHOD_GOTO);
}

static UA_StatusCode 
ComeToMeMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {
	return genericCallback(input, output, Method_ComeTo);
}

static UA_StatusCode 
FollowMeMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {
	return genericCallback(input, output, Method_Follow);
}

static UA_StatusCode 
StopMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

	int returnValue = 0;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;
	

	// --- 1.1) prepare message to be published via MQTT and received by target component
	
	UA_String *hmiID = (UA_String*)input[0].data;
	UA_String *function_id = (UA_String*)input[1].data;	


	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	struct json_object *data = json_object_new_object();
	struct json_object *arguments = json_object_new_object();
	//char *componentName;
	UA_Boolean dataMissing = UA_FALSE;

	// Add answer_topic to JSON object
	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the response node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//TODO free reqId

	//create sub structure data
	//--- Add HMI ID to JSON object
	if (hmiID->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "StopMethodCallback: Received a method call with missing argument HMI_ID.");
	}else{
		temp = json_object_new_string_len((char *) hmiID->data, hmiID->length);
		//temp = json_object_new_string((char *) hmiID->data);
		json_object_object_add(data, "dev_id", temp);
	}

	//--- Add command to JSON object
	temp = json_object_new_string((char *) "stop");			
	json_object_object_add(data, "cmd", temp);

	//--- create sub sub structure arguments
	if (function_id->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "StopMethodCallback: Received a method call with missing argument function_id (Request ID).");
	}else{
		temp = json_object_new_string_len((char *) function_id->data, function_id->length);
		//temp = json_object_new_string((char *) function_id->data);
		json_object_object_add(arguments, "function_id", temp);
	}
	

	if (dataMissing){
		//UA_String errorString = UA_STRING("{\"status\":\"nack\",\"final_msg\":\"true\",\"info\":\"One or more mandatory arguments are missing\"}");
		// --- send method response with output argument to requesting device
		//UA_Variant_setScalarCopy(output, &errorString, &UA_TYPES[UA_TYPES_STRING]);
		//Output Request Status 
		UA_Variant_setScalarCopy(&output[0], "error", &UA_TYPES[UA_TYPES_STRING]);
		//Output Information 
		UA_Variant_setScalarCopy(&output[1], "One or more mandatory arguments are missing", &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "StopMethodCallback: Received a method call with missing arguments.");
		return EXIT_FAILURE;
	}

	

	//--- add sub sub structure arguments to data
	temp = json_object_new_string(json_object_get_string(arguments));
	//temp = json_object_new_string(json_object_to_json_string_ext(arguments, JSON_C_TO_STRING_NOSLASHESCAPE));
	json_object_object_add(data, "arguments", temp);

	// add sub structure data to mqttPayload
	temp = json_object_new_string(json_object_get_string(data));
	json_object_object_add(mqttPayload, "data", temp);

	// Add command component to JSON object
	//temp = json_object_new_string((char *) "methodcall");
	//json_object_object_add(mqttPayload, "command", temp);

	requestConfig->publisher.publishTopic = strdup("dcp-common");
	// TODO: free publishTopic

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
  
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));
	json_object_put(data);
	json_object_put(arguments);
	json_object_put(mqttPayload);
	//char *mqttPayloadString = strdup(json_object_to_json_string_ext(mqttPayload, JSON_C_TO_STRING_NOSLASHESCAPE));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "StopMethodCallback:Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "StopMethodCallback:addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);

	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
//#define COMPONENT_RESPONSE "{\"status\":\"ack\",\"final_msg\":\"false\",\"info\":\"Further information available via subscription to response_node\",\"response_node_ns\":\"1\",\"response_node\":\""
	//char dummy1[sizeof(COMPONENT_RESPONSE)+sizeof("\"}")+1] = COMPONENT_RESPONSE;
	//strcat(dummy1, iiotToXReqId);
	//char dummy2[] = "\"}";
	//strcat(dummy1, dummy2);
//	UA_String outputStr = UA_STRING(dummy1);

	// --- 4.2) send method response with output argument to remote device
	//returnValue = UA_Variant_setScalarCopy(output, &outputStr, &UA_TYPES[UA_TYPES_STRING]);
	//Output Request Status 
	UA_String stringtmp = UA_String_fromChars("ok");
	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Information 
	stringtmp = UA_String_fromChars("Forwarded stop signal to DCP");
	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node 
	stringtmp = UA_String_fromChars(iiotToXReqId);
	returnValue |= UA_Variant_setScalarCopy(&output[2], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node Namespace 
	stringtmp = UA_String_fromChars("1");
	returnValue |= UA_Variant_setScalarCopy(&output[3], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);

	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "StopMethodCallback: Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}

static UA_StatusCode 
ReservationMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

	int returnValue = 0;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;
	

	// --- 1.1) prepare message to be published via MQTT and received by target component
	
	UA_String *hmiID = (UA_String*)input[0].data;
	UA_String *status = (UA_String*)input[1].data;


	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	struct json_object *data = json_object_new_object();
	struct json_object *arguments = json_object_new_object();
	//char *componentName;
	UA_Boolean dataMissing = UA_FALSE;

	// Add answer_topic to JSON object
	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the response node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//TODO free reqId

	//create sub structure data
	//--- Add HMI ID to JSON object
	if (hmiID->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ReservationMethodCallback: Received a method call with missing argument HMI_ID.");
	}else{
		temp = json_object_new_string_len((char *) hmiID->data, hmiID->length);
		//temp = json_object_new_string((char *) hmiID->data);
		json_object_object_add(data, "dev_id", temp);
	}

	//--- Add command to JSON object
	temp = json_object_new_string((char *) "reservation");			
	json_object_object_add(data, "cmd", temp);

	//--- create sub sub structure arguments
	if (status->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ReservationMethodCallback: Received a method call with missing argument function_id (Request ID).");
	}else{
		temp = json_object_new_string_len((char *) status->data, status->length);
		//temp = json_object_new_string((char *) status->data);
		json_object_object_add(arguments, "status", temp);
	}
	

	if (dataMissing){
		//UA_String errorString = UA_STRING("{\"status\":\"nack\",\"final_msg\":\"true\",\"info\":\"One or more mandatory arguments are missing\"}");
		// --- send method response with output argument to requesting device
		//UA_Variant_setScalarCopy(output, &errorString, &UA_TYPES[UA_TYPES_STRING]);
		//Output Request Status 
		UA_Variant_setScalarCopy(&output[0], "error", &UA_TYPES[UA_TYPES_STRING]);
		//Output Information 
		UA_Variant_setScalarCopy(&output[1], "One or more mandatory arguments are missing", &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ReservationMethodCallback: Received a method call with missing arguments.");
		return EXIT_FAILURE;
	}

	

	//--- add sub sub structure arguments to data
	temp = json_object_new_string(json_object_get_string(arguments));
	//temp = json_object_new_string(json_object_to_json_string_ext(arguments, JSON_C_TO_STRING_NOSLASHESCAPE));
	json_object_object_add(data, "arguments", temp);

	// add sub structure data to mqttPayload
	temp = json_object_new_string(json_object_get_string(data));
	json_object_object_add(mqttPayload, "data", temp);

	// Add command component to JSON object
	//temp = json_object_new_string((char *) "methodcall");
	//json_object_object_add(mqttPayload, "command", temp);

	requestConfig->publisher.publishTopic = strdup("dcp-common");
	// TODO: free publishTopic

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
  
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));
	json_object_put(data);
	json_object_put(arguments);
	json_object_put(mqttPayload);
	//char *mqttPayloadString = strdup(json_object_to_json_string_ext(mqttPayload, JSON_C_TO_STRING_NOSLASHESCAPE));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ReservationMethodCallback:Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ReservationMethodCallback:addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);

	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	free(tempThread);
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
//#define COMPONENT_RESPONSE "{\"status\":\"ack\",\"final_msg\":\"false\",\"info\":\"Further information available via subscription to response_node\",\"response_node_ns\":\"1\",\"response_node\":\""
	//char dummy1[sizeof(COMPONENT_RESPONSE)+sizeof("\"}")+1] = COMPONENT_RESPONSE;
	//strcat(dummy1, iiotToXReqId);
	//char dummy2[] = "\"}";
	//strcat(dummy1, dummy2);
//	UA_String outputStr = UA_STRING(dummy1);

	// --- 4.2) send method response with output argument to remote device
	//returnValue = UA_Variant_setScalarCopy(output, &outputStr, &UA_TYPES[UA_TYPES_STRING]);
	//Output Request Status 
	UA_String stringtmp = UA_String_fromChars("ok");
	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Information 
	stringtmp = UA_String_fromChars("Forwarded reservation request to DCP");
	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node 
	stringtmp = UA_String_fromChars(iiotToXReqId);
	returnValue |= UA_Variant_setScalarCopy(&output[2], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node Namespace 
	stringtmp = UA_String_fromChars("1");
	returnValue |= UA_Variant_setScalarCopy(&output[3], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);

	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ReservationMethodCallback: Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}

static UA_StatusCode 
GoToAGVMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

	int returnValue = 0;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;

	// --- 1.1) prepare message to be published via MQTT and received by target component
	
	UA_String *hmiID = (UA_String*)input[0].data;
	UA_String *startNode = (UA_String*)input[1].data;	
	UA_String *endNode = (UA_String*)input[2].data;	


	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	struct json_object *data = json_object_new_object();
	struct json_object *arguments = json_object_new_object();
	//char *componentName;
	UA_Boolean dataMissing = UA_FALSE;

	// Add answer_topic to JSON object
	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the response node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//TODO free reqId

	//create sub structure data
	//--- Add HMI ID to JSON object
	if (hmiID->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback: Received a method call with missing argument HMI_ID.");
	}else{
		temp = json_object_new_string_len((char *) hmiID->data, hmiID->length);
		//temp = json_object_new_string((char *) hmiID->data);
		json_object_object_add(data, "dev_id", temp);
	}

	//--- Add command to JSON object
	temp = json_object_new_string((char *) "gotoposition");			
	json_object_object_add(data, "cmd", temp);

	//--- create sub sub structure arguments
	if (startNode->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback: Received a method call with missing argument startNode.");
	}else{
		temp = json_object_new_string_len((char *) startNode->data, startNode->length);
		//temp = json_object_new_string((char *) startNode->data);
		json_object_object_add(arguments, "start_node", temp);
	}

	if (endNode->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback: Received a method call with missing argument endNode.");
	}else{
		temp = json_object_new_string_len((char *) endNode->data, endNode->length);
		//temp = json_object_new_string((char *) endNode->data);
		json_object_object_add(arguments, "end_node", temp);
	}
	

	if (dataMissing){
		//UA_String errorString = UA_STRING("{\"status\":\"nack\",\"final_msg\":\"true\",\"info\":\"One or more mandatory arguments are missing\"}");
		// --- send method response with output argument to requesting device
		//UA_Variant_setScalarCopy(output, &errorString, &UA_TYPES[UA_TYPES_STRING]);
		//Output Request Status 
		UA_Variant_setScalarCopy(&output[0], "error", &UA_TYPES[UA_TYPES_STRING]);
		//Output Information 
		UA_Variant_setScalarCopy(&output[1], "One or more mandatory arguments are missing", &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback: Received a method call with missing arguments.");
		return EXIT_FAILURE;
	}

	

	//--- add sub sub structure arguments to data
	temp = json_object_new_string(json_object_get_string(arguments));
	//temp = json_object_new_string(json_object_to_json_string_ext(arguments, JSON_C_TO_STRING_NOSLASHESCAPE));
	json_object_object_add(data, "arguments", temp);

	// add sub structure data to mqttPayload
	temp = json_object_new_string(json_object_get_string(data));
	json_object_object_add(mqttPayload, "data", temp);

	// Add command component to JSON object
	//temp = json_object_new_string((char *) "methodcall");
	//json_object_object_add(mqttPayload, "command", temp);

	requestConfig->publisher.publishTopic = strdup("dcp-common");
	// TODO: free publishTopic

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
  
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));
	json_object_put(data);
	json_object_put(arguments);
	json_object_put(mqttPayload);
	//char *mqttPayloadString = strdup(json_object_to_json_string_ext(mqttPayload, JSON_C_TO_STRING_NOSLASHESCAPE));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback:Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback:addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);

	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
//#define COMPONENT_RESPONSE "{\"status\":\"ack\",\"final_msg\":\"false\",\"info\":\"Further information available via subscription to response_node\",\"response_node_ns\":\"1\",\"response_node\":\""
	//char dummy1[sizeof(COMPONENT_RESPONSE)+sizeof("\"}")+1] = COMPONENT_RESPONSE;
	//strcat(dummy1, iiotToXReqId);
	//char dummy2[] = "\"}";
	//strcat(dummy1, dummy2);
//	UA_String outputStr = UA_STRING(dummy1);

	// --- 4.2) send method response with output argument to remote device
	//returnValue = UA_Variant_setScalarCopy(output, &outputStr, &UA_TYPES[UA_TYPES_STRING]);
	//Output Request Status 
	UA_String stringtmp = UA_String_fromChars("ok");
	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Information 
	stringtmp = UA_String_fromChars("Forwarded goto (AGV) request to DCP");
	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node 
	stringtmp = UA_String_fromChars(iiotToXReqId);
	returnValue |= UA_Variant_setScalarCopy(&output[2], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node Namespace 
	stringtmp = UA_String_fromChars("1");
	returnValue |= UA_Variant_setScalarCopy(&output[3], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);

	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GoToAGVMethodCallback: Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}

static UA_StatusCode 
ManualTandemMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

	int returnValue = 0;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;
	

	// --- 1.1) prepare message to be published via MQTT and received by target component
	
	UA_String *hmiID = (UA_String*)input[0].data;
	UA_String *master = (UA_String*)input[1].data;
	UA_String *enable = (UA_String*)input[2].data;


	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	struct json_object *data = json_object_new_object();
	struct json_object *arguments = json_object_new_object();
	//char *componentName;
	UA_Boolean dataMissing = UA_FALSE;

	// Add answer_topic to JSON object
	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the response node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//TODO free reqId

	//create sub structure data
	//--- Add HMI ID to JSON object
	if (hmiID->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback: Received a method call with missing argument HMI_ID.");
	}else{
		temp = json_object_new_string_len((char *) hmiID->data, hmiID->length);
		//temp = json_object_new_string((char *) hmiID->data);
		json_object_object_add(data, "dev_id", temp);
	}

	//--- Add command to JSON object
	temp = json_object_new_string((char *) "manualtandem");			
	json_object_object_add(data, "cmd", temp);

	//--- create sub sub structure arguments
	if (master->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback: Received a method call with missing argument master.");
	}else{
		temp = json_object_new_string_len((char *) master->data, master->length);
		//temp = json_object_new_string((char *) status->data);
		json_object_object_add(arguments, "master", temp);
	}

	if (enable->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback: Received a method call with missing argument master.");
	}else{
		temp = json_object_new_string_len((char *) enable->data, enable->length);
		//temp = json_object_new_string((char *) status->data);
		json_object_object_add(arguments, "enable", temp);
	}
	

	if (dataMissing){
		//UA_String errorString = UA_STRING("{\"status\":\"nack\",\"final_msg\":\"true\",\"info\":\"One or more mandatory arguments are missing\"}");
		// --- send method response with output argument to requesting device
		//UA_Variant_setScalarCopy(output, &errorString, &UA_TYPES[UA_TYPES_STRING]);
		//Output Request Status 
		UA_Variant_setScalarCopy(&output[0], "error", &UA_TYPES[UA_TYPES_STRING]);
		//Output Information 
		UA_Variant_setScalarCopy(&output[1], "One or more mandatory arguments are missing", &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback: Received a method call with missing arguments.");
		return EXIT_FAILURE;
	}

	

	//--- add sub sub structure arguments to data
	temp = json_object_new_string(json_object_get_string(arguments));
	//temp = json_object_new_string(json_object_to_json_string_ext(arguments, JSON_C_TO_STRING_NOSLASHESCAPE));
	json_object_object_add(data, "arguments", temp);

	// add sub structure data to mqttPayload
	temp = json_object_new_string(json_object_get_string(data));
	json_object_object_add(mqttPayload, "data", temp);

	// Add command component to JSON object
	//temp = json_object_new_string((char *) "methodcall");
	//json_object_object_add(mqttPayload, "command", temp);

	requestConfig->publisher.publishTopic = strdup("dcp-common");
	// TODO: free publishTopic

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
  
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));
	json_object_put(data);
	json_object_put(arguments);
	json_object_put(mqttPayload);
	//char *mqttPayloadString = strdup(json_object_to_json_string_ext(mqttPayload, JSON_C_TO_STRING_NOSLASHESCAPE));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback:Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback:addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);

	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
	//Output Request Status 
	UA_String stringtmp = UA_String_fromChars("ok");
	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Information 
	stringtmp = UA_String_fromChars("Forwarded request to DCP");
	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node 
	stringtmp = UA_String_fromChars(iiotToXReqId);
	returnValue |= UA_Variant_setScalarCopy(&output[2], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node Namespace 
	stringtmp = UA_String_fromChars("1");
	returnValue |= UA_Variant_setScalarCopy(&output[3], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);

	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ManualTandemMethodCallback: Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}

static UA_StatusCode 
CraneModeMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

	int returnValue = 0;
	request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
	requestConfig->data = NULL;
	//requestConfig->hh = NULL;
	requestConfig->method_id = NULL;
	//requestConfig->monitoredItem = NULL;
	requestConfig->payload_data = NULL;
	requestConfig->payload_dataArraySize = 0;
	requestConfig->payload_dataArray = NULL;
	//requestConfig->publisher = NULL;
	requestConfig->reqId = NULL;
	requestConfig->sub_id = 0;
	requestConfig->subscription = NULL;
	requestConfig->terminateClient = NULL;
	requestConfig->url = NULL;
	requestConfig->client = NULL;
	requestConfig->command = NULL;
	requestConfig->component = NULL;
	requestConfig->subscribedArraySize = 0;
	requestConfig->subscribedValuesArray = NULL;
	requestConfig->subscribedVariablesArray = NULL;
	requestConfig->val = NULL;
	

	// --- 1.1) prepare message to be published via MQTT and received by target component
	
	UA_String *hmiID = (UA_String*)input[0].data;
	UA_String *mode = (UA_String*)input[1].data;


	struct json_object *temp;
	struct json_object *mqttPayload = json_object_new_object();
	struct json_object *data = json_object_new_object();
	struct json_object *arguments = json_object_new_object();
	//char *componentName;
	UA_Boolean dataMissing = UA_FALSE;

	// Add answer_topic to JSON object
	temp = json_object_new_string((char *) SUBSCRIBE_TOPIC);
	json_object_object_add(mqttPayload, "answer_topic", temp);

	// should be the same as the node name of the response node created for the request in the OPC UA address space
	reqIdCount++;
	char iiotToXReqId[sizeof(REQUEST_ID_PREFIX)+sizeof(snprintf(NULL, 0, "%ld", reqIdCount))+1];
	sprintf(iiotToXReqId, "%s%ld", REQUEST_ID_PREFIX, reqIdCount);
	temp = json_object_new_string(iiotToXReqId);
	json_object_object_add(mqttPayload, "req_id", temp);
	requestConfig->reqId = strdup(iiotToXReqId);
	//TODO free reqId

	//create sub structure data
	//--- Add HMI ID to JSON object
	if (hmiID->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CraneModeMethodCallback: Received a method call with missing argument HMI_ID.");
	}else{
		temp = json_object_new_string_len((char *) hmiID->data, hmiID->length);
		//temp = json_object_new_string((char *) hmiID->data);
		json_object_object_add(data, "dev_id", temp);
	}

	//--- Add command to JSON object
	temp = json_object_new_string((char *) "cranemode");			
	json_object_object_add(data, "cmd", temp);

	//--- create sub sub structure arguments
	if (mode->length == 0){
		dataMissing = UA_TRUE;
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CraneModeMethodCallback: Received a method call with missing argument mode.");
	}else{
		temp = json_object_new_string_len((char *) mode->data, mode->length);
		//temp = json_object_new_string((char *) status->data);
		json_object_object_add(arguments, "mode", temp);
	}
	

	if (dataMissing){
		//Output Request Status 
		UA_Variant_setScalarCopy(&output[0], "error", &UA_TYPES[UA_TYPES_STRING]);
		//Output Information 
		UA_Variant_setScalarCopy(&output[1], "One or more mandatory arguments are missing", &UA_TYPES[UA_TYPES_STRING]);
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CraneModeMethodCallback: Received a method call with missing arguments.");
		return EXIT_FAILURE;
	}

	

	//--- add sub sub structure arguments to data
	temp = json_object_new_string(json_object_get_string(arguments));
	//temp = json_object_new_string(json_object_to_json_string_ext(arguments, JSON_C_TO_STRING_NOSLASHESCAPE));
	json_object_object_add(data, "arguments", temp);

	// add sub structure data to mqttPayload
	temp = json_object_new_string(json_object_get_string(data));
	json_object_object_add(mqttPayload, "data", temp);

	// Add command component to JSON object
	//temp = json_object_new_string((char *) "methodcall");
	//json_object_object_add(mqttPayload, "command", temp);

	requestConfig->publisher.publishTopic = strdup("dcp-common");
	// TODO: free publishTopic

	requestConfig->publisher.useJson = true;
    requestConfig->publisher.publishInterval = PUBLISH_INTERVAL;
    requestConfig->publisher.qos = iQos;
  
	char *mqttPayloadString = strdup(json_object_get_string(mqttPayload));
	json_object_put(data);
	json_object_put(arguments);
	json_object_put(mqttPayload);
	//char *mqttPayloadString = strdup(json_object_to_json_string_ext(mqttPayload, JSON_C_TO_STRING_NOSLASHESCAPE));
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CraneModeMethodCallback:Created JSON message to forward request via MQTT broker to component. Topic: %s, Payload: %s", requestConfig->publisher.publishTopic, mqttPayloadString);

	// --- 1.2) create request specific node and add request data als value, writer group will be later activated in thread (why a thread for this?)
	returnValue = addRequestSpecificNode(common.server, requestConfig->reqId, mqttPayloadString);
	free(mqttPayloadString);
	if (returnValue != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CraneModeMethodCallback:addRequestSpecificNode: Error Name = %s",
					 UA_StatusCode_name(returnValue));
		return returnValue;
	}

	// --- 2) publish MQTT message to broker and thus indirectly to target component
	// TODO: where is the pub sub extension set up to forward MQTT msgs to the response_node?
	// TODO: why separate thread? for workaround sleep?
	pthread_t *tempThread = malloc(sizeof(pthread_t));
	int rc = pthread_create(tempThread, NULL, opc_ua_pubsub_api, (void *)requestConfig);

	if (rc)
	{
		printf("ERR; pthread_create() ret = %d\n", rc);
		exit(-1);
	}
	
	// --- 4.1) prepare method call output argument
	// TODO: in case of error, send error msg with final_msg: true
	//Output Request Status 
	UA_String stringtmp = UA_String_fromChars("ok");
	returnValue = UA_Variant_setScalarCopy(&output[0], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Information 
	stringtmp = UA_String_fromChars("Forwarded request to DCP");
	returnValue |= UA_Variant_setScalarCopy(&output[1], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node 
	stringtmp = UA_String_fromChars(iiotToXReqId);
	returnValue |= UA_Variant_setScalarCopy(&output[2], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);
	//Output Response Node Namespace 
	stringtmp = UA_String_fromChars("1");
	returnValue |= UA_Variant_setScalarCopy(&output[3], &stringtmp, &UA_TYPES[UA_TYPES_STRING]);
	UA_String_clear(&stringtmp);

	if (returnValue != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "CraneModeMethodCallback: Returning Output Arguments to the Method Caller: Error Name = %s",
				UA_StatusCode_name(returnValue));
		return returnValue;
	}

	return returnValue;
}


static UA_StatusCode addGoToMethod(UA_Server *server) {
	UA_StatusCode result;
	//  {"component":"dcp","data":{"dev_id":"hmi1","cmd":"gotoposition",
	//"arguments":{"master":"crane1","slaveDeviceArray":[""],"trolley_master":"trolley1","trolley_slaveDeviceArray":[""],"position":{"x":1,"y":1,"z":0}}}}
	UA_Argument inputArgument[6];
	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that has been used to reserve the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "X, Y and Z cooridnates for GoTo UseCase, Format: X,Y,Z, e. g., 302,545,0");
    inputArgument[1].name = UA_STRING("Coordinates");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[2]);
	inputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter: Name of master device (OPC UA device name), e. g., crane1");
    inputArgument[2].name = UA_STRING("Master Device");
    inputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[3]);
	inputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter: Comma separated name(s) of slave device(s) (OPC UA device name), e. g., crane_2,crane_3");
    inputArgument[3].name = UA_STRING("Slave Devices");
    inputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[3].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[4]);
	inputArgument[4].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter (Crane specific): Name of master trolley (OPC UA trolley name), e. g., trolley1");
    inputArgument[4].name = UA_STRING("Master Trolley");
    inputArgument[4].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[4].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[5]);
	inputArgument[5].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter (Crane specific): Comma separated name(s) of slave trolley(s) (OPC UA trolley name), e. g., trolley2, trolley3");
    inputArgument[5].name = UA_STRING("Slave Trolleys");
    inputArgument[5].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[5].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Send device to target coordiantes");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_GoTo");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_GoTo"),
                           // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_GoTo"),
                            operatorLocAttr, &GoToMethodCallback,
                            6, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}


static UA_StatusCode addComeToMeMethod(UA_Server *server) {
	UA_StatusCode result;
	UA_Argument inputArgument[6];

	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that has been used to reserve the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Address of Target Device for Come To Me UseCase, Format: opc.tcp://IP_or_HOSTNAME:PORT");
    inputArgument[1].name = UA_STRING("Target Device");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[2]);
	inputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter: Name of master device (OPC UA device name), e. g., crane1");
    inputArgument[2].name = UA_STRING("Master Device");
    inputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[3]);
	inputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter: Comma separated name(s) of slave device(s) (OPC UA device name), e. g., crane_2,crane_3");
    inputArgument[3].name = UA_STRING("Slave Devices");
    inputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[3].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[4]);
	inputArgument[4].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter (Crane specific): Name of master trolley (OPC UA trolley name), e. g., trolley1");
    inputArgument[4].name = UA_STRING("Master Trolley");
    inputArgument[4].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[4].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[5]);
	inputArgument[5].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter (Crane specific): Comma separated name(s) of slave trolley(s) (OPC UA trolley name), e. g., trolley2, trolley3");
    inputArgument[5].name = UA_STRING("Slave Trolleys");
    inputArgument[5].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[5].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Instruct device to come to current target position");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_ComeTo");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_ComeTo"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_ComeTo"),
                            operatorLocAttr, &ComeToMeMethodCallback,
                            6, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}

static UA_StatusCode addFollowMeMethod(UA_Server *server) {
	UA_StatusCode result;
	UA_Argument inputArgument[6];

	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that has been used to reserve the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Address of Target Device for Follow Me UseCase, Format: opc.tcp://IP_or_HOSTNAME:PORT");
    inputArgument[1].name = UA_STRING("Target Device");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[2]);
	inputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter: Name of master device (OPC UA device name), e. g., crane1");
    inputArgument[2].name = UA_STRING("Master Device");
    inputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[3]);
	inputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter: Comma separated name(s) of slave device(s) (OPC UA device name), e. g., crane_2,crane_3");
    inputArgument[3].name = UA_STRING("Slave Devices");
    inputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[3].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[4]);
	inputArgument[4].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter (Crane specific): Name of master trolley (OPC UA trolley name), e. g., trolley1");
    inputArgument[4].name = UA_STRING("Master Trolley");
    inputArgument[4].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[4].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[5]);
	inputArgument[5].description = UA_LOCALIZEDTEXT("en-US", "FollowMachine Parameter (Crane specific): Comma separated name(s) of slave trolley(s) (OPC UA trolley name), e. g., trolley2, trolley3");
    inputArgument[5].name = UA_STRING("Slave Trolleys");
    inputArgument[5].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[5].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Instruct device to follow the target device");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_Follow");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_Follow"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_Follow"),
                            operatorLocAttr, &FollowMeMethodCallback,
                            6, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}

static UA_StatusCode addGoToAGVMethod(UA_Server *server) {
	UA_StatusCode result;
	//  ["{ \"answer_topic\": \"iiot-common\", \"req_id\": \"iiot1\", \"data\": \"{ \\\"dev_id\\\": \\\"hmi1\\\", \\\"cmd\\\": \\\"goto\\\",
	// \\\"arguments\\\": \\\"{\\\\\\\"start_node\\\\\\\": start_node_num,\\\\\\\"end_node\\\\\\\":end_node_num}\\\" }\" }"] 
	UA_Argument inputArgument[3];
	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that has been used to reserve the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Start Node for AGV Goto Command");
    inputArgument[1].name = UA_STRING("Start Node");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[2]);
	inputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "End Node for AGV Goto Command");
    inputArgument[2].name = UA_STRING("End Node ");
    inputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[2].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Send device to end node");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_AGV_GoTo");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_AGV_GoTo"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_AGV_GoTo"),
                            operatorLocAttr, &GoToAGVMethodCallback,
                            3, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}

static UA_StatusCode addStopMethod(UA_Server *server) {
	UA_StatusCode result;
	UA_Argument inputArgument[2];

	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that has been used to reserve the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Request ID of method to stop (corresponds to Response Node)");
    inputArgument[1].name = UA_STRING("Request ID");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	 UA_Argument_init(&outputArgument[2]);
     outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
     outputArgument[2].name = UA_STRING("Response Node");
     outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
     outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Stop a running semi-autonomous function");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_Stop");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_Stop"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_Stop"),
                            operatorLocAttr, &StopMethodCallback,
                            2, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}

static UA_StatusCode addReservationMethod(UA_Server *server) {
	UA_StatusCode result;
	UA_Argument inputArgument[2];

	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that reserves the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Reservation Status (on = reservation; off = quit reservation)");
    inputArgument[1].name = UA_STRING("Reservation Status");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Reserve a Device");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_Reservation");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_Reservation"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_Reservation"),
                            operatorLocAttr, &ReservationMethodCallback,
                            2, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}

static UA_StatusCode addManualTandemMethod(UA_Server *server) {
	UA_StatusCode result;
	UA_Argument inputArgument[3];

	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that reserves the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "ID of master device. The remote controller of this device can be used to control tandem functionality, e.g., crane1");
    inputArgument[1].name = UA_STRING("Master Device");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[2]);
	inputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Enable (on) or disable (off) manual tandem function");
    inputArgument[2].name = UA_STRING("Enable");
    inputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[2].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Enable manual tandem functionality of master remote control to move two cranes simultaneously. Only one controller of the involved cranes can be used at a time.");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_ManualTandem");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_ManualTandem"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_ManualTandem"),
                            operatorLocAttr, &ManualTandemMethodCallback,
                            3, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}

static UA_StatusCode addCraneModeMethod(UA_Server *server) {
	UA_StatusCode result;
	UA_Argument inputArgument[2];

	UA_Argument_init(&inputArgument[0]);	
	inputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "User or HMI ID that reserves the device.");
    inputArgument[0].name = UA_STRING("Client ID");
    inputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&inputArgument[1]);
	inputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Supports conventional crane control mode (legacy) and OPTIMUM control mode (optimum)");
    inputArgument[1].name = UA_STRING("Mode");
    inputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument[1].valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument[4];
    UA_Argument_init(&outputArgument[0]);
    outputArgument[0].description = UA_LOCALIZEDTEXT("en-US", "Request status information");
    outputArgument[0].name = UA_STRING("Request Status");
    outputArgument[0].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[0].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[1]);
    outputArgument[1].description = UA_LOCALIZEDTEXT("en-US", "Information");
    outputArgument[1].name = UA_STRING("Information");
    outputArgument[1].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[1].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[2]);
    outputArgument[2].description = UA_LOCALIZEDTEXT("en-US", "Node in OPC UA Address Space that contains further data. Subscribe to it to receive all status information");
    outputArgument[2].name = UA_STRING("Response Node");
    outputArgument[2].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[2].valueRank = UA_VALUERANK_SCALAR;

	UA_Argument_init(&outputArgument[3]);
    outputArgument[3].description = UA_LOCALIZEDTEXT("en-US", "OPC UA Namespace of Response Node");
    outputArgument[3].name = UA_STRING("Response Node Namespace");
    outputArgument[3].dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument[3].valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes operatorLocAttr = UA_MethodAttributes_default;
    operatorLocAttr.description = UA_LOCALIZEDTEXT("en-US","Swap between legacy control mode (no OPTIMUM functionality) and OPTIMUM mode with smart functionality");
    operatorLocAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_CraneMode");
    operatorLocAttr.executable = true;
    operatorLocAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_CraneMode"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_CraneMode"),
                            operatorLocAttr, &CraneModeMethodCallback,
                            2, inputArgument, 4, outputArgument, NULL, NULL);
	return result;
}
/*Add General-Purpose MEthodCall to OPC UA Server Data Modell/Address Space
 *
 */
static UA_StatusCode addMethod_JSON(UA_Server *server) {	
	UA_StatusCode result;
	
    UA_Argument inputArgument;
    UA_Argument_init(&inputArgument);
    inputArgument.description = UA_LOCALIZEDTEXT("en-US", "Command for OPTIMUM Component in JSON encoding, keys 'command' and 'data' are required");
    inputArgument.name = UA_STRING("Method_JSON");
    inputArgument.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument.valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument;
    UA_Argument_init(&outputArgument);
    outputArgument.description = UA_LOCALIZEDTEXT("en-US", "Status of request and response node to be subscribed for further status information");
    outputArgument.name = UA_STRING("Response");
    outputArgument.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument.valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes optimumDeviceAttr = UA_MethodAttributes_default;
    optimumDeviceAttr.description = UA_LOCALIZEDTEXT("en-US","Send Commands to Components of OPTIMUM Device using JSON encoding");
    optimumDeviceAttr.displayName = UA_LOCALIZEDTEXT("en-US","Method_JSON");
    optimumDeviceAttr.executable = true;
    optimumDeviceAttr.userExecutable = true;
    result = UA_Server_addMethodNode(server, UA_NODEID_STRING(3, "Method_JSON"),
                            // UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
						    UA_NODEID_NUMERIC(3, 1033), // optimumCrane object
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(3, "Method_JSON"),
                            optimumDeviceAttr, &GPRequestCallback,
                            1, &inputArgument, 1, &outputArgument, NULL, NULL);
	return result;
}
 
//-------------------------------------- Code for pub sub message reception ---------------------------------
/*
 * @brief OnSubscribe: whenever a message is received on IIoT-common Topic
 * Note: all messages received on MQTT are received in this callback
 * For now, user must make sure that message on the desired topic is processed.
 * Ideally, only the message on topic mentioned in tranportSetting of broker should
 * end up here. open62541 subscriber API is a work in progress
 *
 * @param encodedBuffer JSON encoded received message
 * @param topic Topic at which the message is received
 */
void subscriber_message_callback(UA_ByteString *encodedBuffer, UA_ByteString *topic) {		
	// TODO: HERE goes all the code for the request-response stuff. Start with method calls and then the passing of information to the dcp, later take care about the reverse path
	// TODO: merge opclcient and payload structs, free the final strcut in the client thread
	UA_StatusCode retval = UA_STATUSCODE_GOOD;
	//const char *requestType, *topicToPublish, *craneAddress, *req_id, *isSubscribed = "FALSE";
	//const char *decoded_data, *decoded_cmd, *decoded_args;
	optimum_msg payload;
	payload.answer_topic = NULL;
	payload.req_id = NULL;
	payload.device = 0;
	payload.component = NULL;
	payload.command = NULL;
	payload.method_id = NULL;
	payload.subscription = NULL;
	payload.final_msg = NULL;
	payload.status = NULL;
	payload.data = NULL;
	payload.dataArray = NULL;
	payload.dataArraySize = 0;
	payload.sub_id = 0;
			
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "encodedBuffer: %s\n.", encodedBuffer->data);
	
	//char substr[strlen(SUBSCRIBE_TOPIC)];
    //strncpy(substr, (char*)topic->data, strlen(SUBSCRIBE_TOPIC)); // to make sure the subscribed topic is not appended with rubbish data	
	// replaced by strNcmp
	
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "WriterGroupId = %u", g_appConfig.writerGroupIdentifier.identifier.numeric);

	// CHECK if MQTT topic of received msg is valid
	//if (0 != strncmp(SUBSCRIBE_TOPIC, (char*)topic->data,strlen(SUBSCRIBE_TOPIC)))
	if (0 == strncmp(MODIFY_DATA_TOPIC, (char*)topic->data,strlen(MODIFY_DATA_TOPIC)))
	{
		// update location variables in OPC UA data model
		//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "received MQTT msg on topic '%s", MODIFY_DATA_TOPIC);
		struct json_object *parsed_json;
		char *buffer = malloc(encodedBuffer->length+1);
		memcpy(buffer, encodedBuffer->data, encodedBuffer->length);
		buffer[encodedBuffer->length] = '\0';
		parsed_json = json_tokener_parse(buffer);	
		struct json_object *temp;
		UA_Boolean TypeId = UA_FALSE;
		UA_Boolean TypePath = UA_FALSE;
		// indentify type of modification: changes re referenced by nodeIDs or browsepath?
		if (json_object_object_get_ex(parsed_json, "nodes", &temp)) //in future: "nodeID"
			TypeId = UA_TRUE;
		//if (json_object_object_get_ex(parsed_json, "BrowsePath", &temp))
		//	TypePath = UA_TRUE;
		if (TypeId || TypePath)
		{
		//	if (TypeId)
		//		json_object_object_get_ex(parsed_json, "nodeID", &temp);

			char *key = NULL;                                                      
			struct json_object *val = NULL;                                        
			struct lh_entry *entry;                                           
			struct lh_entry *entry_next = NULL;    
			size_t ns;
			UA_Server_getNamespaceByName(common.server, UA_STRING("http://optimum-itea3.eu/MHCranes/"), &ns);  
			UA_Variant var;       
			UA_Variant_init(&var);  
			UA_NodeId dataType;   
			//UA_Boolean key_valid = UA_FALSE;       
			for (entry = json_object_get_object(temp)->head; (entry ? (key = (char *)lh_entry_k(entry), val = (struct json_object *)lh_entry_v(entry), entry_next = entry->next, entry) : 0); entry = entry_next)
			{
			//	if (TypeId){
					UA_UInt32 key_int = atoi(key);
					//
					if (key_int > 0 && key_int < INT32_MAX){
					//	key_valid = UA_TRUE;
					}else {
						UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Key error: received key %d, key as string: %s", key_int, key);
						free_payload_elements(&payload, UA_TRUE);
						UA_ByteString_clear(topic);
						UA_ByteString_delete(topic);
						UA_ByteString_clear(encodedBuffer);
    					UA_ByteString_delete(encodedBuffer);
						return;
					}

					//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "MHCranes NS Idx: %ld", ns);
					UA_StatusCode retval = UA_Server_readDataType(common.server, UA_NODEID_NUMERIC(ns, key_int), &dataType);
			//	}else{
			//		UA_QualifiedName 
			//		UA_Server_translateBrowsePathToNodeIds()
			//		UA_StatusCode retval = UA_Server_readDataType(common.server, UA_NODEID_NUMERIC(ns, key_int), &dataType);
			//	}

					
					if (retval != UA_STATUSCODE_GOOD)
					{
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "readDataType: Error Name = %s", UA_StatusCode_name(retval));
					}
					//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Data Type of Node to be modified: %d", dataType.identifier.numeric);
					switch(dataType.identifier.numeric-1)
					{
						case UA_TYPES_DOUBLE:
						;
						double valueDb = json_object_get_double(val);
						UA_Variant_setScalar(&var, &valueDb, &UA_TYPES[UA_TYPES_DOUBLE]);
						UA_Server_writeValue(common.server, UA_NODEID_NUMERIC(ns, key_int), var);

						break;
						
						case UA_TYPES_INT16:
						case UA_TYPES_INT32:
						case UA_TYPES_INT64:
						case UA_TYPES_UINT16:
						case UA_TYPES_UINT32:
						case UA_TYPES_UINT64:
						;
						int64_t valueInt = json_object_get_int(val);
						UA_Variant_setScalar(&var, &valueInt, &UA_TYPES[dataType.identifier.numeric-1]);
						UA_Server_writeValue(common.server, UA_NODEID_NUMERIC(ns, key_int), var);

						break;
						case UA_TYPES_STRING:
						;
						UA_String valueChar = UA_STRING((char*) json_object_get_string(val));
						UA_Variant_setScalar(&var, &valueChar, &UA_TYPES[UA_TYPES_STRING]);
						UA_Server_writeValue(common.server, UA_NODEID_NUMERIC(ns, key_int), var);

						break;

						default:
						UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Data Type with NodeId %u not covered in switch statement", dataType.identifier.numeric);

					}
				
			}
			json_object_put(parsed_json);
		}	
		free(buffer);
		
	}else if (0 == strncmp(SUBSCRIBE_TOPIC, (char*)topic->data,strlen(SUBSCRIBE_TOPIC))){

	

		// TODO: restructure decoded_data into struct isntead of global variables -> decdeJson ...data_object...) use a struct instead
		// TODO: CHECK if JSON encoding of MQTT msg payload is successful and parse JSON key and values into variables
		decodeJsonMsg(encodedBuffer, &payload);
		/*if (!decodeJson(encodedBuffer, &payload))
		{
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Incorrect JSON Payload Format \n");
			return;
		}*/
		//recvMsg.payload.mode
		//requestType = json_object_get_string(request);
		//int32_t decoded_data = json_object_get_int(data); // for integer values
		//decoded_data = json_object_get_string(data_object);

		// --- Identify if the received MQTT msg is a request or a response
		// ! TODO: add code for decision
		int32_t msgType = UNKNOWN;
		if (!payload.answer_topic && payload.req_id && payload.final_msg && payload.status && payload.data){
			msgType = RESPONSE;
		}else if (payload.answer_topic && payload.req_id && payload.device && payload.component && payload.command){
			if ((strcmp(payload.component, "iiot") == 0) && payload.subscription){
				if (strcmp(payload.subscription, "cancel") == 0){
					msgType = REQUEST_IIOT_READ_SUBSCRIPTION_CANCEL;
				}else if(strcmp(payload.subscription, "true") == 0){
					msgType = REQUEST_IIOT_READ_SUBSCRIPTION;
				}else if (strcmp(payload.subscription, "false") == 0){
					msgType = REQUEST_IIOT_READ;
				}else{
					msgType = UNKNOWN;
				}

			}else if (payload.data && payload.method_id){
				msgType = REQUEST_IIOT_METHOD;
			}else if(payload.data){
				msgType = REQUEST_COMPONENT;
				// TODO: how to identify write? read payload
				//msgTpe = REQUEST_IIOT_WRITE;
			}	
		} else{
			msgType = UNKNOWN;
		}

		if (msgType < REQUEST_ALL) // matching all request types
		{
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "got request msg on pubsub interface\n");
			// TODO: json_tokener_parse needed again, don't we keep somewhere the json object of the received data?
			/*if (json_object_object_get_ex(json_tokener_parse(decoded_data), "cmd", &cmd))
		{
			decoded_cmd = json_object_get_string(cmd);
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Json CMD:: %s\n", decoded_cmd);

			if (json_object_object_get_ex(json_tokener_parse(decoded_data), "data", &args))
			{
				decoded_args = json_object_get_string(args);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Json InternalData: %s\n", decoded_args);
			}
		}*/
			/*if (json_object_object_get_ex(parsed_json, "subscription", &subscription))
		{
			// enable or disable OPC UA subscription feature
			isSubscribed = json_object_get_string(subscription);
			if (0 == strcmp(isSubscribed, "TRUE"))
			{
				// TODO: free strdup string
				opcClient.nodeToSubscribe = strdup(decoded_data);
				opcClient.isSubscribed = "TRUE";
			}
			else
				opcClient.nodeToSubscribe = "null";
		}*/

			/*	if (payload.subscription != NULL && strcmp(payload.subscription, "true"))
			{	//strcmp has undefinded behaviour in case of NULL, therefore additional NULL condition added
				// enable or disable OPC UA subscription feature
				opcClient.isSubscribed = "TRUE";
				opcClient.nodeToSubscribe = strdup(payload.data);
			}
			else if (payload.subscription != NULL && strcmp(payload.subscription, "false"))
			{
				opcClient.isSubscribed = "FALSE";
				opcClient.nodeToSubscribe = "null";
				// TODO: shouldn' it be rather NULL?
			}
			else if (payload.subscription != NULL && strcmp(payload.subscription, "cancel"))
			{
				opcClient.isSubscribed = "CANCEL";
				opcClient.nodeToSubscribe = "null";
			}
			else
			{
				opcClient.isSubscribed = "UNKNOWN";
				opcClient.nodeToSubscribe = "null";
			}*/

			// this is the answer topic every request to the IIoT-Platform has to contain to be able to send the response
			//topicToPublish = json_object_get_string(answerTopic);
			// URL/IP of target device that can answer the request
			//craneAddress = json_object_get_string(url);
			//arbitrary id to match responses with initial request
			//req_id = json_object_get_string(request_id);
			// mode of the request, eiter 'read': a read of another device iiot-platform/opc ua server data space, or 'methodcall': a request that needs to be forwarded to another component of the target device
			// TODO: free strdup string
			// ! TODO: removed mode, track consequences
			//opcClient.mode = strdup(requestType);

			// TODO: ? remove strdup here
			//if (strlen(strdup(payload.req_id)) > 0)
			
			if (payload.req_id != NULL && strlen(payload.req_id) > 0)
			{
				// ! TODO: this is still on the requesting device, right? ReqIDSpecificNode is used for pub-sub extension to forward later the response back the the requesting component?
				// TODO: strdup here required?, free strdup string
				if (msgType != REQUEST_IIOT_READ_SUBSCRIPTION_CANCEL){
					retval = addRequestIdSpecificNode(common.server, payload.req_id); //removed strdup(payload.req_id)
					if (retval != UA_STATUSCODE_GOOD)
					{
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addRequestIdSpecificNode: Error Name = %s",
									UA_StatusCode_name(retval));
						free_payload_elements(&payload, UA_TRUE);
						UA_ByteString_clear(topic);
						UA_ByteString_delete(topic);
						UA_ByteString_clear(encodedBuffer);
    					UA_ByteString_delete(encodedBuffer);
						return;
					}
				}
			}
			else
			{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Req_ID cannot be empty: %s", payload.req_id);
				free_payload_elements(&payload, UA_TRUE);
				UA_ByteString_clear(topic);
						UA_ByteString_delete(topic);
						UA_ByteString_clear(encodedBuffer);
    					UA_ByteString_delete(encodedBuffer);
				return;
			}
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Node to Subscribe (?): %s", payload.data);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Request value: %s", "MODE VARIABLE DEPRECATED");
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Data value: %s", payload.data);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Topic value: %s", payload.answer_topic);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "URL value: %s", payload.device);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Req_ID value: %s", payload.req_id);
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Is Subscription Key Present: %s", payload.subscription);
			// TODO subscription check: use isSubscribed value here

			// start opc-ua client in a thread
			// TODO: strdup needed here?
			// TODO: free strdup strings
			// TODO: CERTS
			//UA_AppClient *opcClient = malloc(sizeof(UA_AppClient));
			request_data *requestConfig = (request_data*) malloc(sizeof(request_data));
			requestConfig->data = NULL;
			requestConfig->payload_dataArray = NULL;
			//requestConfig->hh = NULL;
			requestConfig->method_id = NULL;
			//requestConfig->monitoredItem = NULL;
			requestConfig->payload_data = NULL;
			requestConfig->payload_dataArraySize = 0;
			//requestConfig->publisher = NULL;
			requestConfig->reqId = NULL;
			requestConfig->sub_id = 0;
			requestConfig->subscription = NULL;
			requestConfig->terminateClient = NULL;
			requestConfig->url = NULL;
			requestConfig->client = NULL;
			requestConfig->command = NULL;
			requestConfig->component = NULL;
			requestConfig->url = safeStringCopy (payload.device);
			requestConfig->subscribedArraySize = 0;
			requestConfig->subscribedValuesArray = NULL;
			requestConfig->subscribedVariablesArray = NULL;
			requestConfig->payload_dataArraySize = payload.dataArraySize;
			if (payload.dataArraySize > 0){
				requestConfig->payload_dataArray = payload.dataArray;
				//does this work out?
			}else{
				requestConfig->payload_data = safeStringCopy(payload.data);
			}
			
			requestConfig->reqId = safeStringCopy(payload.req_id);
			//requestConfig->reqId_numeric = numeric_id(opcClient->reqId);
			requestConfig->component = safeStringCopy(payload.component);
			requestConfig->command = safeStringCopy(payload.command);
			requestConfig->subscription = safeStringCopy(payload.subscription);
			//requestConfig->data = safeStringCopy(payload.data);
			requestConfig->publisher.useJson = UA_TRUE;
			requestConfig->publisher.qos = iQos;
			//opcClient.sub_id = payload.sub_id;
			/*opcClient.url = strdup(payload.device);
			opcClient.payload = strdup(payload.data);
			opcClient.reqId = strdup(payload.req_id);
			opcClient.component = strdup(payload.component);
			opcClient.command = strdup(payload.command);
			opcClient.sub_id = strdup (payload.sub_id); */
			//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Payload: %s", requestConfig->payload_data);
			//strcpy(requestConfig->publisher.publishTopic, payload.answer_topic);
			requestConfig->publisher.publishTopic = safeStringCopy(payload.answer_topic);
			//opcClient->appConfig = &g_appConfig;
			requestConfig->val = NULL;
#ifdef OPC_UA_SECURITY
			//requestConfig->serverPrivateKey = malloc(sizeof(UA_ByteString));
			//UA_ByteString_copy(common.serverPrivateKey, requestConfig->serverPrivateKey);
			requestConfig->serverPrivateKey = common.serverPrivateKey;
			//requestConfig->serverCert = malloc(sizeof(UA_ByteString));
			//UA_ByteString_copy(common.serverCert, requestConfig->serverCert);
			requestConfig->serverCert = common.serverCert;
			//requestConfig->clientPrivateKey = malloc(sizeof(UA_ByteString));
			//UA_ByteString_copy(common.clientPrivateKey, requestConfig->clientPrivateKey);
			requestConfig->clientPrivateKey =common.clientPrivateKey;
			//requestConfig->clientCert = malloc(sizeof(UA_ByteString));
			//UA_ByteString_copy(common.clientCert, requestConfig->clientCert);
			requestConfig->clientCert = common.clientCert;
#endif

			//TODO: do payload check in a right place
			if (requestConfig->component == NULL)	{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Payload of received MQTT msg is wrong, component field is empty");
				free_payload_elements(&payload, UA_TRUE);
				UA_ByteString_clear(topic);
				UA_ByteString_delete(topic);
				UA_ByteString_clear(encodedBuffer);
    			UA_ByteString_delete(encodedBuffer);
				return;
			}
			if (requestConfig->command == NULL)	{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Payload of received MQTT msg is wrong, command field is empty");
				free_payload_elements(&payload, UA_TRUE);
				UA_ByteString_clear(topic);
				UA_ByteString_delete(topic);
				UA_ByteString_clear(encodedBuffer);
    			UA_ByteString_delete(encodedBuffer);
				return;
			}
			// TODO: can data field be empty in case of request to non-iiot component?
			if (requestConfig->payload_data == NULL && requestConfig->payload_dataArray == NULL && msgType != REQUEST_IIOT_READ_SUBSCRIPTION_CANCEL)	{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Payload of received MQTT msg is wrong, data field is empty");
				free_payload_elements(&payload, UA_TRUE);
				UA_ByteString_clear(topic);
				UA_ByteString_delete(topic);
				UA_ByteString_clear(encodedBuffer);
    			UA_ByteString_delete(encodedBuffer);
				return;
			}

			/*
			*Idenitify case of subscription cancelation, all other cases are tested in separate client thread
			*/
			//if (strcmp(opcClient->component, "iiot") == 0 && strcmp(opcClient->command, "read") == 0 && strcmp(opcClient->subscription, "cancel") == 0)
			if(msgType == REQUEST_IIOT_READ_SUBSCRIPTION_CANCEL)
			{ // cancel an active subscription
				//int n = list->firstIndex(list, opcClient.reqId);
				request_data *subscriptionClient;
				UA_Boolean listError = UA_FALSE;
				//if (hashtable_get(table, requestConfig->reqId, (void*) &subscriptionClient) != CC_OK){
				subscriptionClient = find_entry(hashTable, requestConfig->reqId);
				if (subscriptionClient == NULL){
					listError = UA_TRUE;
				}
				
				// clientList *tempClient;
				// if (n == -1){
				// 	listError = UA_TRUE;
				// }

				// if (!listError){
				// 	tempClient = (clientList *) list->at(list, n); 

				// 	if (tempClient == NULL){
				// 		listError = UA_TRUE;
				// 	}
				// 	//TODO: check if list finds always the right entry reliable, then remove reqId comparison
				// 	else if(strcmp(tempClient->req_id, opcClient.reqId) != 0){
				// 		listError = UA_TRUE;
				// 	}
				// }
				
				if (listError){
					UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,  "Received cancelation request for OPC UA client thread for request id %s. No hash table entry found to set termination signal.", requestConfig->reqId);	
				}
				else{
					//*(tempClient->terminateSignal) = UA_TRUE;
					subscriptionClient->terminateClient = UA_TRUE;
					UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Received cancelation request for OPC UA client thread for request id %s and set termination signal.", requestConfig->reqId);	
				}

			}
			else
			{
				//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "OPC UA Client Thread initialization\n");
				/* @brief start OPC UA client to connect to target device and forward request
				* - configure OPC UA pubsub extension to react on change of variable value in OPC UA address space
				* - the client stores the responses from the target device in that variable
				* - pubsub extension will take care of forwarding the value/status change to the requesting component via MQTT
				* */
				//rc = pthread_create(&clientThread, NULL, subscribeToNode, (void *)&opcClient);
				pthread_t *tempThread = malloc(sizeof(pthread_t));
				requestConfig->terminateClient = UA_FALSE;
				rc = pthread_create(tempThread, NULL, opc_ua_client, (void *)requestConfig);
				rc |= pthread_detach(*tempThread);
				free(tempThread);
				if (rc)
				{
					UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ERR; pthread_create() ret = %d", rc);
					free_payload_elements(&payload, UA_FALSE);
					free_UA_AppClient(requestConfig);
					UA_ByteString_clear(topic);
					UA_ByteString_delete(topic);
					UA_ByteString_clear(encodedBuffer);
    				UA_ByteString_delete(encodedBuffer);
					exit(-1);
				}
				else
				{
					// add thread (realising one instance of OPC UA client) to list and match it to request id
					// clientList *tempClient = malloc(sizeof(clientList));
					// tempClient->client = tempThread;
					// tempClient->req_id = strdup(opcClient.reqId);
					// bool *tempTerminateSignal = malloc(sizeof(bool));
					// tempTerminateSignal = UA_FALSE; // or *tempTerminateSignal?
					// tempClient->terminateSignal = tempTerminateSignal;
					// list->add(list, &tempClient);

					//hashtable_add(table, requestConfig->reqId, requestConfig);
					//check if key already exists
					// TODO: if for some reason the client is faster terminating than the hash entry is generated, may an error occurs.
					request_data *temp =  find_entry(hashTable, requestConfig->reqId);
					if (temp == NULL){
						HASH_ADD_KEYPTR(hh, hashTable, requestConfig->reqId, strlen(requestConfig->reqId), requestConfig); //what is hh?
					}
					else{
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "ERROR while adding new element to hashTable: key already exists! reqId: %s", requestConfig->reqId);
						free_payload_elements(&payload, UA_TRUE);
						UA_ByteString_clear(topic);
						UA_ByteString_delete(topic);
						UA_ByteString_clear(encodedBuffer);
    					UA_ByteString_delete(encodedBuffer);
						//free_UA_AppClient(requestConfig);
						exit(-1);
					}
				}
			}
			

			
		}
		else if (msgType == RESPONSE)
		{
			// ! TODO: just copied code from optimum device 2 - adapt it
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "got response msg on pubsub interface");

			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Data value: %s; Status: %s; Req_ID value: %s; Final_msg: %s", payload.data, payload.status, payload.req_id, payload.final_msg);

			// prepare json message that only contains "final_msg" & "data" key-value pairs to write on the response node "Y22691"

			// e.g {"final_msg":"true","data":{"status":"success","data":"skynet_0.1 running"}}

			json_object *jobj_data = json_object_new_object();
			json_object_object_add(jobj_data, "status", json_object_new_string(payload.status));
			json_object_object_add(jobj_data, "final_msg", json_object_new_string(payload.final_msg));
			json_object_object_add(jobj_data, "data", json_object_new_string(payload.data));

			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Prepared target component reponse JSON Message to write on Node %s: %s", payload.req_id, json_object_get_string(jobj_data));

			UA_StatusCode result = writeValueToRequestIdSpecificNode(common.server, UA_STRING((char *)json_object_get_string(jobj_data)), payload.req_id);
			if (result != UA_STATUSCODE_GOOD)
			{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Write to Variable: %s Error Name = %s",
							payload.req_id, UA_StatusCode_name(result));
			}
			else
			{
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Writing to Node %s is Successful", payload.req_id);
			}

			json_object_put(jobj_data);


			//TODO: solve differently, function must exit without deleting this node, trigger it somehow else
			// TODO: use open62541 timed callback to delete node after a certain time, does this callback work?
			//check for final_msg == true
			/*if (strcmp(payload.final_msg, "true") == 0){
				//remove node from address space
				
				result = UA_Server_deleteNode(common.server, UA_NODEID_STRING(1, payload.req_id), UA_FALSE);
				if (result != UA_STATUSCODE_GOOD)
				{
					UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Error while deleting reqeust specific node %s (Error Name = %s)",
								payload.req_id, UA_StatusCode_name(result));
				}
				else
				{
					UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Deleted request specific node %s from address space", payload.req_id);
				}

			} */
		}
		else if (msgType == UNKNOWN)
		{
			//TODO: error handling, could determine if this msg is a request or response
		}
	}
	else
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Invalid Subscription Topic\n");
		free_payload_elements(&payload, UA_TRUE);
		return;
	}
	free_payload_elements(&payload, UA_FALSE);
	UA_ByteString_clear(topic);
	UA_ByteString_delete(topic);
	UA_ByteString_clear(encodedBuffer);
    UA_ByteString_delete(encodedBuffer);
}


//-------------------------------------- Code to run the server --------------------------------------------------------------------

/*
 * @brief Initializes everything, opcua local server with mqtt pubsub connection and opcua client
 *
 * @param tranportProfile tranport profile of the app
 * @param networkAddressUrl network url of the connection
 */
static int
//run(UA_String *transportProfile, UA_NetworkAddressUrlDataType *networkAddressUrl, int opcPort) {
#ifdef OPC_UA_SECURITY
run(UA_String *transportProfile, char *mqttBrokerURL, int opcPort, char* system_id, char* serverCertPath, char* serverPrivateKeyPath, char* clientCertPath, char* clientPrivateKeyPath) {
#else
run(UA_String *transportProfile, char *mqttBrokerURL, int opcPort, char* system_id) {
#endif
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

	memset(&common, 0, sizeof(common_data));
	subscriber_data intraDevMQTTSubscriber;
	memset(&intraDevMQTTSubscriber, 0, sizeof(subscriber_data));
	//hashtable_new(&table);
	
    /* Return value initialized to Status Good */
    UA_StatusCode retval = UA_STATUSCODE_GOOD;
    clientPid = 0;
	
	// Valid Arbitary Users
    //user[0] = "admin";
    //user[1] = "root";
	
    //UA_Server *server = UA_Server_new();
	common.server = UA_Server_new();
   // UA_ServerConfig *config = UA_Server_getConfig(server);
	common.serverConfig = UA_Server_getConfig(common.server);
#ifdef OPC_UA_SECURITY

    /* Load the trustlist */
    size_t trustListSize = 0;
   // if(argc > 3)
   //     trustListSize = (size_t)argc-3;
    UA_STACKARRAY(UA_ByteString, trustList, trustListSize);
    //for(size_t i = 0; i < trustListSize; i++)
    //    trustList[i] = loadFile(argv[i+3]);

    /* Loading of a issuer list, not used in this application */
    size_t issuerListSize = 0;
    UA_ByteString *issuerList = NULL;

    /* Loading of a revocation list currently unsupported */
    UA_ByteString *revocationList = NULL;
    size_t revocationListSize = 0;


	common.serverPrivateKey = loadFile(serverPrivateKeyPath);
	common.serverCert = loadFile(serverCertPath);
	common.clientPrivateKey = loadFile(clientPrivateKeyPath);
	common.clientCert =  loadFile(clientCertPath);
	if (common.serverPrivateKey->length == 0 || common.serverCert->length == 0 || common.clientPrivateKey->length == 0 || common.clientCert->length == 0 )
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Error while loading certificates");
        return EXIT_FAILURE;
	}
		
	//UA_ByteString privateKey = loadFile("optimumServerCert.der");
	//UA_ByteString serverCert = loadFile("optimumServerPrivateKey.der");

	//printf("private key file length: %ld\n",common.privateKey.length);
	//printf("private key file path: %s\n", serverPrivateKeyPath);
	UA_ServerConfig_setDefaultWithSecurityPolicies(common.serverConfig, opcPort,
                                                       common.serverCert, common.serverPrivateKey,
                                                       trustList, trustListSize,
                                                       issuerList, issuerListSize,
                                                       revocationList, revocationListSize);
#else
	UA_ServerConfig_setMinimal(common.serverConfig, opcPort, NULL);
#endif

	//set minimal limit for subscription intervall, client can request only values higher than this 
	UA_DurationRange pub;
	pub.min = SUBSCRIPTION_INTERVAL;
	pub.max = 3600.0 * 1000.0;
	UA_DurationRange sample;
	sample.min = SUBSCRIPTION_INTERVAL;
	sample.max = 24.0 * 3600.0 * 1000.0;

	common.serverConfig->publishingIntervalLimits =pub;//UA_DURATIONRANGE(SUBSCRIPTION_INTERVAL, 3600.0 * 1000.0);
	common.serverConfig->samplingIntervalLimits =sample; // UA_DURATIONRANGE(SUBSCRIPTION_INTERVAL, 24.0 * 3600.0 * 1000.0);

	//UA_ServerConfig_setMinimal(config, opcPort, NULL);
	
    //memset(&g_appConfig, 0, sizeof(UA_AppConfig));
	//list = CList_Init(sizeof(clientList));     /* Initialization of list to track opc ua client thread instances  */

    //g_appConfig.useJson = true;
   // g_appConfig.server = server;
   // g_appConfig.publishInterval = PUBLISH_INTERVAL;
    //g_appConfig.qos = iQos;
    //g_appConfig.publishTopic = (char*)malloc(sizeof(char)*strlen(PUBLISH_TOPIC));
    //.subscribeTopic = (char*)malloc(sizeof(char)*strlen(SUBSCRIBE_TOPIC));
    //strcpy(g_appConfig.publishTopic, PUBLISH_TOPIC);
    //strcpy(g_appConfig.subscribeTopic, SUBSCRIBE_TOPIC);
    //g_appConfig.arguments = (char*)malloc(sizeof(char)*strlen(clientArguments));

	common.mqttBrokerURL.networkInterface = UA_STRING_NULL;
	common.mqttBrokerURL.url = UA_STRING(mqttBrokerURL);
	//TODO: seems currently useless, since its hardcoded to 0 in inner functions:
	intraDevMQTTSubscriber.qos = iQos; 
	intraDevMQTTSubscriber.subscribeTopic = strdup(SUBSCRIBE_TOPIC);


    //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "run;: publish Topic = %s, subsribe topic %s",
    //        g_appConfig.publishTopic, g_appConfig.subscribeTopic);

    /* Add the PubSub network layer implementation to the server config.
     * The TransportLayer is acting as factory to create new connections
     * on runtime. Details about the PubSubTransportLayer can be found inside the
     * tutorial_pubsub_connection */
   // config->pubsubTransportLayers = (UA_PubSubTransportLayer *) UA_calloc(1, sizeof(UA_PubSubTransportLayer));
	common.serverConfig->pubsubTransportLayers = (UA_PubSubTransportLayer *) UA_calloc(1, sizeof(UA_PubSubTransportLayer));
    if(!common.serverConfig->pubsubTransportLayers) {
        UA_Server_delete(common.server);
        return EXIT_FAILURE;
    }

    common.serverConfig->pubsubTransportLayers[0] = UA_PubSubTransportLayerMQTT();
    common.serverConfig->pubsubTransportLayersSize++;

// --- add data model of device, example here: optimum crane ---
    /* create nodes from nodeset */
    retval = namespace_optimum_generated(common.server);
    if(retval != UA_STATUSCODE_GOOD) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Adding the optimum namespace failed. Please check previous error output.");
        UA_Server_delete(common.server);
        return EXIT_FAILURE;
	}
// --- data model END ---

	// retval = addDeviceStatusVariable(common.server);
	// if (retval != UA_STATUSCODE_GOOD) {
	// 	UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Device Status: Error Name = %s",
	// 			UA_StatusCode_name(retval));
	// 	return EXIT_FAILURE;
	// }
	
	/*retval = addReservationStatusVariable(server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Reservation Status: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	}
	*/

	retval = addMethod_JSON(common.server);   
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addMethod_JSON: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	} 

	retval = addGoToMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addGoToMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	} 

	retval = addGoToAGVMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addGoToAGVMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	} 

    retval = addComeToMeMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addComeToMeMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	} 

    retval = addFollowMeMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addFollowMeMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	} 

	retval = addStopMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addStopMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	}

	retval = addReservationMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addReservationMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	}

	retval = addCraneModeMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addCraneModeMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	}

	retval = addManualTandemMethod(common.server);
	if (retval != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addManualTandemMethod: Error Name = %s",
				UA_StatusCode_name(retval));
		return EXIT_FAILURE;
	}

	// Check if environmental variable is set, if yes then set the System_ID accordingly
	if (strcmp(system_id, "") != 0 && system_id != NULL){
		UA_String ua_system_id = UA_STRING(system_id);
		UA_Variant var_system_id;
		UA_Variant_init(&var_system_id);
		UA_Variant_setScalar(&var_system_id, &ua_system_id, &UA_TYPES[UA_TYPES_STRING]);
		retval = UA_Server_writeValue(common.server, UA_NODEID_NUMERIC(3, 6001), var_system_id);	
		if (retval != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "modify System_ID: Error Name = %s",
					UA_StatusCode_name(retval));
			//return EXIT_FAILURE;
		}
	}

	   /* API calls */
	    /* Add PubSubConnection */
	    //retval |= addPubSubConnection(&g_appConfig, networkAddressUrl);
		retval |= addPubSubConnection(&common);
	    if (retval != UA_STATUSCODE_GOOD)
		return EXIT_FAILURE;

	    //retval = sub_mqtt_setup(&g_appConfig);
		retval = sub_mqtt_setup(&common, &intraDevMQTTSubscriber);
	    if (retval != UA_STATUSCODE_GOOD) {
	    	UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "sub_mqtt_setup: Error Name = %s",
	    	        UA_StatusCode_name(retval));
	    	return EXIT_FAILURE;
	    }
    
    UA_PubSubConnection *connection = UA_PubSubConnection_findConnectionbyId(common.server, common.connectionIdent);

    if(!connection) {
        UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                       "Could not create a PubSubConnection");
        UA_Server_delete(common.server);
        return -1;
    } 

    retval = UA_Server_run(common.server, &running);
    UA_Server_delete(common.server);
#ifdef OPC_UA_SECURITY
	free (serverCertPath);
	free (serverPrivateKeyPath);
	free (clientCertPath);
	free (clientPrivateKeyPath);
#endif

   // free((void*)g_appConfig.publishTopic);
   // free((void*)g_appConfig.subscribeTopic);
    //opt_free(); - for what is it used?
	//(void) pthread_join(clientThread, NULL);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*
 * @brief starting point of the program, process the arguments and runs app accordingly
 *
 * opcua_mqtt_app --help
 */
int main(int argc, char **argv) {

	int opt = 0;
	char *mqttBrokerAddress = "127.0.0.1:1883";
	int opcPort = 4840;
	//char *out_fname = NULL;
#ifdef OPC_UA_SECURITY
	char *serverCertPath;
	char *serverPrivateKeyPath;
	char *clientCertPath;
	char *clientPrivateKeyPath;
#endif
	char *system_id = "crane1";
	//UA_ByteString serverCert, privateKey;

	while ((opt = getopt(argc, argv, "c:m:o:p:s:d:q:")) != -1)
	{
		switch (opt)
		{
		case 'm':
			mqttBrokerAddress = optarg;
			printf("MQTT Broker Address is set to: %s\n", mqttBrokerAddress);
			break;
		case 'o':
			opcPort = atoi(optarg);
			printf("OPC UA port is set to: %d\n", opcPort);
			break;
		case 's':
			system_id = safeStringCopy(optarg);
			printf("System_ID is set to: %s\n", system_id);
			break;
#ifdef OPC_UA_SECURITY
		case 'c':
			serverCertPath = safeStringCopy(optarg);
			printf("server certificate is set to: %s\n", serverCertPath);
			break;
		case 'd':
			clientCertPath = safeStringCopy(optarg);
			printf("client certificate is set to: %s\n", clientCertPath);
			break;
		case 'p':
			serverPrivateKeyPath = safeStringCopy(optarg);
			printf("server private key file is set to: %s\n", serverPrivateKeyPath);
			break;
		case 'q':
			clientPrivateKeyPath = safeStringCopy(optarg);
			printf("client private key file is set to: %s\n", clientPrivateKeyPath);
			break;
		//TODO: add trust list, issuer list
#endif
		case '?':
			if (optopt == 'm')
			{
			/* Case when user enters the command as
			* $ ./program -m
			*/
				printf("Option m requires a MQTT Broker URL with port as an argument. E. g. -m 127.0.0.1:1883\n");

			}
			else if (optopt == 'o')
			{
				/* Case when user enters the command as
				* # ./program -o
				*/
				printf("Option o requires a port number as an argument. E. g. -o 4840\n");
			}
#ifdef OPC_UA_SECURITY
			else if (optopt == 'c')
			{
				/* Case when user enters the command as
				* # ./program -c
				*/
				printf("Option c requires a server certificate as an argument. E. g. -c server-certificate.der\n");
			}
			else if (optopt == 'p')
			{
				/* Case when user enters the command as
				* # ./program -p
				*/
				printf("Option p requires a server private key file as an argument. E. g. -p private-key.der\n");
			}
			else if (optopt == 'd')
			{
				/* Case when user enters the command as
				* # ./program -d
				*/
				printf("Option d requires a client certificate as an argument. E. g. -d server-certificate.der\n");
			}
			else if (optopt == 'q')
			{
				/* Case when user enters the command as
				* # ./program -q
				*/
				printf("Option q requires a client private key file as an argument. E. g. -q private-key.der\n");
			}
#endif
			else
			{
				printf("Invalid option received\n");
			}
			break;
		}
	}
	mqttBrokerAddress = mergeStrings("opc.mqtt://", mqttBrokerAddress);
	//printf("final MQTT broker address string: %s\n", mqttBrokerAddress);

#ifdef OPC_UA_SECURITY
	printf("---> OPC UA Security enabled <---\n");
#endif
	UA_String transportProfile = UA_STRING(TRANSPORT_PROFILE_URI);
	//char mqttBrokerAddress[50] = BROKER_ADDRESS_URL;
	//char *mqttBrokerAddress = BROKER_ADDRESS_URL;
	
	/* if (argc > 1 && (atoi(argv[1]) > 0) && (atoi(argv[1]) < 10000)){ //10000 arbitrary limit to check for ports between 1 and 9999
		//strncat(mqttBrokerAddress, argv[1], 5);
		mqttBrokerAddress = mergeStrings(mqttBrokerAddress, argv[1]);
		printf("first program argument:%s\n", argv[1]);
	}else{
		//strncat(mqttBrokerAddress, BROKER_PORT, 5);
		mqttBrokerAddress = mergeStrings(mqttBrokerAddress, BROKER_PORT);
	}

	if (argc > 2 && (atoi(argv[2]) > 0) && (atoi(argv[2]) < 10000)){ //10000 arbitrary limit to check for ports between 1 and 9999
		opcPort = atoi(argv[2]);
		printf("second program argument:%s\n", argv[2]);
	}else{
		opcPort = 4840;
	} */
    //UA_NetworkAddressUrlDataType networkAddressUrl = {UA_STRING_NULL , UA_STRING(BROKER_ADDRESS_URL)};
	//UA_NetworkAddressUrlDataType networkAddressUrl = {UA_STRING_NULL , UA_STRING(mqttBrokerAddress)};
    
	// Initialize the qos to 2 (default value)
    iQos = 0;   
	// DONE: change QoS for PubSub to 0 since we ar working on localhost
	// TODO: check if that change works as expected

    //return run(&transportProfile, &networkAddressUrl, opcPort);
#ifdef OPC_UA_SECURITY
	return run(&transportProfile, mqttBrokerAddress, opcPort, system_id, serverCertPath, serverPrivateKeyPath, clientCertPath, clientPrivateKeyPath);
#else
	return run(&transportProfile, mqttBrokerAddress, opcPort, system_id);
#endif
}
