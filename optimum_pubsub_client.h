
#include "ua_pubsub.h"
#include "iiot_platform.h"
#include <pthread.h>
#define GOTO            1
#define COMETOME        2
#define FOLLOWME        3
#define FOLLOWMACHINE   4
#define TRUE            1
#define FALSE           0
int MODE;

extern char DockerBridgeIP[15];
extern int appMode;
extern uint32_t PosXCount;
extern uint32_t PosYCount;
extern uint32_t PosZCount;

extern int clientTerminateSignal;
extern char clientArguments[200];

void *opcUaClient(void *arg);
void *goToPosition(void *arg);
void *comeToMe(void *arg);
void *followMe(void *arg);
void clientInitialization(void *arg);
void writeValueToNode(void);
