#!/bin/bash

echo "Starting the OPTIMUM IIoT component container"

# start the opc-ua server
echo "Start IIoT Platform"
if [ $VALGRIND == "ON" ]
then
     valgrind --leak-check=full /tmp/iiot-component/build/bin/iiot_platform -m ${MQTTBrokerAddress} -o ${opcPort} -s ${SYSTEM_ID} -c /tmp/iiot-component/open62541_certs/server_cert.der -p /tmp/iiot-component/open62541_certs/server_key.der -d /tmp/iiot-component/open62541_certs/client_cert.der -q /tmp/iiot-component/open62541_certs/client_key.der
else
    /tmp/iiot-component/build/bin/iiot_platform -m ${MQTTBrokerAddress} -o ${opcPort} -s ${SYSTEM_ID} -c /tmp/iiot-component/open62541_certs/server_cert.der -p /tmp/iiot-component/open62541_certs/server_key.der -d /tmp/iiot-component/open62541_certs/client_cert.der -q /tmp/iiot-component/open62541_certs/client_key.der
fi