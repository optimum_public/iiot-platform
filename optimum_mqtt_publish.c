
#include "open62541/server.h"
#include "ua_pubsub.h"
#include "ua_network_pubsub_mqtt.h"
#include "open62541/plugin/log_stdout.h"
#include <signal.h>
#include "optimum_mqtt_publish.h"

#define PUBLISHER_METADATAQUEUENAME  "MetaDataTopic"
#define PUBLISHER_METADATAUPDATETIME 0

char *reqId;
/*
 * @brief Adds a PDS
 *
 * @param server pointer the server
 * @param publishedDataSetIdent pointer to the nodeId, where the newly generated PDS id will be saved
 */
UA_StatusCode
PublishedDataSet(UA_Server *server, UA_NodeId *publishedDataSetIdent) {
    /* The PublishedDataSetConfig contains all necessary public
    * informations for the creation of a new PublishedDataSet */
    UA_PublishedDataSetConfig publishedDataSetConfig;
    memset(&publishedDataSetConfig, 0, sizeof(UA_PublishedDataSetConfig));
    publishedDataSetConfig.publishedDataSetType = UA_PUBSUB_DATASET_PUBLISHEDITEMS;
    publishedDataSetConfig.name = UA_STRING("OPTIMUM Device PDS");
    /* Create new PublishedDataSet based on the PublishedDataSetConfig. */
    return UA_Server_addPublishedDataSet(server, &publishedDataSetConfig, publishedDataSetIdent).addResult;
}


void
SubscriptionOperationDataSetField(UA_Server *server, UA_NodeId publishedDataSetIdent) {
	
    UA_DataSetFieldConfig dataSetFieldConfig;
    memset(&dataSetFieldConfig, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig.field.variable.fieldNameAlias = UA_STRING("ResponseNode");
    dataSetFieldConfig.field.variable.promotedField = UA_FALSE;

    // recent value of variableNode "commonNode" from the address space is included
    dataSetFieldConfig.field.variable.publishParameters.publishedVariable = UA_NODEID_STRING(1, reqId); //  variableNode "commonNode"

    dataSetFieldConfig.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig, NULL);		
}

void
ReadOperationDataSetField(UA_Server *server, UA_NodeId publishedDataSetIdent) {
	
    UA_DataSetFieldConfig dataSetFieldConfig;
    memset(&dataSetFieldConfig, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    //dataSetFieldConfig.field.variable.fieldNameAlias = UA_STRING("ResponseNode");
    dataSetFieldConfig.field.variable.fieldNameAlias = UA_STRING(reqId);
    dataSetFieldConfig.field.variable.promotedField = UA_FALSE;

    // recent value of variableNode "commonNode" from the address space is included
    dataSetFieldConfig.field.variable.publishParameters.publishedVariable = UA_NODEID_STRING(1, reqId); //  variableNode "commonNode"

    dataSetFieldConfig.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
    UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig, NULL);		
}

/*
* @brief Adds a writerGroup to the server
*
* @param appConfig pointer to the global appConfig
*/
UA_StatusCode
//WriterGroup(UA_AppConfig *appConfig) {
WriterGroup(common_data *common, publisher_data *publisher) {
	
	char *topic = publisher->publishTopic;
	int interval = publisher->publishInterval;
	bool useJson = publisher->useJson;
    /* Now we create a new WriterGroupConfig and add the group to the existing PubSubConnection. */
    UA_WriterGroupConfig writerGroupConfig;
    memset(&writerGroupConfig, 0, sizeof(UA_WriterGroupConfig));
    writerGroupConfig.name = UA_STRING("Demo WriterGroup");
    writerGroupConfig.publishingInterval = interval;
    writerGroupConfig.enabled = UA_FALSE;
    writerGroupConfig.writerGroupId = 62541;
    // this id is NOT the reference to the created writer group, the ref to the newly created writergroup is automatically created and return by weitergroup creation function , data type UA_NodeId
   //writerGroupConfig.writerGroupId = appConfig->reqId_numeric;
    UA_UadpWriterGroupMessageDataType *writerGroupMessage;

    /* decide whether to use JSON or UADP encoding*/
    UA_JsonWriterGroupMessageDataType *Json_writerGroupMessage;

    if(useJson) {
        writerGroupConfig.encodingMimeType = UA_PUBSUB_ENCODING_JSON;
        writerGroupConfig.messageSettings.encoding             = UA_EXTENSIONOBJECT_DECODED;

        writerGroupConfig.messageSettings.content.decoded.type = &UA_TYPES[UA_TYPES_JSONWRITERGROUPMESSAGEDATATYPE];
        /* The configuration flags for the messages are encapsulated inside the
         * message- and transport settings extension objects. These extension
         * objects are defined by the standard. e.g.
         * UadpWriterGroupMessageDataType */
        Json_writerGroupMessage = UA_JsonWriterGroupMessageDataType_new();
        /* Change message settings of writerGroup to send PublisherId,
         * DataSetMessageHeader, SingleDataSetMessage and DataSetClassId in PayloadHeader
         * of NetworkMessage */
        Json_writerGroupMessage->networkMessageContentMask =
            (UA_JsonNetworkMessageContentMask)(UA_JSONNETWORKMESSAGECONTENTMASK_NETWORKMESSAGEHEADER |
            (UA_JsonNetworkMessageContentMask)UA_JSONNETWORKMESSAGECONTENTMASK_DATASETMESSAGEHEADER |
            (UA_JsonNetworkMessageContentMask)UA_JSONNETWORKMESSAGECONTENTMASK_SINGLEDATASETMESSAGE |
            (UA_JsonNetworkMessageContentMask)UA_JSONNETWORKMESSAGECONTENTMASK_PUBLISHERID |
            (UA_JsonNetworkMessageContentMask)UA_JSONNETWORKMESSAGECONTENTMASK_DATASETCLASSID);
        writerGroupConfig.messageSettings.content.decoded.data = Json_writerGroupMessage;
    }

    else
    {
        writerGroupConfig.encodingMimeType = UA_PUBSUB_ENCODING_UADP;
        writerGroupConfig.messageSettings.encoding             = UA_EXTENSIONOBJECT_DECODED;
        writerGroupConfig.messageSettings.content.decoded.type = &UA_TYPES[UA_TYPES_UADPWRITERGROUPMESSAGEDATATYPE];
        /* The configuration flags for the messages are encapsulated inside the
         * message- and transport settings extension objects. These extension
         * objects are defined by the standard. e.g.
         * UadpWriterGroupMessageDataType */
        writerGroupMessage  = UA_UadpWriterGroupMessageDataType_new();
        /* Change message settings of writerGroup to send PublisherId,
         * WriterGroupId in GroupHeader and DataSetWriterId in PayloadHeader
         * of NetworkMessage */
        writerGroupMessage->networkMessageContentMask =
            (UA_UadpNetworkMessageContentMask)(UA_UADPNETWORKMESSAGECONTENTMASK_PUBLISHERID |
            (UA_UadpNetworkMessageContentMask)UA_UADPNETWORKMESSAGECONTENTMASK_GROUPHEADER |
            (UA_UadpNetworkMessageContentMask)UA_UADPNETWORKMESSAGECONTENTMASK_WRITERGROUPID |
            (UA_UadpNetworkMessageContentMask)UA_UADPNETWORKMESSAGECONTENTMASK_PAYLOADHEADER);
        writerGroupConfig.messageSettings.content.decoded.data = writerGroupMessage;
    }


    /* configure the mqtt publish topic */
    UA_BrokerWriterGroupTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerWriterGroupTransportDataType));
    /* Assign the Topic at which MQTT publish should happen */
    /*ToDo: Pass the topic as argument from the writer group */

    brokerTransportSettings.queueName = UA_STRING(topic);
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;

    /* Choose the QOS Level for MQTT */
    switch(publisher->qos)
    {
    case 0:
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_ATMOSTONCE;
    	break;
    case 1:
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_ATLEASTONCE;
    	break;
    case 2:
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_EXACTLYONCE;
    	break;
    default:
    	UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
    	        "addWriterGroup: invalid qos received, set to default (2_EXACTLY ONCE)");
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_EXACTLYONCE;
    	break;
    }
	UA_StatusCode retval = UA_STATUSCODE_GOOD;
    /* Encapsulate config in transportSettings */
    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERWRITERGROUPTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

    writerGroupConfig.transportSettings = transportSettings;

    retval = UA_Server_addWriterGroup(common->server, common->connectionIdent, &writerGroupConfig, &publisher->writerGroupIdentifier);
	if (retval == UA_STATUSCODE_GOOD)
		//UA_Server_setWriterGroupOperational(common->server, publisher->writerGroupIdentifier);
    //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "WriterGroupId in addWriterGroup = %u", appConfig->writerGroupIdentifier.identifier.numeric);

    if (useJson) {
        UA_JsonWriterGroupMessageDataType_delete(Json_writerGroupMessage);
    }

    if (!useJson && writerGroupMessage) {
        UA_UadpWriterGroupMessageDataType_delete(writerGroupMessage);
    }
    return retval;
}

UA_StatusCode
//DataSetWriter(UA_AppConfig *appConfig) {
DataSetWriter(common_data *common, publisher_data *publisher) {
    /* We need now a DataSetWriter within the WriterGroup. This means we must
     * create a new DataSetWriterConfig and add call the addWriterGroup function. */
	
    UA_StatusCode retval = UA_STATUSCODE_GOOD;
    char *topic = publisher->publishTopic;
    UA_DataSetWriterConfig dataSetWriterConfig;
    memset(&dataSetWriterConfig, 0, sizeof(UA_DataSetWriterConfig));
    dataSetWriterConfig.name = UA_STRING("Demo DataSetWriter");
    dataSetWriterConfig.dataSetWriterId = 100;
   // dataSetWriterConfig.dataSetWriterId = appConfig->reqId_numeric;
    //appConfig->datasetWriterID = dataSetWriterConfig.dataSetWriterId;
    dataSetWriterConfig.keyFrameCount = 10;

#ifdef UA_ENABLE_JSON_ENCODING
    UA_JsonDataSetWriterMessageDataType jsonDswMd;
    UA_ExtensionObject messageSettings;
    if(publisher->useJson) {
        /* JSON config for the dataSetWriter */
        /*jsonDswMd.dataSetMessageContentMask = (UA_JsonDataSetMessageContentMask)
            (UA_JSONDATASETMESSAGECONTENTMASK_DATASETWRITERID |
             UA_JSONDATASETMESSAGECONTENTMASK_SEQUENCENUMBER |
             UA_JSONDATASETMESSAGECONTENTMASK_STATUS |
             UA_JSONDATASETMESSAGECONTENTMASK_METADATAVERSION |
             UA_JSONDATASETMESSAGECONTENTMASK_TIMESTAMP);*/
             jsonDswMd.dataSetMessageContentMask =UA_JSONDATASETMESSAGECONTENTMASK_NONE;

        messageSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
        messageSettings.content.decoded.type = &UA_TYPES[UA_TYPES_JSONDATASETWRITERMESSAGEDATATYPE];
        messageSettings.content.decoded.data = &jsonDswMd;

        dataSetWriterConfig.messageSettings = messageSettings;
    }
#endif
    /*TODO: Modify MQTT send to add DataSetWriters broker transport settings */
    /*TODO: Pass the topic as argument from the writer group */
    /*TODO: Publish Metadata to metaDataQueueName */
    /* configure the mqtt publish topic */
    UA_BrokerDataSetWriterTransportDataType brokerTransportSettings;
    memset(&brokerTransportSettings, 0, sizeof(UA_BrokerDataSetWriterTransportDataType));

    /* Assign the Topic at which MQTT publish should happen */
    brokerTransportSettings.queueName = UA_STRING(topic);
    brokerTransportSettings.resourceUri = UA_STRING_NULL;
    brokerTransportSettings.authenticationProfileUri = UA_STRING_NULL;
    brokerTransportSettings.metaDataQueueName = UA_STRING(PUBLISHER_METADATAQUEUENAME);
    brokerTransportSettings.metaDataUpdateTime = PUBLISHER_METADATAUPDATETIME;

    /* Choose the QOS Level for MQTT */
    switch(publisher->qos)
    {
    case 0:
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_ATMOSTONCE;
    	break;
    case 1:
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_ATLEASTONCE;
    	break;
    case 2:
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_EXACTLYONCE;
    	break;
    default:
    	UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
    	        "addDataSetWriter: invalid qos received, set to default (2_EXACTLY ONCE)");
    	brokerTransportSettings.requestedDeliveryGuarantee = UA_BROKERTRANSPORTQUALITYOFSERVICE_EXACTLYONCE;
    	break;
    }

    /* Encapsulate config in transportSettings */
    UA_ExtensionObject transportSettings;
    memset(&transportSettings, 0, sizeof(UA_ExtensionObject));
    transportSettings.encoding = UA_EXTENSIONOBJECT_DECODED;
    transportSettings.content.decoded.type = &UA_TYPES[UA_TYPES_BROKERDATASETWRITERTRANSPORTDATATYPE];
    transportSettings.content.decoded.data = &brokerTransportSettings;

    dataSetWriterConfig.transportSettings = transportSettings;
    retval = UA_Server_addDataSetWriter(common->server, publisher->writerGroupIdentifier, publisher->publishedDataSetIdent,
                               &dataSetWriterConfig, &publisher->DataSetWriterIdentifier);

    //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "WriterGroupId in addDataSetWriter = %u",       appConfig->writerGroupIdentifier.identifier.numeric);

    return retval;
}

/*
* @brief Adds all neccesary components for opcua publisher over MQTT
*
* @param appConfig pointer to the global appConfig
*/
//UA_StatusCode setup_json(UA_AppConfig *appConfig)
UA_StatusCode setup_json(common_data *commonPtr, request_data *requestConfig)
{
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;
	reqId = requestConfig->reqId;
	
	PublishedDataSet(commonPtr->server, &requestConfig->publisher.publishedDataSetIdent);
    // TODO: why differenciating between read and subscription? both called functions have exactly the same content!?!
	//if(0 == strcmp(appConfig->operation, "read")){
		ReadOperationDataSetField(commonPtr->server, requestConfig->publisher.publishedDataSetIdent);
	//}
	//else if(0 == strcmp(appConfig->operation, "method_call")){
	//    SubscriptionOperationDataSetField(appConfig->server, appConfig->publishedDataSetIdent);
	//}

	WriterGroup(commonPtr, &requestConfig->publisher);
    //TODO: check if disabled group lets still add writer
   // UA_Server_setWriterGroupDisabled(commonPtr->server, requestConfig->publisher.writerGroupIdentifier);
	retVal = DataSetWriter(commonPtr, &requestConfig->publisher);
	    if (retVal != UA_STATUSCODE_GOOD) {
	    	UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addDataSetWriter: Error Name = %s", UA_StatusCode_name(retVal));
	    }

	return retVal;
}


UA_StatusCode
DataSetField(UA_Server *server, UA_NodeId publishedDataSetIdent, char *Id) {

    UA_DataSetFieldConfig dataSetFieldConfig;
    memset(&dataSetFieldConfig, 0, sizeof(UA_DataSetFieldConfig));
    dataSetFieldConfig.dataSetFieldType = UA_PUBSUB_DATASETFIELD_VARIABLE;

    dataSetFieldConfig.field.variable.fieldNameAlias = UA_STRING("PAYLOAD");
    dataSetFieldConfig.field.variable.promotedField = UA_FALSE;

    dataSetFieldConfig.field.variable.publishParameters.publishedVariable = UA_NODEID_STRING(1, Id); 

    dataSetFieldConfig.field.variable.publishParameters.attributeId = UA_ATTRIBUTEID_VALUE;
	
    return UA_Server_addDataSetField(server, publishedDataSetIdent, &dataSetFieldConfig, NULL).result;
	
}

/*
* @brief Adds all neccesary components for opcua publisher over MQTT
*
* @param appConfig pointer to the global appConfig
*/
//UA_StatusCode setup_publish_message(UA_AppConfig *appConfig, char *reqId)
UA_StatusCode setup_publish_message(common_data *commonPtr, request_data *requestConfig)
{
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;

 	retVal = PublishedDataSet(commonPtr->server, &requestConfig->publisher.publishedDataSetIdent);
	if (retVal != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "PublishedDataSet: Error Name = %s",
				UA_StatusCode_name(retVal));
	}
	retVal = DataSetField(commonPtr->server, requestConfig->publisher.publishedDataSetIdent, requestConfig->reqId);
	if (retVal != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "DataSetField: Error Name = %s",
				UA_StatusCode_name(retVal));
	}

	retVal = WriterGroup(commonPtr, &requestConfig->publisher);
	if (retVal != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addWriterGroup: Error Name = %s",
				UA_StatusCode_name(retVal));
	}
    UA_Server_setWriterGroupDisabled(commonPtr->server, requestConfig->publisher.writerGroupIdentifier);

	retVal = DataSetWriter(commonPtr, &requestConfig->publisher);
	if (retVal != UA_STATUSCODE_GOOD) {
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "addDataSetWriter: Error Name = %s",
				UA_StatusCode_name(retVal));
	}

	return retVal;
}