OPTIMUM IIoT Platform based on OPC UA stack [open62541](https://github.com/open62541/open62541) (version 1.2.4), available on GitHub, and includes pub/sub extension of OPC UA Part 14.

**tl;dr**
```
The IIoT-Platform is responsible for non-deterministic 1communication between machines
and between human and machine. The human, called operator in the following, uses a
Human-Machine-Interface (HMI) to interact with the machines. This HMI can be a computer,
tablet or smartphone.
The platform has three main components: external OPC UA interface, internal MQTT interface
and a device information data model. The combination of these components allows the IIoT
Platform to forward OPC UA request from outside to other components of the same device
via MQTT messages and vice versa. The data model of the platform can be access via OPC UA
(read-only) and MQTT (only internal, write access). Further information on the components is
presented in the following.
```
detailed information about purpose and functionality of the IIoT Platform: [PDF file](https://itea4.org/project/workpackage/document/download/7432/16043_OPTIMUM_Final_Implementation_IIoTPlatform_API_SDK_2ndPrototype.pdf)


<!---If the testbed shall be compiled together with the open62541 library, enable it during cmake configuration with cmake .. -DIMD_OPCUA_MQTT_ANALYSIS=ON-->

**Dependencies:**

- python3
- cmake3

**Submodules (downloaded during docker installation automatically, otherwise see instructions below):**

- json-c, see [Github project](https://github.com/json-c/json-c)
- uthash, see [Github project](https://github.com/troydhanson/uthash)
<!--- - ua-nodeset, see [Github project](https://github.com/OPCFoundation/UA-Nodeset)
<!--- Collections-C, see [Github project](https://github.com/srdja/Collections-C)

 Addition Requirements for testbed (opcua_mqtt_analysis):

- Mosquitto Broker, see [Mosquitto Website](https://mosquitto.org/download/)
-->
***Update of submodules 
Update of certain submodule to specific commit (e.g. to avoid build issues)
```
cd /deps/${submodule}                   # go to referenced submodule
git log                                 # determine hash value of specific commit (git reflog)
git checkout 177ea6d9 (e.g.)            # 
cd ../..                                # return to main iiot_component git
git status                              #
git diff /deps/${submodule}             # make sure hash of referenced commit hash changed accordingly
git commit -am "meaningful comment"     #
```
**Installation with Docker:**
```
docker build -t optimum_iiot:latest .
docker run -rm optimum_iiot:latest
```
**Build Options:**
Environment variables are defined in the Dockerfile to activate security and set OPC UA port and MQTT broker URL and port, e.g.:
- ENV OPC_UA_SECURITY=OFF
- ENV OPC_UA_OPENSSL=OFF
- ENV NXP_SE050=OFF (not implemented yet)
- ENV MQTTBrokerAddress 127.0.0.1:1883
- ENV opcPort 4840


**Installation without Docker:**
```
git clone https://gitlab.amd.e-technik.uni-rostock.de/optimum/iiot-platform/iiot-component.git
cd iiot-component
git submodule init
git submodule update
mkdir build
cd build
cmake ..
make -j
```

**Build Options:**
Environment variables are defined in the Dockerfile to activate security and set OPC UA port and MQTT broker URL and port, e.g.:
- ENV OPC_UA_SECURITY=OFF
- ENV OPC_UA_OPENSSL=OFF
- ENV NXP_SE050=OFF (not implemented yet)
- ENV MQTTBrokerAddress 127.0.0.1:1883
- ENV opcPort 4840

**Commandline Options:**
- -o : OPC UA port (default: 4840)
- -m : MQTT Broker address with port (default: 127.0.0.1:1883)
- -s : Set System_ID variable in OPC UA Address Space (default: crane1)
- -c : Path to a certificate that will be used by the OPC UA server, mandatory in case of OPC_UA_SECURITY=ON
- -p : Path to private key related to certificate that will be used by the OPC UA server, mandatory in case of OPC_UA_SECURITY=ON

**Testbeds:**
Different scenarios are prepared in the folder 'testbeds'. 


_'OPTIMUM_Testbed_2_Devs'_ : creates 2x IIoT Platforms + 2x MQTT Brokers to reflect 2 individual OPTIMUM devices that can talk via OPC UA between each other and via MQTT internally. They are interconnected via a virtual network. The Testbed can be started by executing the docker-compose script with 
```
docker-compose up -d
```

To be on the safe side that the docker-compose script is not just using images of older versions of the project, please run the following after fetching/pulling the latest version from GitLab:
```
docker-compose up -d --build
```

The IIoT Platforms are accessible from the host computer via the localhost address and the ports 4841 (Device 1) and 4842 (Device 2). The MQTT Brokers are also accessible from the host via the ports 1884 (Device 1) and 1885 (Device 2). Inside the virtual network, the default ports 4840 for OPC UA and 1883 for MQTT are used.


_'OPTIMUM_HMI_Development'_ : creates two OPTIMUM devices with IIoT Platforms and an MQTT Brokers. Additionally a DCP mockup is created with the help of mqtt-spy (deamon) to send fake location data to the IIoT Platforms. Additionally, two instances of the leightweight IIoT-Platform (with dynamic fake location data) are used to simulate an operator and a forklift. The IIoT Platforms can be accessed from the host computer via the localhost address (alternatively the host IP address) and the port 4841 (crane1), 4842 (crane2), 4843 (operator), 4844 (forklift).

**Add OPC UA Methods:**
OPC UA Methods are not part of the OPTIMUM data model. The required methods will be defined in the source code and added to the OPC UA Address Space during execution of the binary. Two steps are required to add a method (see also open62541 documentation: https://open62541.org/doc/current/tutorial_server_method.html):

1) Create a callback function that will be executed when an OPC UA client invokes the method. Here you can process the input arguments, do the intended task and preapre the output arguments of the method.
```
CraneModeMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output)
```

2) Add the method to the server and link the callback to it.
```
UA_Server_addMethodNode(UA_Server *server, const UA_NodeId requestedNewNodeId,
                        const UA_NodeId parentNodeId, const UA_NodeId referenceTypeId,
                        const UA_QualifiedName browseName, const UA_MethodAttributes attr,
                        UA_MethodCallback method,
                        size_t inputArgumentsSize, const UA_Argument *inputArguments,
                        size_t outputArgumentsSize, const UA_Argument *outputArguments,
                        void *nodeContext, UA_NodeId *outNewNodeId)
``` 

**To Dos:**

- fix opt package build in top CMake file
- optimize and document code
- In future: merge separate request to same target device (-> use only one client and a hashtable to map subscription id to req id) -> this involves also checking if the same data was already requested, then act intelligent and try to distribute data efficently to all interested requesters
- Open62541 added new rt features to pubsub, check it out , keyword "UA_PUBSUB_RT"
- and keep track of used ids for msg exchange between iiot component and target component
- response node in address space of target device is not deleted after final message (already done?)
- Can we use the existing list function in open62541 for internal housekeeping of writergroups etc . To keep track of threads?
- describe data model integration (also basic model with data types and demonstrator-specific model)
- Return meaningful error to requesting component if reqId is currently already in use

**Done:**

- implement OPC UA machinery
- preparations for docker image
- once HARDCODED req Id for msg exchange between iiot component and target component, create id dynamically 

