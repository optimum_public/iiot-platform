
#ifndef EXAMPLES_PUBSUB_OPCUA_MQTT_OPCUA_H_
#define EXAMPLES_PUBSUB_OPCUA_MQTT_OPCUA_H_

#include "open62541/server.h"
//#include "hashtable.h"
#include "uthash/src/uthash.h"

/* typedef struct UA_App_Config_t
{
	//global data:
	UA_Server *server; // application wide info about OPC UA server
	UA_NodeId connectionIdent; // id of opened Pub Sub extension connection, may also used app wide if not further connections besides MQTT are required
	//common pub/sub data:
	int qos;
	bool useJson;
	// publisher-specific data:
	char *publishTopic;
	UA_NodeId publishedDataSetIdent;
	UA_NodeId writerGroupIdentifier;
	UA_NodeId DataSetWriterIdentifier;
	int publishInterval;
	// subscriber-specific data:
	char *subscribeTopic;
	UA_NodeId readerGroupIdentifier;
	UA_NodeId readerIdentifier;
	//request-specific data:
	char *arguments;
//	char *operation;
	char *reqId;
//	unsigned reqId_numeric;
} UA_AppConfig;

typedef struct opc_ua_client
{
	UA_Client *client;
	char *url;
	char *payload;
//	char *nodeToSubscribe;
	char *data;
	char *mode;
//	char *reqId;
//	unsigned reqId_numeric;
	char *component;
//	char *isSubscribed;
	char *subscription;
	UA_AppConfig *appConfig;
	char *command;
	char *method_id;
	UA_UInt32 sub_id;
	UA_Boolean terminateClient;
}UA_AppClient;
 */

typedef struct common_data_t
{
	//global data:
	UA_Server *server; // application wide info about OPC UA server
	UA_NodeId connectionIdent; // id of opened Pub Sub extension connection, may also used app wide if not further connections besides MQTT are required
	UA_ServerConfig *serverConfig; 
	UA_NetworkAddressUrlDataType mqttBrokerURL;
#ifdef OPC_UA_SECURITY
	UA_ByteString *serverCert;
	//char *serverCertPath;
	UA_ByteString *serverPrivateKey;
	//char *privateKeyPath;
	UA_ByteString *clientCert;
	UA_ByteString *clientPrivateKey;
#endif
} common_data;

typedef struct publisher_data_t
{
	// publisher-specific data:
	char *publishTopic;
	UA_NodeId publishedDataSetIdent;
	UA_NodeId writerGroupIdentifier;
	UA_NodeId DataSetWriterIdentifier;
	int publishInterval;
	int qos;
	bool useJson;
} publisher_data;

typedef struct subscriber_data_t
{
	// subscriber-specific data:
	char *subscribeTopic;
	//UA_Int16 subscribeTopicCount;
	//UA_String *subscribeTopics;
	//UA_String * (*subscribeTopics)[];
	UA_NodeId readerGroupIdentifier;
	UA_NodeId readerIdentifier;
	int qos;
	//bool useJson;
} subscriber_data;

typedef struct request_data_t
{
	char *reqId;
	UA_Client *client;
	char *url;
	char *payload_data;
	size_t payload_dataArraySize;
	char *data;
	char **payload_dataArray;
	//char *mode;
	char *component;
	char *subscription;
	char *command;
	char *method_id;
	UA_UInt32 sub_id;
	UA_Boolean terminateClient;
	UA_MonitoredItemCreateRequest monitoredItem; //TODO: is it really required, do we need to access it after creation again, seems not currently
//	char *arguments;
//	unsigned reqId_numeric;
	publisher_data publisher;
	UT_hash_handle hh; //required for hash table
	//OPC UA client subscription fields: 
	char **subscribedVariablesArray;
	//char **subscribedValuesArray;
	
	UA_String **subscribedValuesArray;
	size_t subscribedArraySize;
	UA_Variant *val;
#ifdef OPC_UA_SECURITY
	UA_ByteString *serverCert;
	//char *serverCertPath;
	UA_ByteString *serverPrivateKey;
	//char *privateKeyPath;
	UA_ByteString *clientCert;
	UA_ByteString *clientPrivateKey;
#endif
} request_data;

/* typedef struct iiot_data_t {
	request_data request;
	publisher_data publisher;
	subscriber_data subscriber;
	common_data common;
} iiot_data; */


typedef struct optimum_struct
{
	char *answer_topic; 	// this is the answer topic every request to the IIoT-Platform has to contain to be able to send the response
	char *req_id;			//arbitrary id to match responses with initial request
	char *device;			// URL/IP of target device that can answer the request
	char *component;
	char *command;
	char *method_id;
	char *subscription;		// enable or disable OPC UA subscription feature
	char *final_msg;
	char *status;
	char *data;
	char** dataArray;
	size_t dataArraySize;
	UA_UInt32 sub_id;
}optimum_msg;

typedef struct client_list
{
	char *req_id;
	pthread_t *client;
	bool *terminateSignal;
} clientList;

//extern CList *list; // list of opc ua client threads
//extern HashTable *table;
extern request_data *hashTable;
extern common_data common;

extern void subscriber_message_callback(UA_ByteString *encodedBuffer, UA_ByteString *topic);
extern void free_UA_AppClient(request_data *requestConfig);
extern unsigned numeric_id(char *id);
extern void pubSubNotificationTrigger(common_data *common, publisher_data *publisher);

#endif /* EXAMPLES_PUBSUB_OPCUA_MQTT_OPCUA_H_ */
