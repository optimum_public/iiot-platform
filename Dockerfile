# --------------------------------------------------------------------
# --- STAGE: Tools
# --------------------------------------------------------------------
ARG IMAGE=alpine:3.9
FROM $IMAGE AS build_tools
#install tools:
RUN apk add --no-cache cmake gcc g++ musl-dev python3 py-pip make git mbedtls mbedtls-dev openssl openssl-dev python-dev python3-dev zeromq-dev linux-headers && rm -rf /var/cache/apk/*

# --------------------------------------------------------------------
# --- STAGE: Pull project from GitLab 
# --------------------------------------------------------------------
FROM build_tools AS iiot_code
#RUN git clone git@gitlab.amd.e-technik.uni-rostock.de:optimum/iiot-platform/iiot-component.git /tmp/open62541
COPY . /tmp/iiot-component
RUN cd /tmp/iiot-component \
  && git submodule init \
  && git submodule update

# --------------------------------------------------------------------
# --- STAGE: Build 
# --------------------------------------------------------------------
FROM iiot_code AS iiot_build
ENV OPEN62541_DEBUG=OFF
ENV OPEN62541_TRACE=OFF
ENV OPC_UA_SECURITY=ON
ENV OPC_UA_OPENSSL=OFF
ENV NXP_SE050=OFF
ARG NXP_BUILD=OFF
ENV NXP_BUILD=$NXP_BUILD
RUN if [ $NXP_BUILD == "OFF" ]; then \
  mkdir -p /tmp/iiot-component/build \
  && cd /tmp/iiot-component/build \
  && cmake .. \
  && make -j; \
else \
  mkdir -p /tmp/iiot-component/build \
  && cd /tmp/iiot-component/build \
  && cmake .. \
  && make -j 4; \
fi 

#RUN cd /tmp/iiot-component/deps/open62541/tools/certs \
#  && openssl rand \
#  && pip3 install netifaces \
#  && python3 ./create_self-signed.py /tmp/iiot-component/build/bin


# --------------------------------------------------------------------
# --- STAGE: Release
# --------------------------------------------------------------------
#FROM alpine:3.9
ENV MQTTBrokerAddress 127.0.0.1:1883
ENV opcPort 4840
ENV SYSTEM_ID crane1
ARG VALGRIND=OFF
ENV VALGRIND=$VALGRIND
RUN if [ $VALGRIND == "ON" ]; then apk add --no-cache valgrind; fi
#COPY --from=iiot_build /tmp/iiot-component/build/bin/iiot_platform .
#RUN apk --no-cache add libgcc mbedtls mbedtls-utils openssl
#RUN gen_key type=rsa rsa_keysize=4096 filename=optimumServerPrivateKey.der format=der \
#  && cert_write selfsign=1 issuer_key=optimumServerPrivateKey.der issuer_name=CN=urn:open62541.server.application,O=OPTIMUM,C=GER output_file=optimumServerCert.der
#CMD ["./iiot_platform"]
#CMD ./iiot_platform -m ${MQTTBrokerAddress} -o ${opcPort}
#CMD ./iiot_platform -m ${MQTTBrokerAddress} -o ${opcPort} -c optimumServerCert.der -p optimumServerPrivateKey.der
CMD ["/bin/sh", "/tmp/iiot-component/start_iiot_component.sh"]
