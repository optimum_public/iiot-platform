#include "open62541/server.h"
#include "iiot_platform.h"

void ReadOperationDataSetField(UA_Server *server, UA_NodeId publishedDataSetIdent);

void SubscriptionOperationDataSetField(UA_Server *server, UA_NodeId publishedDataSetIdent);

UA_StatusCode WriterGroup(common_data *common, publisher_data *publisher);

UA_StatusCode DataSetWriter(common_data *common, publisher_data *publisher);
 
UA_StatusCode PublishedDataSet(UA_Server *server, UA_NodeId *publishedDataSetIdent);

UA_StatusCode setup_json(common_data *commonPtr, request_data *requestConfig);

UA_StatusCode setup_publish_message(common_data *commonPtr, request_data *requestConfig);