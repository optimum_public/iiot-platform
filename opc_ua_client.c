#include "open62541/server.h"
#include <open62541/client_config_default.h>
#include <open62541/client_highlevel.h>
#include <open62541/client_subscriptions.h>
#include "optimum_pubsub_client.h"
#include "open62541/plugin/log_stdout.h"
#include "optimum_mqtt_publish.h"
#include "iiot_platform.h"
#include "opc_ua_client.h"
#include "ua_pubsub.h"
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <json-c/json.h>

#define SUBSCRIPTION_INTERVAL 50.0

//UA_Client *client;
//UA_AppClient *opcClientStruct;
//int cond = _TRUE;
//char *nodeIdentifier;
//struct json_object *parsed_json_data,*status, *response_node, *device_status, *data_object;
//bool boolean[2] = {true, false}; 
//UA_UInt32 *subscribedVariablesArray;
//char **subscribedVariablesArray;
//char **subscribedValuesArray;
//UA_String **subscribedValuesArray;
//size_t subscribedArraySize;
//UA_Boolean dataInitialized;
/*
 * callback functions of OPC Client can't handover required information to generate MQTT msg
 * We use "global" variables that are thread specific - thus every OPC UA client threads holds its own data structure
 */
static pthread_mutex_t	keylock;  	/* static ensures only one copy of keylock */
static pthread_key_t	key;
static int	once_per_keyname = 0;
void *	tsd = NULL;

// TODO: use local instead of global variable client
//void clean_thread(UA_Client*client){
void clean_thread(request_data *requestConfig){
	UA_Client_disconnect(requestConfig->client);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Disconnected OPC UA client related to request id '%s' now", requestConfig->reqId);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Terminating OPC UA client thread related to request id '%s' now", requestConfig->reqId);
	free_UA_AppClient(requestConfig);
    pthread_exit(NULL);
}

void exit_client(request_data* requestConfig){
	UA_Server_deleteNode(common.server, UA_NODEID_STRING(1, requestConfig->reqId), FALSE);
	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Deleted response node (connection to request component), related to request id '%s'", requestConfig->reqId);
	HASH_DEL(hashTable, requestConfig);
	clean_thread(requestConfig);
}

/* 
 * Link OPC UA client data structure to thread specific key, to access it also in callback functions
 * Derived from: https://www.cs.rit.edu/~hpb/Man/_Man_Solaris_2.6_html/html3t/pthread_key_create.3t.html
 */
//void *set_thread_specific_data_once(UA_AppClient* dataStructure)
void *set_thread_specific_data_once(request_data *dataStructure)
{
	if (!once_per_keyname) {     /* see pthread_once(3T) */
		pthread_mutex_lock(&keylock);
		if (!once_per_keyname++)     /* retest with lock */
			pthread_key_create(&key, NULL); //TODO: add freeing function for data structure or do it manually at the end of the thread
		pthread_mutex_unlock(&keylock);
	}
	tsd = pthread_getspecific(key);
	if (tsd == NULL) {
		//tsd = (void *)malloc(strlen(UA_AppClient));
		pthread_setspecific(key, dataStructure);
		//printf("tsd for %d = %s\n",thr_self(),(char *)pthread_getspecific(key));
		//sleep(2);
		//printf("tsd for %d remains %s\n",thr_self(),(char *)pthread_getspecific(key));
	}
	return 0;
}  /* end thread_specific_data */

json_object * formatReadOperationMessage(UA_String readData, char *reqId){
	
	//{"req_id":"X68461","data":{"status":"success","data":{"device_status":"idle"}}}
	json_object *jobj_data = json_object_new_object();
	json_object *jobj_data1 = json_object_new_object();

	json_object *jstr1 = json_object_new_string(reqId);	
	json_object *jstr2 = json_object_new_string("success");					
	json_object *jstr3 = json_object_new_string((char*)readData.data);				
					
	json_object_object_add(jobj_data1,"req_id", jstr1);	
	json_object_object_add(jobj_data1,"status", jstr2 );
	json_object_object_add(jobj_data,"device_status", jstr3 );
	json_object_object_add(jobj_data1,"data", jobj_data);

	return jobj_data1;	
}

json_object * formatMonitoredMessage(UA_String readData, char* reqId){
	// TODO: create uniform msg format: req_id; status; final_msg; data
	//{"req_id":"X68461", "data":{"final_msg": "true", "data": { "status": "SUCCESS", "data": "Skynet_0.1 running" } } }	
	json_object *jobj_data = json_object_new_object();
	struct json_object *temp;
	
	temp = json_object_new_string(reqId);
	json_object_object_add(jobj_data,"req_id", temp);

	
	json_object *jstr2 = json_tokener_parse((char*)readData.data); //{"final_msg": "true", "data": { "status": "SUCCESS", "data": "Skynet_0.1 running" } }
	if(json_object_object_get_ex(jstr2, "status", &temp)){
		json_object_object_add(jobj_data,"status", temp);
	} 

	if(json_object_object_get_ex(jstr2, "final_msg", &temp)){
		json_object_object_add(jobj_data,"final_msg", temp);
	} 

	if(json_object_object_get_ex(jstr2, "data", &temp)){
		json_object_object_add(jobj_data,"data", temp);
	} 
	
//	json_object_object_add(jobj_data,"req_id", jstr1);		
//	json_object_object_add(jobj_data,"data", jstr2);
	return jobj_data;	
}

// bool decodeResponseMessage(UA_String responseData){
// 	char  buffer[strlen((char*)responseData.data)];
// 	strncpy(buffer, (char*)responseData.data, strlen((char*)responseData.data));
	
// 	//parsed_json_data = json_tokener_parse(buffer);	
// 	struct json_object *parsed_json_data= json_tokener_parse(buffer);	
// 	struct json_object *status;
// 	struct json_object *response_node;
	
// 	if(json_object_object_get_ex(parsed_json_data, "status", &status) 
// 		&& json_object_object_get_ex(parsed_json_data, "response_node", &response_node)){ 
		
// 		//return boolean[0];
// 		return UA_TRUE;
// 	}
// 	else{
// 		//return boolean[1];
// 		return UA_FALSE;
// 	}	
// }

//UA_StatusCode clientConnectionInitialization(void *arg){
//UA_StatusCode clientConnectionInitialization(UA_AppClient *arg){
UA_StatusCode clientConnectionInitialization(request_data *requestConfig){

    //opcClientStruct = (UA_AppClient *) malloc(sizeof(UA_AppClient));
    //opcClientStruct = (UA_AppClient*)arg; 
	// TODO why copying the struct here?
	
	UA_StatusCode retval;
	//client = UA_Client_new();
	requestConfig->client = UA_Client_new();
	UA_ClientConfig *clientConfig = UA_Client_getConfig(requestConfig->client);

#ifdef OPC_UA_SECURITY

    /* Load the trustlist */
    size_t trustListSize = 0;
   // if(argc > 3)
   //     trustListSize = (size_t)argc-3;
    UA_STACKARRAY(UA_ByteString, trustList, trustListSize);
    //for(size_t i = 0; i < trustListSize; i++)
    //    trustList[i] = loadFile(argv[i+3]);

    /* Loading of a revocation list currently unsupported */
    UA_ByteString *revocationList = NULL;
    size_t revocationListSize = 0;

	printf("private key file length: %ld\n",requestConfig->clientPrivateKey->length);
	//printf("private key file path: %s\n", requestConfig->clientPrivateKeyPath);

	UA_ClientConfig_setDefaultEncryption(clientConfig,
                                     	*requestConfig->clientCert, *requestConfig->clientPrivateKey,
                                        trustList, trustListSize,
                                        revocationList, revocationListSize);
	clientConfig->securityMode = UA_MESSAGESECURITYMODE_SIGNANDENCRYPT;
#else
	UA_ClientConfig_setDefault(clientConfig);
#endif
	
	//retval = UA_Client_connect_username(client, "opc.tcp://localhost:4801", "user1", "password");
	//retval = UA_Client_connect(client, opcClientStruct->url); //Connect to Optimum Device 2 
	retval = UA_Client_connect(requestConfig->client, requestConfig->url); //Connect to Optimum Device 2 
	// if(retval != UA_STATUSCODE_GOOD) {
	// 	UA_Client_delete(requestConfig->client);
	// 	pthread_exit(NULL);
	// 	return EXIT_SUCCESS;	
	// }
 	if(retval == UA_STATUSCODE_GOOD)	
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "connected to IIoT platform of device %s", requestConfig->url);		
	
	return retval;
}

UA_StatusCode
writeValueToRequestIdSpecificNode(UA_Server *server, UA_String writeValue, char* reqId) {
    UA_NodeId responseNodeId = UA_NODEID_STRING(1, reqId);

    UA_Variant variant;
    UA_Variant_init(&variant);
    UA_Variant_setScalar(&variant, &writeValue, &UA_TYPES[UA_TYPES_STRING]);
    return UA_Server_writeValue(server, responseNodeId, variant);	
}

/* 
 * This function writes the response value received via OPC UA into the PubSub response node in the address space
 * and then triggers the publishing of an MQTT message to the MQTT broker that contains the value of the response node.
 * The response node is therefore a workaround and just a temporary value storage. 
 *
 */
//UA_StatusCode replyToReadRequest(UA_String value, const char *topicToPublish, char *reqId)
UA_StatusCode replyToReadRequest(char * identifier, UA_String value, common_data *commonPtr, request_data *requestConfig)
{
	UA_StatusCode retval = UA_STATUSCODE_GOOD;
	//strcpy(opcClientStruct->appConfig->publishTopic, topicToPublish);		
	UA_String stringValue;
	//json_object *jobj_data;
	
	//jobj_data = formatReadOperationMessage(value, requestConfig->reqId); // prepare a json message

	json_object *jobj_data = json_object_new_object();
	json_object *jobj_data1 = json_object_new_object();

	json_object *jstr1 = json_object_new_string(requestConfig->reqId);	
	json_object *jstr2 = json_object_new_string("success");					
	json_object *jstr3 = json_object_new_string((char*)value.data);				
					
	json_object_object_add(jobj_data1,"req_id", jstr1);	
	json_object_object_add(jobj_data1,"status", jstr2 );
	json_object_object_add(jobj_data, identifier, jstr3 );
	json_object_object_add(jobj_data1,"data", jobj_data);

	//stringValue = UA_STRING(strdup(json_object_get_string(jobj_data)));
	//Todo: WHY not jobj_data1??
	stringValue = UA_STRING(strdup(json_object_get_string(jobj_data1)));
	
	retval = writeValueToRequestIdSpecificNode(commonPtr->server, stringValue, requestConfig->reqId);    

	if (UA_STATUSCODE_GOOD != retval){
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
		        "prepareReadValue: UA_Server_writeValue failed\n");
	}
	else{
		//requestConfig->appConfig->operation = strdup("read");
		//retval = setup_json(commonPtr, requestConfig);
		//sleep(1);	
		//UA_sleep_ms(1000);
		pubSubNotificationTrigger(commonPtr, &requestConfig->publisher);
		// disable writer group
		//UA_Server_setWriterGroupDisabled(requestConfig->appConfig->server, requestConfig->appConfig->writerGroupIdentifier);
	}	
	json_object_put(jobj_data1);
	UA_String_clear(&stringValue);
	return retval;
}

/* 
 * This function writes the response value array received via OPC UA into the PubSub response node in the address space
 * and then triggers the publishing of an MQTT message to the MQTT broker that contains the value content (string converted array of received values) of the response node.
 * The response node is therefore a workaround and just a temporary value storage. 
 *
 */
//UA_StatusCode replyToReadRequest(UA_String value, const char *topicToPublish, char *reqId)
UA_StatusCode replyArrayToReadRequest(char **variableArray, UA_String **valueArray, size_t arraySize, common_data *commonPtr, request_data *requestConfig)
{
	UA_StatusCode retval = UA_STATUSCODE_GOOD;
	//strcpy(opcClientStruct->appConfig->publishTopic, topicToPublish);		
	UA_String stringValue;
	//json_object *jobj_data;
	
	//jobj_data = formatReadOperationMessage(value, requestConfig->reqId); // prepare a json message

	json_object *innerJson = json_object_new_object();
	json_object *outerJson = json_object_new_object();

	// reqID
	json_object *jstr1 = json_object_new_string(requestConfig->reqId);	
	json_object_object_add(outerJson,"req_id", jstr1);	

	// request status
	json_object *jstr2 = json_object_new_string("success");	
	json_object_object_add(outerJson,"status", jstr2 );		

	// reponse payload data
	json_object *tempJson;
	int i;
	for (i = 0; i < arraySize; i++){

		tempJson = json_object_new_string((char*) valueArray[i]->data);				
		json_object_object_add(innerJson, variableArray[i], tempJson );
	}

	json_object_object_add(outerJson,"data", innerJson);
	stringValue = UA_STRING(strdup(json_object_get_string(outerJson)));
	retval = writeValueToRequestIdSpecificNode(commonPtr->server, stringValue, requestConfig->reqId);    
	//json_object_put(innerJson);
	json_object_put(outerJson);
	UA_String_clear(&stringValue);
	if (UA_STATUSCODE_GOOD != retval){
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
		        "prepareReadValue: UA_Server_writeValue failed\n");
	}
	else{
		//requestConfig->appConfig->operation = strdup("read");
		//retval = setup_json(commonPtr, requestConfig);
		//sleep(1);	
		//UA_sleep_ms(1000);
		pubSubNotificationTrigger(commonPtr, &requestConfig->publisher);
		// disable writer group
		//UA_Server_setWriterGroupDisabled(requestConfig->appConfig->server, requestConfig->appConfig->writerGroupIdentifier);
	}	
	return retval;
}

/*
 *@Brief: read notification of OPC UA server and forward information to requesting component via MQTT
 */
static void
callback_ReadResponseData(UA_Client *client1, UA_UInt32 subId, void *subContext,
                     UA_UInt32 monId, void *monContext, UA_DataValue *value)
{

	char dummy[20];
	UA_String receivedData;
	if (value->value.type == NULL){
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "callback_ReadResponseData: received value without type definition, subId: %d, monId: %d", subId, monId);
		return;
	}

	switch (value->value.type->typeIndex)
	{
		case UA_TYPES_INT16:
		case UA_TYPES_INT32:
		case UA_TYPES_INT64:
		case UA_TYPES_UINT16:
		case UA_TYPES_UINT32:
		case UA_TYPES_UINT64:
		//if (UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT32]) || UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT16])) {
		;
		//char dummy[20];
		//int intDummy = ;
		//sprintf(dummy, "%d", intDummy);
		sprintf(dummy, "%d", *(int *)value->value.data);
		///sprintf(dummy, "%d", *(int*)response.results[i].value.data);
		//UA_String UAdummy = UA_STRING(strdup(dummy));
		//UAdummy[i] = UA_STRING(strdup(dummy));
		//resultArray[i] = &UAdummy;
		//resultArray[i] = &UAdummy[i];
		//resultArray[i] = dummy;
		receivedData = UA_STRING(dummy);
		break;

	case UA_TYPES_DOUBLE:;
		//char dummy2[20];
		//double doubleDummy = *(double*)response.results[i].value.data;
		sprintf(dummy, "%f", *(double *)value->value.data);
		//UA_String UAdummy2 = UA_STRING(dummy);
		//UAdummy[i] = UA_STRING(strdup(dummy));
		//resultArray[i] = &UAdummy[i];
		receivedData = UA_STRING(dummy);
		break;

	case UA_TYPES_STRING:
		receivedData = *(UA_String *)value->value.data;
		break;
	default:
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "callback_ReadResponseDataArray: Data Type with NodeId %u not covered in switch statement", value->value.type->typeIndex);
	}


	//if (value->value.type == &UA_TYPES[UA_TYPES_STRING])
    //{
		//UA_String receivedData = *(UA_String *)value->value.data;
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "callback_ReadResponseDataArray: received the following value: %s", receivedData.data);
		UA_StatusCode retval;
		//json_object *jobj_data;
		request_data *requestConfig = (request_data *) pthread_getspecific(key);
		if (requestConfig == NULL){
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "OPC UA server read response callback: failed to access OPC UA client data structure.");
			return;
		}
		//  DONE: find a way to retrieve reqId 
		//char subscribedIdentifier[20];
		//sprintf(subscribedIdentifier, "%d", subscribedVariablesArray[monId]);
		json_object *jobj_data = json_object_new_object();
		json_object *jobj_data1 = json_object_new_object();

		json_object *jstr1 = json_object_new_string(requestConfig->reqId);
		json_object *jstr2 = json_object_new_string("success");
		json_object *jstr3 = json_object_new_string((char *)receivedData.data);

		json_object_object_add(jobj_data1, "req_id", jstr1);
		json_object_object_add(jobj_data1, "status", jstr2);
		//json_object_object_add(jobj_data, subscribedIdentifier, jstr3);
		json_object_object_add(jobj_data, requestConfig->subscribedVariablesArray[monId-1], jstr3);
		json_object_object_add(jobj_data1, "data", jobj_data);

		//jobj_data = formatReadOperationMessage(receivedData, requestConfig->reqId); // prepare a json message

		UA_String processedData = UA_STRING(strdup(json_object_get_string(jobj_data1)));			
		json_object_put(jobj_data1);
		retval = writeValueToRequestIdSpecificNode(common.server, processedData, requestConfig->reqId);
		if(retval != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Write to Request Id Node %s: Error Name = %s",
			requestConfig->reqId, UA_StatusCode_name(retval));
			return;
		}	
		//opcClientStruct->appConfig->operation = strdup("read");
/* 		retval = setup_json(opcClientStruct->appConfig);
		if(retval != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Publish Message SetUp: Error Name = %s",
			UA_StatusCode_name(retval));
			return; 
		} */		
		//UA_Server_setWriterGroupOperational(opcClientStruct->appConfig->server, opcClientStruct->appConfig->writerGroupIdentifier);
		//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "trigger now MQTT msg publication for req id:%s", requestConfig->reqId);
		pubSubNotificationTrigger(&common, &requestConfig->publisher);
		//sleep(1);
		//UA_Server_setWriterGroupDisabled(opcClientStruct->appConfig->server, opcClientStruct->appConfig->writerGroupIdentifier);
		//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "WriterGroupId = %u",opcClientStruct->appConfig->writerGroupIdentifier.identifier.numeric);
		
	//}
	UA_String_clear(&processedData);
}

static void
callback_ReadResponseDataArray(UA_Client *client1, UA_UInt32 subId, void *subContext,
                     UA_UInt32 monId, void *monContext, UA_DataValue *value)
{
	if (!value->hasValue)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "callback_ReadResponseDataArray: one subscription update did not contain a value");
		return; //void workaround otherwise EXIT_FAILURE would be better suited here
							 //TODO: inform requesting component with error mqtt msg
	}
	request_data *requestConfig = (request_data *)pthread_getspecific(key);
	//char dummy[20];
	//UA_String receivedDataTemp;
	UA_String *receivedData;
	if (requestConfig->subscribedValuesArray[monId-1] != NULL){
		receivedData = requestConfig->subscribedValuesArray[monId-1];
	} else{
		receivedData = malloc(sizeof(UA_String));
	}
	//UA_String tempUAString[subscribedArraySize]; 
	if (value->value.type == NULL){
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "callback_ReadResponseDataArray: received value without type definition, subId: %d, monId: %d", subId, monId);
		return;
	}
	switch (value->value.type->typeIndex)
	{
		case UA_TYPES_INT16:
		case UA_TYPES_INT32:
		case UA_TYPES_INT64:
		case UA_TYPES_UINT16:
		case UA_TYPES_UINT32:
		case UA_TYPES_UINT64:
		//if (UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT32]) || UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT16])) {
		;
		//char dummy[20];
		//int intDummy = ;
		//sprintf(dummy, "%d", intDummy);
		char *temp;
		int len = snprintf( NULL, 0, "%6d", *(int *)value->value.data);
		if (requestConfig->subscribedValuesArray[monId-1] != NULL){
			temp = (char*) receivedData->data;
			snprintf(temp, len + 1, "%d", *(int *)value->value.data);
			*receivedData= UA_STRING(temp);
		}else{
			temp = malloc(len+1);
			snprintf(temp, len + 1, "%d", *(int *)value->value.data);
			*receivedData= UA_STRING(temp);
		}

		//len = snprintf( NULL, 0, "%6d", *(int *)value->value.data);
		//printf("len %d data: %d\n", len, *(int *)value->value.data);
		//sprintf(dummy, "%d", *(int *)value->value.data);
		///sprintf(dummy, "%d", *(int*)response.results[i].value.data);
		//UA_String UAdummy = UA_STRING(strdup(dummy));
		//UAdummy[i] = UA_STRING(strdup(dummy));
		//resultArray[i] = &UAdummy;
		//resultArray[i] = &UAdummy[i];
		//resultArray[i] = dummy;
		//receivedData = UA_STRING(temp);
		//receivedDataTemp = UA_STRING_ALLOC(temp);
		//receivedDataTemp = UA_STRING(temp);
		//*receivedData= UA_STRING(temp);
		break;

	case UA_TYPES_DOUBLE:;
		//char dummy2[20];
		//double doubleDummy = *(double*)response.results[i].value.data;
		int lenDouble = snprintf( NULL, 0, "%g", *(double *)value->value.data);
		char *tempDouble = malloc(lenDouble+1);
		snprintf(tempDouble, lenDouble + 1, "%g", *(double *)value->value.data);
		//sprintf(dummy, "%f", *(double *)value->value.data);
		//UA_String UAdummy2 = UA_STRING(dummy);
		//UAdummy[i] = UA_STRING(strdup(dummy));
		//resultArray[i] = &UAdummy[i];
		//receivedData = UA_STRING(tempDouble);
		//receivedDataTemp = UA_STRING_ALLOC(tempDouble);
		*receivedData = UA_STRING_ALLOC(tempDouble);
		break;

	case UA_TYPES_STRING:
	// TODO:ok?  or char cast UA_STRING()?
		//receivedDataTemp = *(UA_String *)value->value.data;
		*receivedData = *(UA_String *)value->value.data;
		break;
	default:
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "callback_ReadResponseDataArray: Data Type with NodeId %u not covered in switch statement", value->value.type->typeIndex);
	}

	
//TODO: memory leak, on every value update a new string is allocated and the old dereferenced; done!; delete function also frees string in UA_String structure?
// TODO: is monitoring ID always starting with 1 and therefore be smaller than arraysize? If not memory access error
//	if (subscribedValuesArray[monId-1] != NULL){
//		UA_String_delete(subscribedValuesArray[monId-1]);
//	}
	//UA_String 
	//tempUAString[monId-1] = UA_STRING(strdup(receivedData.data));
	//UA_STRING_ALLOC(receivedData.data);
	//
	//UA_String_copy(&receivedDataTemp, receivedData);
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "callback_ReadResponseDataArray: received the following value: %s", receivedData->data);
	//UA_String tempUAString = UA_STRING_ALLOC(receivedData.data);
	//subscribedValuesArray[monId-1] = &tempUAString[monId-1];
	requestConfig->subscribedValuesArray[monId-1] = receivedData;

	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "callback_ReadResponseDataArray: received the following value: %s", subscribedValuesArray[monId-1]->data);
	//if (!receivedData.data){
	//	UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "callback_ReadResponseDataArray: receivedData.data error (uninitialized?)");
	//	return;
	//}
//	subscribedValuesArray[monId-1]->data = (UA_Byte *) strdup(receivedData.data);
//	subscribedValuesArray[monId-1]->length = strlen(receivedData.data);

//}else{
//	UA_String tempUAString = UA_STRING(strdup(receivedData.data));
//	//UA_String tempUAString = UA_STRING_ALLOC(receivedData.data);
//	subscribedValuesArray[monId-1] = &tempUAString;
//	//strcpy(subscribedValuesArray[monId]->data, receivedData.data);
//}

int i;
UA_Boolean readyToSend = UA_TRUE;
for (i=0; i<requestConfig->subscribedArraySize; i++){
	if (requestConfig->subscribedValuesArray[i] == NULL){
		readyToSend = UA_FALSE;
		break;
	}
}

if (readyToSend){
	//request_data *requestConfig = (request_data *)pthread_getspecific(key);
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "trigger now MQTT msg publication for req id:%s", requestConfig->reqId);
	//UA_StatusCode retval = writeValueToRequestIdSpecificNode(common.server, processedData, requestConfig->reqId);
	UA_StatusCode retval = replyArrayToReadRequest(requestConfig->subscribedVariablesArray, requestConfig->subscribedValuesArray, requestConfig->subscribedArraySize, &common, requestConfig);
	if (retval != UA_STATUSCODE_GOOD)
	{
		UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Write to Request Id Node %s: Error Name = %s",
					 requestConfig->reqId, UA_StatusCode_name(retval));
		return;
	}
	
	//pubSubNotificationTrigger(&common, &requestConfig->publisher);
}
	// UA_StatusCode retval;
	// json_object *jobj_data;
	// request_data *requestConfig = (request_data *)pthread_getspecific(key);
	// if (requestConfig == NULL)
	// {
	// 	UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "OPC UA server read response callback: failed to access OPC UA client data structure.");
	// 	return;
	// }
	// //  DONE: find a way to retrieve reqId
	// jobj_data = formatReadOperationMessage(receivedData, requestConfig->reqId); // prepare a json message
	// receivedData = UA_STRING(strdup(json_object_get_string(jobj_data)));

	// retval = writeValueToRequestIdSpecificNode(common.server, receivedData, requestConfig->reqId);
	// if (retval != UA_STATUSCODE_GOOD)
	// {
	// 	UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Write to Request Id Node %s: Error Name = %s",
	// 				 requestConfig->reqId, UA_StatusCode_name(retval));
	// 	return;
	// }


	//opcClientStruct->appConfig->operation = strdup("read");
	/* 		retval = setup_json(opcClientStruct->appConfig);
		if(retval != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Publish Message SetUp: Error Name = %s",
			UA_StatusCode_name(retval));
			return; 
		} */
	//UA_Server_setWriterGroupOperational(opcClientStruct->appConfig->server, opcClientStruct->appConfig->writerGroupIdentifier);
	//(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "trigger now MQTT msg publication for req id:%s", requestConfig->reqId);
	//pubSubNotificationTrigger(&common, &requestConfig->publisher);
	//sleep(1);
	//UA_Server_setWriterGroupDisabled(opcClientStruct->appConfig->server, opcClientStruct->appConfig->writerGroupIdentifier);
	//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "WriterGroupId = %u",opcClientStruct->appConfig->writerGroupIdentifier.identifier.numeric);
}

static void
callback_NewResponseData(UA_Client *client1, UA_UInt32 subId, void *subContext,
                     UA_UInt32 monId, void *monContext, UA_DataValue *value)
{
    if (value->value.type == &UA_TYPES[UA_TYPES_STRING])
    {
        UA_String receivedData = *(UA_String *)value->value.data;
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "updatedData = %s", receivedData.data);
		UA_StatusCode retval;
		json_object *jobj_data;

		request_data *requestConfig = (request_data *) pthread_getspecific(key);
		
		jobj_data = formatMonitoredMessage(receivedData, requestConfig->reqId); // prepare a json message
		receivedData = UA_STRING(strdup(json_object_get_string(jobj_data)));	
		//UA_Server_setWriterGroupOperational(common.server, requestConfig->publisher.writerGroupIdentifier);
		retval = writeValueToRequestIdSpecificNode(common.server, receivedData, requestConfig->reqId);
		if(retval != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Write to Request Id Node %s: Error Name = %s",
			requestConfig->reqId, UA_StatusCode_name(retval));
			return;
		}
		//DONE: should also be done only once in case of subscription!
		/* retval = setup_json(opcClientStruct->appConfig);
		if(retval != UA_STATUSCODE_GOOD) {
			UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Publish Message SetUp: Error Name = %s",
			UA_StatusCode_name(retval));
			return; 
		} */
		//UA_Server_setWriterGroupOperational(opcClientStruct->appConfig->server, opcClientStruct->appConfig->writerGroupIdentifier);
		//DONE: modify open62541 stack in such a way that the writergroup disables itself after sending one message, solution: eventbased trigger!
		//sleep(1); 
		pubSubNotificationTrigger(&common, &requestConfig->publisher);
		// disable writer group
		//UA_Server_setWriterGroupDisabled(opcClientStruct->appConfig->server, opcClientStruct->appConfig->writerGroupIdentifier);
		
		/** uncomment this else statement to cancel Subcription to the node "Y22691" when "final_msg" = "true" **/
		
		/*else{	
			parsed_json_data = json_tokener_parse((char*)(*(UA_String *)value->value.data).data);
			if(json_object_object_get_ex(parsed_json_data, "final_msg", &data_object)){
				const char  *final_message = json_object_get_string(data_object);
				if (0 == strcmp(final_message, "true")){
					retval = UA_Client_Subscriptions_deleteSingle(client, subId); // delete subscription to the node "Y22691"

					if(retval != UA_STATUSCODE_GOOD) {
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Subscription Deletion: Error Name = %s",
						UA_StatusCode_name(retval));
					}
					else{
						UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Subscription to node %s deleted\n.", (char *)nodeIdentifier);
						cond = _FALSE;
					}
				}
			}
		}*/

		//check for final_msg == true and tear down connection
		struct json_object *temp;	
		if(json_object_object_get_ex(jobj_data, "final_msg", &temp) && strcmp(json_object_get_string(temp), "true") == 0){
			//remove node from address space
			//TODO: check if we need to waste some time to allow to send the notification of changed variable to subscriber before removing the node
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "received final message and tearing down OPC UA client thread for req_id %s", requestConfig->reqId);
			requestConfig->terminateClient = UA_TRUE;

		}

	}
    else
    {
        printf("could not decode received value!\n");
    }	
}



// @Brief: This function reads OPC UA server variables and optionally subscribes to them, does mehtod calls etc. Basically, the whole OPTIMUM iiot client functionality is realized in this function.
void *opc_ua_client(void *arg){
	UA_StatusCode retval;
	//char delimiter[] = ";";
	int cond = _TRUE;
	request_data *requestConfig = (request_data *) arg;
	// TODO: tidy up usage of global variable requestConfig
	//char *request_id = requestConfig->reqId;
	//requestConfig->appConfig->reqId = request_id;
	//requestConfig->reqId = requestConfig->reqId;
	//requestConfig->appConfig->reqId_numeric = numeric_id(requestConfig->appConfig->reqId);
	char *identifier = requestConfig->payload_data;
	UA_Boolean activeSubscription = UA_FALSE;

	// --- use global (thread-wide) pointer to access OPC UA client data structure in callback functions
	set_thread_specific_data_once(requestConfig);
	
	UA_String read_value = UA_STRING("null");
	UA_Variant *val = UA_Variant_new();
	requestConfig->val = val;
	 
	//retval  = clientConnectionInitialization(arg);
	retval = clientConnectionInitialization(requestConfig);
	if( retval != UA_STATUSCODE_GOOD) {	
		 //clean_thread(requestConfig);
		 UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Client connection could not be estabilshed, reason: %s",	UA_StatusCode_name(retval));
		// UA_sleep_ms(1000); // allow main thread to create hash entry before client structure is thrown away, otherwise hashtable search may causes an error
		 exit_client(requestConfig);
		 //cond = _FALSE;
	}

	//if (0 == strcmp(requestConfig->mode, "read"))
	// identify the target component
	if (strcmp(requestConfig->component, "iiot") == 0)
	{
		// shall we read or write?
		if (strcmp(requestConfig->command, "read") == 0){
			// requesting component needs to read a variable value from OPC UA server/iiot platform of other device
			//   One-time read or subscription?: check if subscription is activated

			// if command is read, subscription needs to be set
			if (requestConfig->subscription == NULL)	{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Payload of received MQTT msg is wrong, read command, but subscription field is empty");
				exit_client(requestConfig);
			}

			UA_UInt16 ns =999;
			UA_String currentNS = UA_STRING("http://optimum-itea3.eu/MHCranes/");
			UA_StatusCode ns_ret = UA_Client_NamespaceGetIndex(requestConfig->client, &currentNS, &ns);
			if (ns_ret != UA_STATUSCODE_GOOD){
				// workaround for wrong namespace titel in old IIoT light software
				currentNS = UA_STRING("http://demagcranes.de/MHCranes/");
				ns_ret = UA_Client_NamespaceGetIndex(requestConfig->client, &currentNS, &ns);
			}
			if (ns_ret != UA_STATUSCODE_GOOD){
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,"requested Namespace not found on remote OPC UA server. Cancelling remote connection.");
				exit_client(requestConfig);
			} 
			if (strcmp(requestConfig->subscription, "true") == 0)
				{
					/* Create a subscription */
					// ! TODO: But before subscription value is alwas read once with a single read command?
					UA_CreateSubscriptionRequest request = UA_CreateSubscriptionRequest_default();

					request.requestedLifetimeCount = 1000;
					request.requestedMaxKeepAliveCount = 1000;

					// ask for a publishing interval of 20 ms 
					// TODO: check if possbile, may revert back to 100 ms
					request.requestedPublishingInterval = SUBSCRIPTION_INTERVAL;
					// TODO: do we need to specifiy a delete callback to remove the subscription or to delete additional things like the node in our case? 
					//! Latter ones MUST be added
					UA_CreateSubscriptionResponse response = UA_Client_Subscriptions_create(requestConfig->client, request, NULL, NULL, NULL);

					//UA_UInt32 subId = response.subscriptionId;
					requestConfig->sub_id = response.subscriptionId;
					if (response.responseHeader.serviceResult != UA_STATUSCODE_GOOD){
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,"Subscription creation failed");
						exit_client(requestConfig);
					}
						//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,"Create subscription on node %s,  succeeded, id %u\n", identifier, requestConfig->sub_id);

					// --- Test to setup Pub/Sub Extension already before the reception of the answer but with disabled writergroup, formerly executed in 'callback_ReadResponseData'
					retval = setup_json(&common, requestConfig);
					if(retval != UA_STATUSCODE_GOOD) {
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Publish Message SetUp: Error Name = %s",
						UA_StatusCode_name(retval));
						exit_client(requestConfig);
					}
					// --- test end

					//check if more than one variable is requested
					if (requestConfig->payload_dataArraySize > 0){

						UA_MonitoredItemCreateRequest items[3];
						//UA_UInt32 newMonitoredItemIds[3];
						UA_Client_DataChangeNotificationCallback callbacks[3];
						UA_Client_DeleteMonitoredItemCallback deleteCallbacks[3];
						void *contexts[3];
						int i;
						for (i = 0; i < requestConfig->payload_dataArraySize; i++){

							    /* monitor the server state */
							items[i] = UA_MonitoredItemCreateRequest_default(UA_NODEID_NUMERIC(ns, atoi(requestConfig->payload_dataArray[i])));
							callbacks[i] = callback_ReadResponseDataArray;
							contexts[i] = NULL;
							deleteCallbacks[i] = NULL;
						}
						
						UA_CreateMonitoredItemsRequest createRequest;
						UA_CreateMonitoredItemsRequest_init(&createRequest);
						createRequest.subscriptionId = response.subscriptionId;
						createRequest.timestampsToReturn = UA_TIMESTAMPSTORETURN_BOTH;
						createRequest.itemsToCreate = items;
						createRequest.itemsToCreateSize = requestConfig->payload_dataArraySize;
						UA_CreateMonitoredItemsResponse createResponse = UA_Client_MonitoredItems_createDataChanges(requestConfig->client, createRequest, contexts, callbacks, deleteCallbacks);

						//monRequestNode = UA_MonitoredItemCreateRequest_default(UA_NODEID_STRING(ns, requestConfig->payload_dataArray[i]));
						//monResponseNode = UA_Client_MonitoredItems_createDataChange(requestConfig->client, response.subscriptionId, UA_TIMESTAMPSTORETURN_BOTH, monRequestNode, NULL, callback_ReadResponseData, NULL);
						UA_Boolean allResultsGood = UA_TRUE;
						//subscribedVariablesArray = malloc(requestConfig->payload_dataArraySize*sizeof(UA_Int32));
						requestConfig->subscribedVariablesArray = malloc(requestConfig->payload_dataArraySize*sizeof(char*));
						//subscribedValuesArray =  malloc(requestConfig->payload_dataArraySize*sizeof(char*));
						requestConfig->subscribedValuesArray =  malloc(requestConfig->payload_dataArraySize*sizeof(UA_String*));
						requestConfig->subscribedArraySize = requestConfig->payload_dataArraySize;
						for (i = 0; i < requestConfig->payload_dataArraySize; i++){
							if (createResponse.results[i].monitoredItemId != 0){
								requestConfig->subscribedVariablesArray[createResponse.results[i].monitoredItemId-1] = NULL;
							}
							requestConfig->subscribedValuesArray[i] = NULL;
							if (createResponse.results[i].statusCode == UA_STATUSCODE_GOOD && createResponse.results[i].monitoredItemId-1 < requestConfig->payload_dataArraySize)
							{
								//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Monitoring %s, id %u\n", requestConfig->payload_dataArray[i], createResponse.results[i].monitoredItemId);
								//subscribedVariablesArray[createResponse.results[i].monitoredItemId] = atoi(requestConfig->payload_dataArray[i]);
								requestConfig->subscribedVariablesArray[createResponse.results[i].monitoredItemId-1] = strdup(requestConfig->payload_dataArray[i]);
								//subscribedValuesArray[i]->data = NULL;
								//requestConfig->subscribedValuesArray[i] = NULL;
							}else{
								UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Monitoring Failed:  %s, id %u\n", requestConfig->payload_dataArray[i], createResponse.results[i].monitoredItemId);
		
								allResultsGood = UA_FALSE;
								UA_CreateMonitoredItemsResponse_clear(&createResponse);
								exit_client(requestConfig);
							}
						}
						UA_CreateMonitoredItemsResponse_clear(&createResponse);
						if (allResultsGood)
							activeSubscription = UA_TRUE;
						
						// TODO: add error handling on bad monitoring result
					}else{

						UA_MonitoredItemCreateRequest monRequestNode = UA_MonitoredItemCreateRequest_default(UA_NODEID_NUMERIC(ns, atoi(requestConfig->payload_data)));
						UA_MonitoredItemCreateResult monResponseNode = UA_Client_MonitoredItems_createDataChange(requestConfig->client, response.subscriptionId,
																												UA_TIMESTAMPSTORETURN_BOTH, monRequestNode, NULL, callback_ReadResponseData, NULL);
						if (monResponseNode.statusCode == UA_STATUSCODE_GOOD)
						{
							UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,"Monitoring %s, id %u\n", identifier, monResponseNode.monitoredItemId);
							
							//subscribedVariablesArray[monResponseNode.monitoredItemId] = atoi(requestConfig->payload_data);
							requestConfig->subscribedVariablesArray = malloc(monResponseNode.monitoredItemId*sizeof(char*));
							requestConfig->subscribedVariablesArray[monResponseNode.monitoredItemId-1] = strdup(requestConfig->payload_data);
							activeSubscription = UA_TRUE;
						}
						// TODO: add error handling on bad monitoring result
					}
				}
				else if (strcmp(requestConfig->subscription, "false") == 0)
				{
					// one-time read variable value operation
					//requestConfig->appConfig->operation = strdup("read");

					retval = setup_json(&common, requestConfig);
					if(retval != UA_STATUSCODE_GOOD) {
						UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Publish Message SetUp: Error Name = %s",
						UA_StatusCode_name(retval));
						exit_client(requestConfig);
					}

					//check if more than one variable is requested to be read
					if (requestConfig->payload_dataArraySize > 0){
						//read multiple variables at once
						// no documentation available for multi variable read, thus used func __UA_Client_readAttribute as a template
						char **variableArray = requestConfig->payload_dataArray;
						//char *variableArray[] = (char *) requestConfig->payload_data;
						UA_ReadRequest request;
						UA_ReadRequest_init(&request);
						UA_ReadValueId ids[requestConfig->payload_dataArraySize];
						int i;
						for (i = 0; i < requestConfig->payload_dataArraySize; i++){
							
							UA_ReadValueId_init(&ids[i]);
							ids[i].attributeId = UA_ATTRIBUTEID_VALUE;
							ids[i].nodeId = UA_NODEID_NUMERIC(ns, atoi(variableArray[i]));
						}
						// set here the nodes you want to read
							request.nodesToRead = ids;
							request.nodesToReadSize = requestConfig->payload_dataArraySize;

							UA_ReadResponse response = UA_Client_Service_read(requestConfig->client, request);


						if (response.responseHeader.serviceResult != UA_STATUSCODE_GOOD) ///&& UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_STRING])))
						{
							UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "opc_ua_client: Error Name = %s",
										UA_StatusCode_name(retval));
							exit_client(requestConfig);
							//UA_Client_disconnect(requestConfig->client);
							//UA_Client_delete(requestConfig->client);
							//UA_Server_deleteNode(common.server, UA_NODEID_STRING(1, requestConfig->reqId), FALSE);
							//return (void *) EXIT_FAILURE;
						}
						if (requestConfig->payload_dataArraySize != response.resultsSize){
							UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "opc_ua_client: number of values in response does not match with number of requested variable values");
							exit_client(requestConfig);
							//TODO: inform requesting component with error mqtt msg
						}

						//UA_Variant *resultArray;
						//resultArray = malloc(response.resultsSize*sizeof(UA_Variant));
						UA_String **resultArray = malloc(response.resultsSize*sizeof(UA_String *));
						//char **resultArray = malloc(response.resultsSize*sizeof(char *));
						UA_String UAdummy[response.resultsSize];
						char dummy[20];
						for (i = 0; i < response.resultsSize; i++){
							if (!response.results[i].hasValue){
								UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "opc_ua_client: one returned request result did not contain a value");
								exit_client(requestConfig);
								//TODO: inform requesting component with error mqtt msg
							} else if(response.results[i].value.type == NULL){
								UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "opc_ua_client: received value without type definition");
								exit_client(requestConfig);
							}
							switch(response.results[i].value.type->typeId.identifier.numeric){
								case UA_TYPES_INT16:
								case UA_TYPES_INT32:
								case UA_TYPES_INT64:
								case UA_TYPES_UINT16:
								case UA_TYPES_UINT32:
								case UA_TYPES_UINT64:
									//if (UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT32]) || UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT16])) {
									;
									//char dummy[20];
									//int intDummy = ;
									//sprintf(dummy, "%d", intDummy);
									sprintf(dummy, "%d", *(int*)response.results[i].value.data);
									///sprintf(dummy, "%d", *(int*)response.results[i].value.data);
									//UA_String UAdummy = UA_STRING(strdup(dummy));
									UAdummy[i] = UA_STRING(strdup(dummy));
									//resultArray[i] = &UAdummy;
									resultArray[i] = &UAdummy[i];
									//resultArray[i] = dummy;
									break;

								case UA_TYPES_DOUBLE:
									;
									//char dummy2[20];
									//double doubleDummy = *(double*)response.results[i].value.data;
									sprintf(dummy, "%f", *(double*)response.results[i].value.data);
									//UA_String UAdummy2 = UA_STRING(dummy);
									UAdummy[i] = UA_STRING(strdup(dummy));
									resultArray[i] = &UAdummy[i];
									break;

								case UA_TYPES_STRING:
									resultArray[i] = (UA_String *)response.results[i].value.data;
									break;
								default:
								UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Data Type with NodeId %u not covered in switch statement", response.results[i].value.type->typeId.identifier.numeric);
							}
						}	
						retval = replyArrayToReadRequest(variableArray,resultArray, response.resultsSize, &common, requestConfig);
						for (i = 0; i < response.resultsSize; i++){
							UA_String_clear(resultArray[i]);
							//TODO: may rather UA_String_delete_members(resultArray[i]);
						}
						free(resultArray);

						if (retval != UA_STATUSCODE_GOOD)
						{
							UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "replyArrayToReadRequest: Error Name = %s", UA_StatusCode_name(retval));
							exit_client(requestConfig);
						}


					}else{
						// only one variable needs to be read
						//retval = UA_Client_readValueAttribute(requestConfig->client, UA_NODEID_STRING(ns, identifier), val);
						UA_UInt32 nodeId = atoi(identifier);
						retval = UA_Client_readValueAttribute(requestConfig->client, UA_NODEID_NUMERIC(ns, nodeId), val);

						if (retval != UA_STATUSCODE_GOOD )
						//if (!(retval == UA_STATUSCODE_GOOD && UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_STRING])))
						{
							UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "client_server_read: Error Name = %s",
										UA_StatusCode_name(retval));
							UA_Client_disconnect(requestConfig->client);
							UA_Client_delete(requestConfig->client);
							UA_Server_deleteNode(common.server, UA_NODEID_STRING(1, requestConfig->reqId), FALSE);
							exit_client(requestConfig);
						}
						
						if(val->type == NULL){
								UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "opc_ua_client: received value without type definition");
								exit_client(requestConfig);
						}

						switch(val->type->typeId.identifier.numeric-1){
								case UA_TYPES_INT16:
								case UA_TYPES_INT32:
								case UA_TYPES_INT64:
								case UA_TYPES_UINT16:
								case UA_TYPES_UINT32:
								case UA_TYPES_UINT64:
								// TODO: be more careful in typecast here
									//if (UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT32]) || UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_INT16])) {
									;
									char dummy[20];
									int intDummy = *(int*)val->data;
									sprintf(dummy, "%d", intDummy);
									//UA_String UAdummy = UA_STRING(dummy);
									//resultArray[i] = &UAdummy;
									read_value = UA_STRING(dummy);
									break;
								case UA_TYPES_DOUBLE:
									if (UA_Variant_hasScalarType(val, &UA_TYPES[UA_TYPES_DOUBLE])) {
									char dummy[20];
									double doubleDummy = *(double*)val->data;
									sprintf(dummy, "%f", doubleDummy);
									//UA_String UAdummy = UA_STRING(dummy);
									//resultArray[i] = &UAdummy;
									read_value = UA_STRING(dummy);
									}
									break;
								case UA_TYPES_STRING:
									read_value = *(UA_String *)val->data;
									break;
								default:
								UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Data Type with NodeId %u not covered in switch statement", val->type->typeId.identifier.numeric);
							}

						//read_value = *(UA_String *)val->data;
						UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Read value from second optimum device %s\n", read_value.data);

						//retval = prepareReadValue(read_value, requestConfig->appConfig->publishTopic, request_id);
						retval = replyToReadRequest(identifier, read_value, &common, requestConfig);

						if (retval != UA_STATUSCODE_GOOD)
						{
							UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "replyToReadRequest: Error Name = %s", UA_StatusCode_name(retval));
							exit_client(requestConfig);
						}
					}

					//tear down OPC UA client thread since task is done
					cond = _FALSE;
				}
				//else if (0 != strcmp(requestConfig->nodeToSubscribe, "null") && 0 != strcmp(requestConfig->isSubscribed, "FALSE"))
				else
				{ // not true, not false, not cancel -> unknown
					UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Payload of received MQTT msg is wrong, read command, but subscription field holds unknown value: %s", requestConfig->subscription);
					UA_Client_disconnect(requestConfig->client);
					UA_Client_delete(requestConfig->client);
					//! TODO: send out last msg with final_msg:true
					UA_Server_deleteNode(common.server, UA_NODEID_STRING(1, requestConfig->reqId), FALSE);
				}

		}else if (strcmp(requestConfig->command, "write") == 0){
			// TODO: tbd if required (currently not needed)

		}else if (strcmp(requestConfig->command, "method") == 0){
			// TODO: tbd (e.g. reservation, cometome etc. on other OPTIMUM device)
			// still required? Currently, no demonstrator use case for this: a component of device 1 is activating 

		}else{
			// TODO: error msg
		}
	}
	//else if (strcmp(requestConfig->mode, "method_call"))
	else 
	{//  request a component of another OPTIMUM device that is not iiot
		//inputArgument = strcat(inputArgument, request_id);
		// TODO: check use of reqId und receiver side, really requried?
		// ! TODO: target component is no longer part of data field, add it here to payload 
		//inputArgument = strcat(inputArgument, requestConfig->reqnot the IIoT-Platform aka "generic request-response OR general purpose request"

		//char *inputArgument = strcat(requestConfig->payload, deId);
		//UA_String data = UA_STRING(inputArgument);

		UA_String outputArgument;
		//requestConfig->appConfig->operation = strdup("method_call");
		
		// --- prepare JSON payload
		json_object *request = json_object_new_object();
		json_object *tmp = json_object_new_string(requestConfig->component);
		json_object_object_add(request, "component", tmp);
		tmp = json_object_new_string(requestConfig->command);
		json_object_object_add(request, "command", tmp);
		tmp = json_object_new_string(requestConfig->payload_data);
		json_object_object_add(request, "data", tmp);

		UA_Variant input;
		UA_Variant_init(&input);
		UA_String requestString = UA_STRING((char*) json_object_get_string(request));
		//UA_Variant_setScalarCopy(&input, &data, &UA_TYPES[UA_TYPES_STRING]);
		UA_Variant_setScalarCopy(&input, &requestString, &UA_TYPES[UA_TYPES_STRING]);

		UA_CallRequest method_request;
		UA_CallRequest_init(&method_request);
		UA_CallMethodRequest item;
		UA_CallMethodRequest_init(&item);
		// TODO: find better name for generic method call
		item.methodId = UA_NODEID_STRING(3, "Method_JSON");
		//item.objectId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
		item.objectId = UA_NODEID_NUMERIC(3, 1033);
		item.inputArguments = &input;
		item.inputArgumentsSize = 1;
		method_request.methodsToCall = &item;
		method_request.methodsToCallSize = 1;

		// Call the service
		UA_CallResponse call_response = UA_Client_Service_call(requestConfig->client, method_request);
		retval = call_response.responseHeader.serviceResult;
		if (retval == UA_STATUSCODE_GOOD)
		{
			if (call_response.resultsSize == 1 && call_response.results->outputArgumentsSize > 0)
			{
				// extract response of method call
				UA_Variant *output = call_response.results[0].outputArguments; // access return output argument from the method call
				outputArgument = *(UA_String *)output->data;
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Method Call outputArgument value:  %s", outputArgument.data);

				retval = call_response.results[0].statusCode;
			}
			else
			{
				retval = UA_STATUSCODE_BADUNEXPECTEDERROR;
				//pthread_exit(NULL);
				exit_client(requestConfig);
			}
		}
		//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Method Call: STATUS CODE = %s", UA_StatusCode_name(retval));

		// parse response (output argument) of method call

		char  buffer[strlen((char*)outputArgument.data)];
		strncpy(buffer, (char*)outputArgument.data, strlen((char*)outputArgument.data));
		
		//parsed_json_data = json_tokener_parse(buffer);	
		struct json_object *parsed_json_data= json_tokener_parse(buffer);	
		struct json_object *status;
		struct json_object *response_node;
		char *nodeIdentifier;
		
		if(json_object_object_get_ex(parsed_json_data, "status", &status) 
			&& json_object_object_get_ex(parsed_json_data, "response_node", &response_node)){ 
			
			//if (decodeResponseMessage(outputArgument))
			//{
			const char *decoded_status, *decoded_response_node;
			decoded_status = json_object_get_string(status);
			decoded_response_node = json_object_get_string(response_node);
			nodeIdentifier = strdup(decoded_response_node);

			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Status: %s", decoded_status);
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ResponseNode: %s", nodeIdentifier);

			json_object *jobj_data;
			jobj_data = formatMonitoredMessage(outputArgument, requestConfig->reqId); // prepare a json message
			outputArgument = UA_STRING(strdup(json_object_get_string(jobj_data)));

			// write  ack and response_node information to the requestIdSpecificNode, later we set up the pub sub communication that triggers the OPC UA pub sub extension to send out this data via MQTT
			// ?: why? ack is maybe of interest for the requesting component, reqId is here the id of the initial request. Required by reqeusting component to match this reply to the request
			retval = writeValueToRequestIdSpecificNode(common.server, outputArgument, requestConfig->reqId);

			if (UA_STATUSCODE_GOOD != retval)
			{
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
							 "writeReceivedOutputArguments: UA_Server_writeValue failed\n");
			}
			else
			{
				// Activate pubsub extension (in detail: the specific writergroup) to send mqtt message to requesting component (mqtt payload contains only ack and maybe remote nodeId)
				// setup  ack and response_node to sent to the requester before making a subscription
				// appConfig->reqId is the local reqId which is the name of a node in the local address space to connect opc client with MQTT pubsub
				//requestConfig->appConfig->operation = strdup("method_call");
				retval = setup_json(&common, requestConfig);
				// --- WORKAROUND:  to create a event-based mqtt publsiher based on timer-based publish function in open62541 ---
				//UA_Server_setWriterGroupOperational(requestConfig->appConfig->server, requestConfig->appConfig->writerGroupIdentifier);
				pubSubNotificationTrigger(&common, &requestConfig->publisher);
				//sleep(1); 
				//UA_Server_setWriterGroupDisabled(requestConfig->appConfig->server, requestConfig->appConfig->writerGroupIdentifier);
				// --- WORKAROUND END --- 
			}

			/* Create a subscription */
			UA_CreateSubscriptionRequest request = UA_CreateSubscriptionRequest_default();

			request.requestedLifetimeCount = 1000;
			request.requestedMaxKeepAliveCount = 1000;

			// ask for a publishing interval of 100 ms
			//request.requestedPublishingInterval = 100.0;
			request.requestedPublishingInterval = 50.0;
			UA_CreateSubscriptionResponse response = UA_Client_Subscriptions_create(requestConfig->client, request, NULL, NULL, NULL);

			UA_UInt32 subId = response.subscriptionId;
			if (response.responseHeader.serviceResult == UA_STATUSCODE_GOOD)
				printf("Create subscription succeeded, id %u\n", subId);

		

			//UA_MonitoredItemCreateRequest monRequestNode = UA_MonitoredItemCreateRequest_default(UA_NODEID_STRING(1, (char *)nodeIdentifier));
			requestConfig->monitoredItem = UA_MonitoredItemCreateRequest_default(UA_NODEID_STRING(1, (char *)nodeIdentifier));
			//UA_MonitoredItemCreateResult monResponseNode = UA_Client_MonitoredItems_createDataChange(requestConfig->client, response.subscriptionId,
			//																						 UA_TIMESTAMPSTORETURN_BOTH, monRequestNode, NULL, callback_NewResponseData, NULL);
			UA_MonitoredItemCreateResult monResponseNode = UA_Client_MonitoredItems_createDataChange(requestConfig->client, response.subscriptionId,
																									 UA_TIMESTAMPSTORETURN_BOTH, requestConfig->monitoredItem, NULL, callback_NewResponseData, NULL);
			
			
			if (monResponseNode.statusCode == UA_STATUSCODE_GOOD)
				printf("Monitoring 'Response Node', id %u\n", monResponseNode.monitoredItemId);
			else
				UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Monitoring 'Response Node: Error Name = %s",
							 UA_StatusCode_name(monResponseNode.statusCode));
		}
		else
		{
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Incorrect JSON Payload Format Received from Optimum Device 2\n");
			exit_client(requestConfig);
		}
	}

	// int n = list->firstIndex(list, requestConfig->reqId);
	// UA_Boolean listError = UA_FALSE;
	// clientList *tempClient;
	// if (n == -1)
	// {
	// 	listError = UA_TRUE;
	// }

	// if (!listError)
	// {
	// 	tempClient = (clientList *)list->at(list, n);

	// 	if (tempClient == NULL)
	// 	{
	// 		listError = UA_TRUE;
	// 	}
	// 	//TODO: check if list finds always the right entry reliable, then remove reqId comparison
	// 	else if (strcmp(tempClient->req_id, requestConfig->reqId) != 0)
	// 	{
	// 		listError = UA_TRUE;
	// 	}
	// }

	// if (listError)
	// {
	// 	cond = _FALSE;
	// 	UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "OPC UA client thread for request id %s termiantes now. No list entry found to poll for termination signal.", requestConfig->reqId);
	// }
 	//if (!cond)
	//	HASH_DEL(hashTable, requestConfig);
	//int connectTimeout = 0;
	UA_StatusCode retVal = UA_STATUSCODE_GOOD;
	while (cond)
	{
		retVal = UA_Client_run_iterate(requestConfig->client, 0);
		/*if (clientTerminateSignal == 1)
		{
			clientTerminateSignal = 0;
			cond = _FALSE;
			printf("client termiantes now\n");
		}*/
		
		//if (*(tempClient->terminateSignal) == UA_TRUE)
		if (requestConfig->terminateClient == UA_TRUE)
		{
			cond = _FALSE;
			//hashtable_remove(table, requestConfig->reqId, NULL);
			//HASH_DEL(hashTable, requestConfig);
			UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "OPC UA client thread for request id %s received termination signal and exits now.", requestConfig->reqId);
		}
		else if (retVal != UA_STATUSCODE_GOOD)
		{
			//UA_sleep_ms(1000);
			// reconnect to server
			//UA_Client_connect(requestConfig->client, requestConfig->url);
			//connectTimeout++;
			//if (connectTimeout > 5)
			//{
				cond = _FALSE;
				//UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "OPC UA client thread for request id %s lost connection, failed to reconnect and exits now.", requestConfig->reqId);
				UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "OPC UA client thread for request id %s lost connection and exits now.", requestConfig->reqId);
			//}
		}
		//else{
		//	connectTimeout = 0;
		//}
		 UA_sleep_ms(SUBSCRIPTION_INTERVAL/2);
	}

	// delete subscription
	if (activeSubscription){
		//! can i do that outside of client thread? if not hand it over to exit function inside thread
		//UA_Server_removeDataSetWriter(requestConfig->appConfig->server, requestConfig->appConfig->publishedDataSetIdent); //TODO data set writer id ?
		UA_Server_removeDataSetWriter(common.server, requestConfig->publisher.DataSetWriterIdentifier); // or is this inherently done by removing the writer group?
		UA_Server_removeWriterGroup(common.server, requestConfig->publisher.writerGroupIdentifier); //TODO: or writerGroupId?
		//UA_Server_removeDataSetField(requestConfig->appConfig->server, requestConfig->appConfig->publishedDataSetIdent); // should be inherently removed when removing parent PDS, 
		// and othwerise publishedDataSetIdent seems to be wrong, data set creation function UA_Server_addDataSetField returns may the right identifier
		UA_Server_removePublishedDataSet(common.server, requestConfig->publisher.publishedDataSetIdent); 
		
		UA_Client_Subscriptions_deleteSingle(requestConfig->client, requestConfig->sub_id);
		UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Deleted subscription with id %d (related to request id '%s'", requestConfig->sub_id, requestConfig->reqId);
		//! TODO: send out last msg with final_msg:true
	}

	//tear down connection
	exit_client(requestConfig);

	return (void *) EXIT_SUCCESS;
}

