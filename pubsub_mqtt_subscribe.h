
#ifndef EXAMPLES_PUBSUB_PUBSUB_MQTT_SUBSCRIBE_H_
#define EXAMPLES_PUBSUB_PUBSUB_MQTT_SUBSCRIBE_H_

#include <open62541/types.h>
#include "iiot_platform.h"

/*
* @brief Adds all neccesary components for opcua subscriber over MQTT
*
* @param appConfig pointer to the global appConfig
*/
//extern UA_StatusCode sub_mqtt_setup(UA_AppConfig *appConfig);
extern UA_StatusCode sub_mqtt_setup(common_data *common, subscriber_data *subscriber);



#endif /* EXAMPLES_PUBSUB_PUBSUB_MQTT_SUBSCRIBE_H_ */
