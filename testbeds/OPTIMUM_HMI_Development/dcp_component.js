function onMessage() {
	
	var request, request2, response, roll_the_dice, come_to_me_file, come_to_me_state, follow_me_state, follow_me_file, dcp_req_id, tempString;
	var Thread = Java.type("java.lang.Thread");
	//var File = Java.type("java.io.File");
	//var FileUtils = Java.type("org.apache.commons.io.FileUtils");
	
	// check current state
	//come_to_me_file = new File("come_to_me_state.txt")
	//come_to_me_state = JSON.parse(FileUtils.readFileToString(come_to_me_file, "utf-8"));
	//follow_me_file = new File("follow_me_state.txt")
	//follow_me_state = JSON.parse(FileUtils.readFileToString(follow_me_file, "utf-8"));
		

	// extract key-value pairs from request msg payload
	tempString = receivedMessage.getPayload();
	tempString = tempString.slice(2,-2);
	tempString = tempString.replace(/\\/g, '');
	tempString = tempString.replace(/}\"/g, '}');
	tempString = tempString.replace(/\"{/g, '{');
	//tempString = tempString.replace("}\\\\\\\" }\\\" }", "}}}");
	//tempString = tempString.replace("\\\"data\\\": \\\"{", "\\\"data\\\":{");
	//tempString = tempString.replace("\\\\\\\"arguments\\\\\\\": \\\"{", "\\\"arguments\\\":{");
	
	//logger.info(tempString);
	request = JSON.parse(tempString);
	// example request payload: {"answer_topic":"iiot_common","req_id":"dcp2103","mode":"method","method_id":"come_to_me","data":{"dev_id":"231","loc_dev":"192.168.0.85"}}
	//if (request.mode == "method"){
		switch (request["data"]["cmd"]) {
			case "cometo":
				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"[" + request.req_id + "]Parsing arguments of the request\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				//java.lang.Thread.sleep(3000);
				Thread.sleep(1000);

				//response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"Parsing arguments of the request with ID: " + request.req_id + "\"}";
				//mqttspy.publish(request.answer_topic, response, 0, false);
				//java.lang.Thread.sleep(3000);
				//Thread.sleep(1000);

				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"[" + request.req_id + "]Requesting target position\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				targetRequest = "{\"answer_topic\":\"dcp-common\",\"req_id\":\"" +request.req_id + "_dcp_sub_req\",\"command\":\"read\",\"device\":\"" + request["data"]["arguments"]["target_ip"] +  "\",\"component\":\"iiot\",\"data\":[\"1042\",\"1043\"],\"subscription\":\"false\"}";
				mqttspy.publish(request.answer_topic, targetRequest, 0, false);
				//java.lang.Thread.sleep(3000);
				Thread.sleep(1000);

				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"ongoing\",\"data\":\"[" + request.req_id + "]Coming to X: TEST Y: TEST Z: TEST\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				//java.lang.Thread.sleep(3000);
				Thread.sleep(3000);
				

				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"finish\",\"data\":\"[" + request.req_id + "]Target location reached\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				break;

			case "gotoposition":
				// check current state

				//if (come_to_me_state["status"] == "pending") {

				//	response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"data\":{\"status\":\"fail\",\"cause\":\"come to me process is already active\"}}";
				//	mqttspy.publish(request.answer_topic, response, 0, false);

				//} else if(come_to_me_state["status"] == "inactive") {
				//	dcp_req_id = "dcp" + (Math.random() * 100000).toFixed(0);
				//	file_content = "{\"status\":\"pending\",\"req_id\":\""+ request.req_id + "\",\"dcp_req_id\":\"" + dcp_req_id + "\"}";
				//	FileUtils.write(come_to_me_file, file_content, "utf-8");					
				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"[" + request.req_id + "]Parsing arguments of the request\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				Thread.sleep(10000);

				//response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"Parsing arguments of the request with ID: " + request.req_id + "\"}";
				//mqttspy.publish(request.answer_topic, response, 0, false);
				//java.lang.Thread.sleep(3000);
				//Thread.sleep(1000);

				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"ongoing\",\"data\":\"[" + request.req_id + "]Coming to X: " + request["data"]["arguments"]["position"]["x"] +" Y: " + request["data"]["arguments"]["position"]["y"] +" Z: " + request["data"]["arguments"]["position"]["z"] +"\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				//java.lang.Thread.sleep(3000);
				Thread.sleep(3000);

				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"finish\",\"data\":\"[" + request.req_id + "]Target location reached\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);


					//request location data
			//		request2 = "{\"answer_topic\":\"dcp_common\",\"req_id\":\"" + dcp_req_id +"\",\"mode\":\"read\",\"device\":\"" + request["data"]["loc_dev"] + "\",\"data\":\"location\",\"subscription\":\"false\"}";
			//		mqttspy.publish("iiot_common", request2, 0, false);

			//	} else {
			//		response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\,\"data\":{\"status\":\"fail\",\"cause\":\"unkown function status\"}}";
			//		mqttspy.publish(request.answer_topic, response, 0, false);
			//	}
				break;

			case "follow":
			//	if (follow_me_state["status"] == "pending") {

			//		response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"data\":{\"status\":\"fail\",\"cause\":\"follow me process is already active\"}}";
			//		mqttspy.publish(request.answer_topic, response, 0, false);

			//	} else if(follow_me_state["status"] == "inactive") {
			//		dcp_req_id = "dcp" + (Math.random() * 100000).toFixed(0);
			//		file_content = "{\"status\":\"pending\",\"req_id\":\""+ request.req_id + "\",\"dcp_req_id\":\"" + dcp_req_id + "\"}";
			//		FileUtils.write(follow_me_file, file_content, "utf-8");					
			response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"[" + request.req_id + "]Parsing arguments of the request\"}";
			mqttspy.publish(request.answer_topic, response, 0, false);
			Thread.sleep(1000);

			//response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"Parsing arguments of the request with ID: " + request.req_id + "\"}";
			//mqttspy.publish(request.answer_topic, response, 0, false);
			//java.lang.Thread.sleep(3000);
			//Thread.sleep(1000);

			response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"ongoing\",\"data\":\"[" + request.req_id + "]Begin following\"}";
			mqttspy.publish(request.answer_topic, response, 0, false);
			targetRequest = "{\"answer_topic\":\"dcp-common\",\"req_id\":\"" +request.req_id + "_dcp_sub_req\",\"command\":\"read\",\"device\":\"" + request["data"]["arguments"]["target_ip"] +  "\",\"component\":\"iiot\",\"data\":[\"1042\",\"1043\"],\"subscription\":\"true\"}";
				mqttspy.publish(request.answer_topic, targetRequest, 0, false);
			//java.lang.Thread.sleep(3000);
			Thread.sleep(10000);
			targetRequest = "{\"answer_topic\":\"dcp-common\",\"req_id\":\"" +request.req_id + "_dcp_sub_req\",\"command\":\"read\",\"device\":\"" + request["data"]["arguments"]["target_ip"] +  "\",\"component\":\"iiot\",\"data\":[\"1042\",\"1043\"],\"subscription\":\"cancel\"}";
				mqttspy.publish(request.answer_topic, targetRequest, 0, false);

			//response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\"",\"status\":\"finished\",\"data\":\"Target location reached\"}";
			//mqttspy.publish(request.answer_topic, response, 0, false);

					//request location data
				//	request2 = "{\"answer_topic\":\"dcp_common\",\"req_id\":\"" + dcp_req_id +"\",\"mode\":\"read\",\"device\":\"" + request["data"]["loc_dev"] + "\",\"data\":\"location\",\"subscription\":\"true\"}";
				//	mqttspy.publish("iiot_common", request2, 0, false);

			//	} else {
			//		response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\"",\"data\":{\"status\":\"fail\",\"cause\":\"unkown function status\"}}";
			//		mqttspy.publish(request.answer_topic, response, 0, false);
			//	}
				break;

			case "stop":
				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"success\", \"data\":\"[" + request.req_id + "]Parsing arguments of the request\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				Thread.sleep(10000);
				
				//response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":{\"device has stopped current activity\"}}";
				response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"success\",\"data\":\"[" + request.req_id + "]device has stopped current activity\"}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				break;

			case "reservation":
			//	roll_the_dice = Math.floor(Math.random() * 10) + 1;
			//	roll_the_dice = Math.floor(Math.random() * 10) + 1;
			//	if (roll_the_dice < 6) {
				if (request["data"]["arguments"]["status"]  == "on") {
					
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"[" + request.req_id + "]forwarding reservation request to DCP\"}";
					mqttspy.publish(request.answer_topic, response, 0, false);
					Thread.sleep(10000);
					
					mqttspy.publish("iiot-modify-data", "{\"nodes\":{\"6000\":\"Reserved\",\"6005\":\"" +  request["data"]["dev_id"] + "\"}}", 0, false);
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"success\",\"data\":\"[" + request.req_id + "]Reservation successful\"}";
					mqttspy.publish(request.answer_topic, response, 0, false);
					// update reservation status on mqtt broker and set retain flag
					//mqttspy.publish("reservation_status", "{\"status\":\"reserved\",\"dev_id\":\"" + request["data"]["dev_id"] +"\"}", 0, true);

				} else {	
					
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\",\"status\":\"pending\",\"data\":\"[" + request.req_id + "]forwarding reservation cancelation request to DCP\"}";
					mqttspy.publish(request.answer_topic, response, 0, false);	
					Thread.sleep(10000);
					
					mqttspy.publish("iiot-modify-data", "{\"nodes\":{\"6000\":\"Available\",\"6005\":\"\"}}", 0, false);
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"success\",\"data\":\"[" + request.req_id + "]Reservation canceled\"}";
					mqttspy.publish(request.answer_topic, response, 0, false);		
			//		response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\,\"data\":{\"status\":\"fail\",\"cause\":\"device_already_reserved\"}}";
			//		mqttspy.publish(request.answer_topic, response, 0, false);
				}
				break;
				
			// case "control_group":
			// 	response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"false\,\"data\":{\"status\":\"pending\"\",\"info\":\"selecting required components\"}}";
			// 	mqttspy.publish(request.answer_topic, response, 0, false);
			// 	java.lang.Thread.sleep(2000); //wait for 2 seconds
			// 	response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\,\"data\":{\"status\":\"success\"\",\"info\":\"required components have been selected\"}}";
			// 	mqttspy.publish(request.answer_topic, response, 0, false);
			// 	// update Control_group with (dummy) data on mqtt broker and set retain flag
			// 	mqttspy.publish("control_group", "{\"master\":\"425-1\",\"slaves\": [\"425-2\", \"426-1\", \"426-2\"]}}", 0, true);
			// 	//TO DO: test if using slaves list work in response
			// 	//mqttspy.publish("control_group", "{\"master\":\"" + request["data"]["master"] +"\",\"slaves\":" + request["data"]["slaves"] +"}}", 0, true);
			// 	// update optional operational_mode on mqtt broker and set retain flag
			// 	//mqttspy.publish("operational_mode", "{\"status\":\"group\"}", 0, true);

			// 	break;

			case "manualtandem":
				if (request["data"]["arguments"]["enable"]  == "on") { 
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"success\",\"data\":\"[" + request.req_id + "]Manual tandem is enabled on " + request["data"]["arguments"]["master"] + "\"}";
				} else if (request["data"]["arguments"]["enable"]  == "off") { 
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"success\",\"data\":\"[" + request.req_id + "]Manual tandem is disabled on " + request["data"]["arguments"]["master"] + "\"}";
				} else{
					response = "{\"req_id\":\"" + request.req_id + "\",\"final_msg\":\"true\",\"status\":\"fail\",\"data\":\"[" + request.req_id + "]Some reason here\"}";
				}

				mqttspy.publish(request.answer_topic, response, 0, false);
				Thread.sleep(10000);

				break;
				

			default:
				//response = "{\"req_id\":\"" + request.req_id + "\",\"status\":\"fail\",\"data\":{\"unkown data request\":\"" + request["data"] + "\"}}";
				response = "{\"req_id\":\"" + request.req_id + "\",\"status\":\"fail\",\"data\":{\"[" + request.req_id + "]unkown data request, cmd\":\"" + request["data"]["cmd"] + "\"}}";
				mqttspy.publish(request.answer_topic, response, 0, false);
				break;
		}
		
	// } else {
	// 	// possibly answer of iiot for come_to_me or follow me function:
	// 	if (request.req_id == come_to_me_state.dcp_req_id) {
	// 		response = "{\"req_id\":\"" + come_to_me_state.req_id + "\",\"final_msg\":\"false\,\"data\":{\"status\":\"pending\",\"info\":\"received location data, moving to target now\"}}";
	// 		mqttspy.publish("iiot_common", response, 0, false);
			
	// 		//simulate movement by sleeping
	// 		Thread.sleep(3000);
	
	// 		response = "{\"req_id\":\"" + come_to_me_state.req_id + "\",\"final_msg\":\"true\,\"data\":{\"status\":\"success\",\"info\":\"target location reached\"}}";
	// 		mqttspy.publish("iiot_common", response, 0, false);
	// 		file_content = "{\"status\":\"inactive\"}";
	// 		FileUtils.write(come_to_me_file, file_content, "utf-8");
		
	// 	} else if (request.req_id == follow_me_state.dcp_req_id) {
	// 		response = "{\"req_id\":\"" + follow_me_state.req_id + "\",\"final_msg\":\"true\,\"data\":{\"status\":\"pending\",\"info\":\"receiving location data, following the target now\"}}";
	// 		mqttspy.publish("iiot_common", response, 0, false);
			
		
	// 		file_content = "{\"status\":\"inactive\"}";
	// 		FileUtils.write(follow_me_file, file_content, "utf-8");
		
	// 	} else{
	// 		//dev/null
	// 		//response = "{\"req_id\":\"" + request.req_id + "\",\"data\":{\"status\":\"fail\",\"" + request.mode + "\":\"unkown mode\"}}}";
	// 	}
	// }

	return true; 
}

