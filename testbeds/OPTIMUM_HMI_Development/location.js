function publish()
{
	var x = 0; // start location x
	var y = 0; // start location y
	var step_width = 150; //(80 cm per second = 3 km/h)
	var localization_update_rate = 100; //100 ms
	var location_updates = 300;
	var randomRange = 5; //cm , in reality ~20cm but with normal distribution. script currently only supports equally distributed -> heavy  worst case scenario

	location_increment = step_width/(1000/localization_update_rate); //+ step progress determined by step width and update rate

	for (i = 0; i < location_updates; i++) {
	// ToDos:
	// distance error by simultaneous increment of x AND y (pythagoras) - done 
	// add noise (+/- 20 cm) - partly done, no normal distribution yet due to problems using functions
		noiseX = Math.floor(Math.random()*randomRange) + 1;
		noiseY = Math.floor(Math.random()*randomRange) + 1;
		noiseX *= Math.floor(Math.random()*2) == 1 ? 1 : -1; 
		noiseY *= Math.floor(Math.random()*2) == 1 ? 1 : -1; 
		x = x +	Math.floor(location_increment/Math.sqrt(2) + noiseX); // assuming linear ascent with some noise		
		y = y + Math.floor(location_increment/Math.sqrt(2) + noiseY); // assuming linear ascent with some noise
		response = "{\"nodes\":{\"1042\":" + x + ",\"1043\":" + y + ",\"1056\":0}}";
		mqttspy.publish("iiot-modify-data", response, 0, false);
		java.lang.Thread.sleep(localization_update_rate);
	}
}
while (true){
	publish();
}
